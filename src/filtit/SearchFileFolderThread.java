package filtit;

/**
 *
 * @author Hans-Georg Schladitz
 */
class SearchFileFolderThread extends Thread {

    /**
     * FiltIt model.
     */
    private FiltItModel model;
    /**
     * Temp-Model to avoid thread-problems.
     */
    private FiltItModel tempModel;
    /**
     * FiltIt controller.
     */
    private FiltItController controller;

    /**
     * Defaultconstructor.
     */
    public SearchFileFolderThread(FiltItModel model, FiltItController controller) {
        this.model = model;
        this.controller = controller;
        tempModel = new FiltItModel();
        tempModel.getVM().setActualViewmodi(model.getVM().getActualViewmodi());
        tempModel.setFind(model.getFind());
        tempModel.setFolderPath(model.getFolderPath());
    }

    @Override
    public void run() {
        // Methode starten:
        tempModel.searchFilesAndFolder();
        model.getDataModel().setDatalist(tempModel.getDataModel().getDatalist());
        controller.update();
    }
}
