package filtit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

/**
 * Frame who manage the Columnstructure.
 *
 * @author Hans-Georg Schladitz
 */
public class ColumnManagementFrame extends JFrame implements ActionListener, KeyListener, WindowListener {

    /**
     * Current DatenModel.
     */
    private DataModel dm;
    /**
     * Current used maincontroller.
     */
    private FiltItController maincontroller;
    /**
     * List of current labels order by name.
     */
    private ArrayList<Object> exampleValueList;
    /**
     * List of current checkBox order by View.
     */
    private ArrayList<JCheckBox> checkBoxList;

    private Viewmodi v;
    /**
     * Constructor.
     *
     * @param title String
     * @param dm DataModel
     * @param controller FiltItController
     */
    public ColumnManagementFrame(String title, DataModel dm, FiltItController controller, Viewmodi v) {
        super(title);
        this.dm = dm;
        this.maincontroller = controller;
        checkBoxList = new ArrayList();
        exampleValueList = new ArrayList();
        this.v = v;
        loadCheckBoxList();
        init();
    }

    /**
     * Init the Frame.
     */
    private void init() {
        this.addWindowListener(this);
        this.setSize(600, 600);

        // Set elements:        
        JPanel mainpanel = new JPanel(new BorderLayout());
        JPanel whitepanel = new JPanel(new BorderLayout());

        whitepanel.setBackground(Color.WHITE);
//        Border lineborder = BorderFactory.createLineBorder(Color.BLACK);
//        whitepanel.setBorder(lineborder);

        JScrollPane sp = new JScrollPane(whitepanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        JLabel labelColumn = new JLabel("Columns: ");
        JLabel labelExample = new JLabel("Example Data:");

        JButton btnSave = new JButton(" Save ");
        btnSave.addActionListener(this);
        btnSave.setActionCommand("MBok");

        String iconFileName = "icon16.png";
        IconManager im = new IconManager();
        this.setIconImage(im.createImageIcon(iconFileName).getImage());

        Box mbox = Box.createHorizontalBox();
        Box vbox01 = Box.createVerticalBox(); // for whitepanel: vbox03|04
        Box vbox02 = Box.createVerticalBox(); // for btn´s
        Box hbox = Box.createHorizontalBox();
        Box vbox03 = Box.createVerticalBox(); // for Checkboxes
        Box vbox04 = Box.createVerticalBox(); // for Example

        Box hHead = Box.createHorizontalBox();
        hHead.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));

        this.add(mainpanel);
        mainpanel.add(mbox);
        mbox.add(Box.createHorizontalStrut(35));
        vbox01.add(Box.createVerticalStrut(35));
        vbox01.add(hHead);
        hHead.add(labelColumn);
        hHead.add(Box.createHorizontalStrut(90));
        hHead.add(labelExample);
        hHead.add(Box.createHorizontalGlue());
        mbox.add(vbox01);
        mbox.add(Box.createHorizontalStrut(35));
        mbox.add(vbox02);
        mbox.add(Box.createHorizontalStrut(35));
        mbox.add(Box.createVerticalStrut(35));
        vbox01.add(sp);

        whitepanel.add(hbox);
        hbox.add(vbox03);
        hbox.add(Box.createHorizontalStrut(35));
        hbox.add(vbox04);
        vbox01.add(Box.createVerticalGlue());

        JLabel label;
        Icon icon;
        vbox04.add(Box.createVerticalStrut(3));

        for (int i = 0; i < checkBoxList.size(); i++) {

            vbox03.add(checkBoxList.get(i));
            // Wenn der Wert ein String ist:
            if (exampleValueList.get(i) instanceof String) {
                label = new JLabel((String) exampleValueList.get(i));
                label.setForeground(Color.GRAY);
                vbox04.add(label);
            }
            // Wenn der Wert ein Icon ist:
            if (exampleValueList.get(i) instanceof Icon) {
                icon = (Icon) exampleValueList.get(i);
                label = new JLabel("folder", icon, SwingConstants.LEFT);
                vbox04.add(label);
            }

            vbox04.add(Box.createVerticalStrut(8));

        }
        vbox03.add(Box.createVerticalGlue());
        vbox04.add(Box.createVerticalGlue());
        vbox02.add(btnSave);

        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - this.getSize().width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - this.getSize().height) / 2;
        this.setBounds(x, y, 500, 500);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
    }

    /**
     * Creating the List of possible Columns and sets true in checkbox when
     * column was choosen.
     */
    private void loadCheckBoxList() {
        JCheckBox cb;

        ArrayList<Integer> columnList = Utilities.castStringToArrayList(v.getColumnCode());
        
        // Erst die Spalten, die schon im SpaltenCode stehen:
        for (int i = 0; i < columnList.size(); i++) {
            cb = new JCheckBox(dm.getColumnNames()[columnList.get(i)]);
            cb.setBackground(Color.WHITE);
            cb.setName(String.valueOf(columnList.get(i)));
            cb.setSelected(true);
            this.checkBoxList.add(cb);       
            if (dm.getExampleValues()[columnList.get(i)] instanceof String) {
                this.exampleValueList.add((String) dm.getExampleValues()[columnList.get(i)]);
            }
            if (dm.getExampleValues()[columnList.get(i)] instanceof Icon) {
                this.exampleValueList.add((Icon) dm.getExampleValues()[columnList.get(i)]);
            }
        }
        // Dann die restlichen Spalten hinzufügen:
        // Grund: Reihenfolge wird beachtet!!!                            
        for (int i = 0; i < dm.getColumnNames().length; i++) {
            // Wenn die Nummer noch nicht kam
            if (!columnList.contains(i)) {
                cb = new JCheckBox(dm.getColumnNames()[i]);
                cb.setBackground(Color.WHITE);
                cb.setName(String.valueOf(i));
                cb.setSelected(false);
                this.checkBoxList.add(cb);
                // Wenn es sich um einen String handelt
                if (dm.getExampleValues()[i] instanceof String) {
                    this.exampleValueList.add((String) dm.getExampleValues()[i]);
                }
                if (dm.getExampleValues()[i] instanceof Icon) {
                    this.exampleValueList.add((Icon) dm.getExampleValues()[i]);
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("MBok")) {
            this.maincontroller.refreshColumnStructure(this.checkBoxList);
            this.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getExtendedKeyCode() == 10) {
            this.maincontroller.refreshColumnStructure(this.checkBoxList);
            this.dispose();
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}
