package filtit;

import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Creates and handle a Componente for the View.
 *
 * @author Hans-Georg Schladitz
 */
class MinMaxCollectorFileSizeComponent {

    private JTextArea minTA = new JTextArea("", 0, 33);
    private JTextArea maxTA = new JTextArea("", 0, 33);

    private JScrollPane minSP = new JScrollPane(minTA, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    private JScrollPane maxSP = new JScrollPane(maxTA, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    private final String[] cbValues;

    private JComboBox minCB;
    private JComboBox maxCB;

    private int TAWidth = 100;
    private int TAHeight = 33;

    private int CBWidth = 60;
    private int CBHeight = 22;

    private int StrutWidth = 9; // HorizontalStrut 2 / 5 / 2

    public MinMaxCollectorFileSizeComponent() {
        cbValues = new String[]{"Byte", "KB", "MB", "GB", "TB", "PB"};
        this.minCB = new JComboBox(cbValues);
        this.maxCB = new JComboBox(cbValues);
    }

    public Box insertComponentsIntoAHorizontalBox(Box box) {

        minTA.setName("minTA");
        maxTA.setName("maxTA");

        // Feste Größen definieren:
        minSP.setMinimumSize(new Dimension(TAWidth, TAHeight));
        minSP.setPreferredSize(new Dimension(TAWidth, TAHeight));
        minSP.setMaximumSize(new Dimension(TAWidth, TAHeight));

        maxSP.setMinimumSize(new Dimension(TAWidth, TAHeight));
        maxSP.setPreferredSize(new Dimension(TAWidth, TAHeight));
        maxSP.setMaximumSize(new Dimension(TAWidth, TAHeight));

        minCB.setMinimumSize(new Dimension(CBWidth, CBHeight));
        minCB.setPreferredSize(new Dimension(CBWidth, CBHeight));
        minCB.setMaximumSize(new Dimension(CBWidth, CBHeight));

        maxCB.setMinimumSize(new Dimension(CBWidth, CBHeight));
        maxCB.setPreferredSize(new Dimension(CBWidth, CBHeight));
        maxCB.setMaximumSize(new Dimension(CBWidth, CBHeight));

        box.setMinimumSize(new Dimension(TAWidth * 2 + CBWidth * 2 + StrutWidth, 25));
        box.setPreferredSize(new Dimension(TAWidth * 2 + CBWidth * 2 + StrutWidth, 25));
        box.setMaximumSize(new Dimension(TAWidth * 2 + CBWidth * 2 + StrutWidth, 25));

        box.add(minSP);
        box.add(Box.createHorizontalStrut(2)); // StrutWidth
        box.add(minCB);
        box.add(Box.createHorizontalStrut(5)); // StrutWidth
        box.add(maxSP);
        box.add(Box.createHorizontalStrut(2)); // StrutWidth
        box.add(maxCB);
        return box;
    }

    /**
     * Getter for min-Input as Byte.
     *
     * @return double.
     */
    public double getdMinValue() {
        if (minTA.getText().equals("")|| minTA == null) {
            return 0;
        }
        return calcValues(minTA.getText(), (String) this.minCB.getSelectedItem());
    }

    /**
     * Getter for max-Input as Byte.
     *
     * @return double.
     */
    public double getdMaxValue() {
        if (maxTA.getText().equals("")|| maxTA == null) {
            return 0;
        }
        return calcValues(maxTA.getText(), (String) this.maxCB.getSelectedItem());
    }

    /**
     * Calculates the real value and normalized it.
     *
     * @param tatext String normal input as integer
     * @param cbvalue Defined values.
     * @return double
     */
    private double calcValues(String tatext, String cbvalue) {

        try {
            double tatvalue = Double.valueOf(tatext);

            if (cbvalue.equals("Byte")) {
                return tatvalue;
            }
            if (cbvalue.equals("KB")) {
                return tatvalue * 1024;
            }
            if (cbvalue.equals("MB")) {
                return tatvalue * 1048576;
            }
            if (cbvalue.equals("GB")) {
                return tatvalue * 1073741824;
            }
            if (cbvalue.equals("TB")) {
                return tatvalue * 1073741824 * 1024;
            }
            if (cbvalue.equals("PB")) {
                return tatvalue * 1073741824 * 1048576;
            }

        } catch (Exception e) {
            return 0;
        }
        return 0;
    }

    public JComboBox getMinCB() {
        return minCB;
    }

    public JComboBox getMaxCB() {
        return maxCB;
    }

    public int getComboundBoxWidth() {
        return TAWidth * 2 + CBWidth * 2 + StrutWidth;
    }

    public int getComboundBoxHeight() {
        return TAHeight + CBHeight;
    }

    public JScrollPane getMinSP() {
        return minSP;
    }

    public JScrollPane getMaxSP() {
        return maxSP;
    }

    public int getMinCB_Value() {
        return this.minCB.getSelectedIndex();
    }

    public int getMaxCB_Value() {
        return this.maxCB.getSelectedIndex();
    }

    public JTextArea getMinTA() {
        return minTA;
    }

    public JTextArea getMaxTA() {
        return maxTA;
    }

    public void setMinCB_Value(int selectIndex) {
        this.minCB.setSelectedIndex(selectIndex);
    }

    public void setMaxCB_Value(int selectIndex) {
        this.maxCB.setSelectedIndex(selectIndex);
    }
}
