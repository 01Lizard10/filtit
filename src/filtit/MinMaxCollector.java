package filtit;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Creates and handle a Componente for the View.
 *
 * @author Hans-Georg Schladitz
 */
class MinMaxCollector {

    private JTextArea minTA = new JTextArea("", 0, 33);
    private JTextArea maxTA = new JTextArea("", 0, 33);

    private JScrollPane minSP = new JScrollPane(minTA, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    private JScrollPane maxSP = new JScrollPane(maxTA, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    private JComboBox minCB;
    private JComboBox maxCB;

    private int TAWidth = 100;
    private int TAHeight = 33;

    private int CBWidth = 60;
    private int CBHeight = 22;

    private int StrutWidth = 9; // HorizontalStrut 2 / 5 / 2


    /**
     * Getter for max-Input.
     *
     * @return String.
     */
    public String getMinTextValue() {
        return minTA.getText();
    }

    /**
     * Getter.
     *
     * @return choosen Value.
     */
    public String getMinValue() {
        return (String) this.minCB.getSelectedItem();
    }

    /**
     * Getter for max-Input.
     *
     * @return String.
     */
    public String getMaxTextValue() {
        return maxTA.getText();
    }

    /**
     * Getter.
     *
     * @return choosen Value-
     */
    public String getMaxValue() {
        return (String) this.maxCB.getSelectedItem();
    }

    public int getComboundBoxWidth() {
        return TAWidth * 2 + CBWidth * 2 + StrutWidth;
    }

    public int getComboundBoxHeight() {
        return TAHeight + CBHeight;
    }

    /**
     * Setter for max.
     *
     * @return String.
     */
    public void setMaxTextValue(String text) {
        this.maxTA.setText(text);
    }

    /**
     * Setter for min.
     *
     * @return String.
     */
    public void setMinTextValue(String text) {
        this.minTA.setText(text);
    }

    public JTextArea getMinTA() {
        return minTA;
    }

    public void setMinTA(JTextArea minTA) {
        this.minTA = minTA;
    }

    public JTextArea getMaxTA() {
        return maxTA;
    }

    public void setMaxTA(JTextArea maxTA) {
        this.maxTA = maxTA;
    }

    public JScrollPane getMinSP() {
        return minSP;
    }

    public void setMinSP(JScrollPane minSP) {
        this.minSP = minSP;
    }

    public JScrollPane getMaxSP() {
        return maxSP;
    }

    public void setMaxSP(JScrollPane maxSP) {
        this.maxSP = maxSP;
    }

 
    public void setMinCB(JComboBox minCB) {
        this.minCB = minCB;
    }


    public void setMaxCB(JComboBox maxCB) {
        this.maxCB = maxCB;
    }
}
