package filtit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class ViewmodiManagementFrame extends JFrame implements ActionListener,
        KeyListener, WindowListener, MouseListener, TableColumnModelListener {

    /**
     * Current used maincontroller.
     */
    private FiltItController maincontroller;
    /**
     * Manage the viewmodi.
     */
    private ViewmodiManagement vm;
    /**
     * Viewmoditable.
     */
    private JTable table;
    /**
     * Scrollpanel for viewmoditable.
     */
    private JScrollPane sptable;
    /**
     * DefaultTableModel.
     */
    private DefaultTableModel dtm;
    /**
     * Messagebox.
     */
    private MessageBox mb = new MessageBox(this);
    /**
     * Backup for the abort-button.
     */
    private ArrayList<Viewmodi> bavList;

    /**
     *
     * @param title String
     * @param controller FiltIt controller
     * @param vm ViewmodeManager
     */
    public ViewmodiManagementFrame(String title, FiltItController controller, ViewmodiManagement vm) {
        super(title);
        this.maincontroller = controller;
        this.vm = vm;
        table = new JTable();
        sptable = new JScrollPane(table);
        vm.readViewmodiList();
        bavList = vm.getvList();
        this.loadTableStructure();
        this.insertTableValues();
        init();
    }

    /**
     * Init the Frame.
     */
    private void init() {
        this.addWindowListener(this);
        this.setSize(600, 600);

        // Set elements:        
        JPanel mainpanel = new JPanel(new BorderLayout());
        JPanel whitepanel = new JPanel(new BorderLayout());

        whitepanel.setBackground(Color.WHITE);

        JButton btnUP = new JButton("up");
        btnUP.addActionListener(this);
        btnUP.setActionCommand("up");

        JButton btnDOWN = new JButton("down");
        btnDOWN.addActionListener(this);
        btnDOWN.setActionCommand("down");

        JButton btnDELETE = new JButton("delete");
        btnDELETE.addActionListener(this);
        btnDELETE.setActionCommand("delete");

        JButton btnOK = new JButton("ok");
        btnOK.addActionListener(this);
        btnOK.setActionCommand("ok");

        JButton btnABORT = new JButton("abort");
        btnABORT.addActionListener(this);
        btnABORT.setActionCommand("abort");

        int bWidth = 80;
        int bHeight = 30;

        btnUP.setPreferredSize(new Dimension(bWidth, bHeight));
        btnUP.setMaximumSize(new Dimension(bWidth, bHeight));
        btnUP.setMinimumSize(new Dimension(bWidth, bHeight));

        btnDOWN.setPreferredSize(new Dimension(bWidth, bHeight));
        btnDOWN.setMaximumSize(new Dimension(bWidth, bHeight));
        btnDOWN.setMinimumSize(new Dimension(bWidth, bHeight));

        btnDELETE.setPreferredSize(new Dimension(bWidth, bHeight));
        btnDELETE.setMaximumSize(new Dimension(bWidth, bHeight));
        btnDELETE.setMinimumSize(new Dimension(bWidth, bHeight));

        btnOK.setPreferredSize(new Dimension(bWidth, bHeight));
        btnOK.setMaximumSize(new Dimension(bWidth, bHeight));
        btnOK.setMinimumSize(new Dimension(bWidth, bHeight));

        btnABORT.setPreferredSize(new Dimension(bWidth, bHeight));
        btnABORT.setMaximumSize(new Dimension(bWidth, bHeight));
        btnABORT.setMinimumSize(new Dimension(bWidth, bHeight));

        int width = 100;
        btnDOWN.setSize(width, btnDOWN.getHeight());
        btnDELETE.setSize(width, btnDELETE.getHeight());
        btnUP.setSize(width, btnUP.getHeight());

        String iconFileName = "icon32.png";
        IconManager im = new IconManager();
        this.setIconImage(im.createImageIcon(iconFileName).getImage());

        Box prebox = Box.createVerticalBox();
        Box mbox = Box.createHorizontalBox();
        Box vbox01 = Box.createVerticalBox(); // for whitepanel: vbox03|04
        Box vbox02 = Box.createVerticalBox(); // for btn´s
        Box hbox03 = Box.createHorizontalBox(); // for btn Ok Abort
        Box hbox04 = Box.createHorizontalBox();

//        hbox03.setBorder(new LineBorder(Color.BLACK));
//        vbox02.setBorder(new LineBorder(Color.BLUE));
//        vbox01.setBorder(new LineBorder(Color.GREEN));
//        mbox.setBorder(new LineBorder(Color.RED));
//        prebox.setBorder(new LineBorder(Color.MAGENTA));
        int tWidth = 300;
        int tHeight = 300;
        sptable.setMinimumSize(new Dimension(tWidth, tHeight));
        sptable.setPreferredSize(new Dimension(tWidth, tHeight));
        sptable.setMaximumSize(new Dimension(tWidth, tHeight));

        int vbox02Width = 200;
        int vbox02height = 300;

        vbox02.setMinimumSize(new Dimension(vbox02Width, vbox02height));
        vbox02.setPreferredSize(new Dimension(vbox02Width, vbox02height));
        vbox02.setMaximumSize(new Dimension(vbox02Width, vbox02height));

        vbox01.setMaximumSize(new Dimension(tWidth, tHeight));

        hbox03.setMinimumSize(new Dimension(500, 100));
        hbox03.setPreferredSize(new Dimension(500, 100));
        hbox03.setMaximumSize(new Dimension(500, 100));

        this.add(mainpanel);
        mainpanel.add(prebox);

        prebox.add(mbox);

        mbox.add(Box.createHorizontalStrut(35));
        mbox.add(vbox01);
        mbox.add(Box.createHorizontalStrut(35));
        mbox.add(vbox02);
        mbox.add(Box.createHorizontalStrut(35));
        mbox.add(Box.createHorizontalGlue());

        vbox01.add(sptable);
        vbox01.add(Box.createVerticalGlue());

        vbox02.add(Box.createVerticalStrut(35));
        vbox02.add(btnUP);
        vbox02.add(Box.createVerticalStrut(25));
        vbox02.add(btnDOWN);
        vbox02.add(Box.createVerticalStrut(25));
        vbox02.add(btnDELETE);

        prebox.add(hbox04);

        hbox04.add(hbox03);
        hbox04.add(Box.createGlue());

        hbox03.add(Box.createHorizontalGlue());
        hbox03.add(btnOK);
        hbox03.add(Box.createHorizontalStrut(35));
        hbox03.add(btnABORT);
        hbox03.add(Box.createHorizontalStrut(35));

        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - this.getSize().width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - this.getSize().height) / 2;
        this.setBounds(x, y, 500, 500);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
    }

    private void loadTableStructure() {

        //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        dtm = new DefaultTableModel(0, 0);
        String[] head = {"Nr.", "Viewmode", "ColumnCount", "Columns"};
        dtm.setColumnIdentifiers(head);
        table.setModel(dtm);
        table.setBackground(Color.WHITE);
        table.setFillsViewportHeight(true);
        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);
        table.getTableHeader().setName("Header");
        table.getColumnModel().addColumnModelListener(this);

        // Erste Spalte Grau machen:
        // ToDO.
    }

    private void insertTableValues() {

        String[] rowData = new String[dtm.getColumnCount()];
        // Für jeden Viewmodi:
        for (int i = 0; i < vm.getvList().size(); i++) {
            rowData[0] = String.valueOf(i);
            rowData[1] = vm.getvList().get(i).getCaption();
            rowData[2] = String.valueOf(vm.getvList().get(i).getColumnCode().length());
            dtm.addRow(rowData);
        }
        // Erste Spalte an Inhalt anpassen:
        setColumnSizeDependsOnContent(0);
    }

    /**
     * SetColumnWidth depending on content.
     *
     * @param column int
     */
    private void setColumnSizeDependsOnContent(int column) {

        int length = 0;
        // Für jede Zeile:
        for (int i = 0; i < dtm.getRowCount(); i++) {
            if (String.valueOf(dtm.getValueAt(i, column)).length() > length) {
                length = String.valueOf(dtm.getValueAt(i, column)).length();
            }
        }
        length = length + 20;
        table.getColumnModel().getColumn(column).setPreferredWidth(length);
        table.getColumnModel().getColumn(column).setMaxWidth(length);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Wenn der Up-Button gedrückt wurde:
        if (e.getActionCommand().equals("up")) {
            // Tausche spalten aus:
            int newRowPos = table.getSelectedRow() - 1;
            this.vm.exchangeVM(table.getSelectedRow(), newRowPos);

            // Neu zeichnen:
            loadTableStructure();
            insertTableValues();
            // verschobene Zeile neu markieren:
            table.changeSelection(newRowPos, 0, false, false);
        }
        // Wenn der Down-Button gedrückt wurde:
        if (e.getActionCommand().equals("down")) {
            int newRowPos = table.getSelectedRow() + 1;
            this.vm.exchangeVM(table.getSelectedRow(), newRowPos);
            // Neu zeichnen:
            loadTableStructure();
            insertTableValues();
            // verschobene Zeile neu markieren:
            table.changeSelection(newRowPos, 0, false, false);
        }
        // Wenn der Delete-Button gedrückt wurde:
        if (e.getActionCommand().equals("delete")) {
            String iconName = "icon32.png";
            IconManager im = new IconManager();
            Image img = im.createImageIcon(iconName).getImage();

            this.setAlwaysOnTop(false);
            int result = JOptionPane.showConfirmDialog(null, "Do you really want delete the selected Viewmode(s)?", "Question:", JOptionPane.OK_OPTION, JOptionPane.YES_NO_OPTION, new ImageIcon(img));

            // Wenn das Löschen ab verneint oder abgebrochen wurde:
            if (result == JOptionPane.NO_OPTION || result == JOptionPane.CANCEL_OPTION) {
                vm.setvList(bavList);
            }
            // Wenn das Löschen bestätigt wurde:
            if (result == JOptionPane.YES_OPTION) {
                // lösche markierte zeile:
                this.vm.deleteViewmodi(table.getSelectedRows());

                // Neu zeichnen:
                loadTableStructure();
                insertTableValues();
            }

            this.setAlwaysOnTop(true);
        }
        // Wenn ok geklickt wurde:
        if (e.getActionCommand().equals("ok")) {
            mb.close();

            // save acutal list in property-file:
            vm.writeALLViewmodi();
            maincontroller.updateViewModiList();
            this.dispose();
        }
        // if abort is pressed:
        if(e.getActionCommand().equals("abort")){
            this.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        mb.close();
        this.dispose();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void columnAdded(TableColumnModelEvent e) {
    }

    @Override
    public void columnRemoved(TableColumnModelEvent e) {
    }

    @Override
    public void columnMoved(TableColumnModelEvent e) {
    }

    @Override
    public void columnMarginChanged(ChangeEvent e) {
    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e) {
    }
}
