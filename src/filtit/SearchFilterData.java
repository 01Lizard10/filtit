package filtit;

/**
 *
 * @author Hans-Georg Schladitz
 */
class SearchFilterData implements Comparable<SearchFilterData> {

    /**
     * Document type suffix. Example: ".txt"
     */
    private String docType;
    /**
     * Is TRUE, if Usere choosed the docType.
     */
    private Boolean isChecked;
    /**
     * Handle-Methode for the docetype.
     */
    private String handleMethode;
    /**
     * Contains the minimum document size in Byte. Contains the content of the
     * TA in Byte.
     */
    private String minDocSize;
    /**
     * Contains the maximum document size in Byte. Contains the content of the
     * TA in Byte.
     */
    private String maxDocSize;
    /**
     * Integer Index for the choosen Size: {"Byte", "KB", "MB", "GB", "TB",
     * "PB"};
     */
    private int minCB_Value;
    /**
     * Integer Index for the choosen Size: {"Byte", "KB", "MB", "GB", "TB",
     * "PB"};
     */
    private int maxCB_Value;
    /**
     * String in teaxtarea for the minCB_Value. minCB_Value and minTA_Value
     * together can be used tu calc the document size in Byte.
     */
    private String minTA_Value;
    /**
     * String in teaxtarea for the minCB_Value. minCB_Value and minTA_Value
     * together can be used tu calc the document size in Byte.
     */
    private String maxTA_Value;
    /**
     * Contains the content of the TA-Min.
     */
    private String globalMinTA;
    /**
     * Contains the content of the TA-Max.
     */
    private String globalMaxTA;
    /**
     * Contains the index of the Combox-Min.
     */
    private int globalMinCB;
    /**
     * Contains the index of the Combox-Max.
     */
    private int globalMaxCB;

    // Getter & Setter -------------------------------------------------------
    public SearchFilterData() {
        docType = "";
        isChecked = false;
        handleMethode = "0";

        // global MinMax:
        globalMinTA = "";
        globalMaxTA = "";
        globalMinCB = 0;
        globalMaxCB = 0;

        minCB_Value = 0;
        maxCB_Value = 0;
        minTA_Value = "";
        maxTA_Value = "";

        maxDocSize = "";
        minDocSize = "";
    }

    /**
     *
     * @return DocType as String like ".rar", ".zip" etc.
     */
    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getHandleMethode() {
        return handleMethode;
    }

    public void setHandleMethode(String handleMethode) {
        this.handleMethode = handleMethode;
    }

    public String getMinDocSize() {
        return minDocSize;
    }

    public void setMinDocSize(String minDocSize) {
        this.minDocSize = minDocSize;
    }

    public String getMaxDocSize() {
        return maxDocSize;
    }

    public void setMaxDocSize(String maxDocSize) {
        this.maxDocSize = maxDocSize;
    }

    public int getMinCB_Value() {
        return minCB_Value;
    }

    public void setMinCB_Value(int minCB_Value) {
        this.minCB_Value = minCB_Value;
    }

    public int getMaxCB_Value() {
        return maxCB_Value;
    }

    public void setMaxCB_Value(int maxCB_Value) {
        this.maxCB_Value = maxCB_Value;
    }

    public String getMinTA_Value() {
        return minTA_Value;
    }

    public void setMinTA_Value(String minTA_Value) {
        this.minTA_Value = minTA_Value;
    }

    public String getMaxTA_Value() {
        return maxTA_Value;
    }

    public void setMaxTA_Value(String maxTA_Value) {
        this.maxTA_Value = maxTA_Value;
    }

    public String getGlobalMinTA() {
        return globalMinTA;
    }

    public void setGlobalMinTA(String globalMinTA) {
        this.globalMinTA = globalMinTA;
    }

    public String getGlobalMaxTA() {
        return globalMaxTA;
    }

    public void setGlobalMaxTA(String globalMaxTA) {
        this.globalMaxTA = globalMaxTA;
    }

    public int getGlobalMinCB() {
        return globalMinCB;
    }

    public void setGlobalMinCB(int globalMinCB) {
        this.globalMinCB = globalMinCB;
    }

    public int getGlobalMaxCB() {
        return globalMaxCB;
    }

    public void setGlobalMaxCB(int globalMaxCB) {
        this.globalMaxCB = globalMaxCB;
    }

    @Override
    public int compareTo(SearchFilterData o) {
        return this.docType.compareTo(o.docType);
    }

    /**
     * Check if a SFD has the same values like another.
     *
     * @param sfd
     * @return
     */
    public boolean equals(SearchFilterData sfd) {

        if (sfd == null) {
            return false;
        }
        if (!this.docType.equals(sfd.docType)) {
            return false;
        }
        if (!this.isChecked == sfd.getIsChecked()) {
            return false;
        }
        if (!this.handleMethode.equals(sfd.getHandleMethode())) {
            return false;
        }
        if (!this.globalMinTA.equals(sfd.getGlobalMinTA())) {
            return false;
        }
        if (!this.globalMaxTA.equals(sfd.getGlobalMaxTA())) {
            return false;
        }
        if (this.globalMinCB != sfd.getGlobalMinCB()) {
            return false;
        }
        if (this.globalMaxCB != sfd.getGlobalMaxCB()) {
            return false;
        }
        if (this.minCB_Value != sfd.getMinCB_Value()) {
            return false;
        }
        if (this.maxCB_Value != sfd.getMaxCB_Value()) {
            return false;
        }
        if (!this.minTA_Value.equals(sfd.getMinTA_Value())) {
            return false;
        }
        if (!this.maxTA_Value.equals(sfd.getMaxTA_Value())) {
            return false;
        }
        if (!this.minDocSize.equals(sfd.getMinDocSize())) {
            return false;
        }
        if (!this.maxDocSize.equals(sfd.getMaxDocSize())) {
            return false;
        }
        return true;
    }
}
