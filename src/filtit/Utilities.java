package filtit;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class Utilities {

    /**
     * Hilfsfunktion, welche eine ArrayList ausgibt.
     *
     * @param al
     */
    static void printAL(ArrayList al) {
        for (int i = 0; i < al.size(); i++) {
            System.out.println("i: " + i + " -> " + String.valueOf(al.get(i)));
        }
    }

    public static ArrayList<Integer> castStringToArrayList(String list) {
        // Wenn es keine Spalte im SpaltenCode gibt:
        if (list.equals("")) {
            return new ArrayList();
        }
        // Bei existierenden Spalten im SpaltenCode:
        int ipos = list.indexOf(",");
        ArrayList<Integer> al = new ArrayList();
        while (ipos >= 0) {
            al.add(Integer.valueOf(list.substring(0, ipos)));
            list = list.substring(ipos + 1);
            ipos = list.indexOf(",");
        }
        al.add(Integer.valueOf(list.substring(ipos + 1)));

        return al;
    }

    public static String castArrayListToString(ArrayList al) {
        String result = "";
        for (int i = 0; i < al.size(); i++) {
            if (i == al.size() - 1) {
                result = result + al.get(i);
                return result;
            }
            result = result + al.get(i) + ",";
        }
        return result;
    }

    /**
     * Hilfsfunktion, welche ein Array ausgibt.
     *
     * @param columns
     */
    static void printArray(String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            System.out.println("i: " + i + " -> " + String.valueOf(columns[i]));
        }
    }

    /**
     * Exchangeing the positions of two items in one List. Example: al =
     * [1,2,3,4,5] -> al = switchItemInArraylist(al,0,3); -> al = [4,2,3,1,5]
     *
     * @param al
     * @param pos1
     * @param pos2
     */
    static ArrayList switchItemInArraylist(ArrayList al, int pos1, int pos2) {
        if (pos1 == pos2) {
            return al;
        }
        if (pos1 >= al.size() || pos2 >= al.size()) {
            return al;
        }
        Object o1 = al.get(pos1);
        Object o2 = al.get(pos2);
        al.set(pos1, o2);
        al.set(pos2, o1);
        return al;
    }

    public static void main(String[] args) {
        ArrayList al = new ArrayList();
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(4);
        al.add(5);
        System.out.println(al);
        al = switchItemInArraylist(al, 0, 3);
        System.out.println(al);

    }

    /**
     * Erstellt aus einem gegebenen Array mit verschiedenen Files eine neue
     * Liste, die gleichmäßig verteilt ist. Jede Teilliste in der Liste enthält
     * 'partLength' viele Elemente. Es darf maximal 'maxParts' viele Teillisten
     * geben. Ist 'maxParts' erreicht und Elemente noch nicht zugeordnet, wird
     * der letzten Teillsite alles angehangen. Beispiel: partLength = 2,
     * array.lengt = 9, maxParts = 4, bei eine
     * ->[H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\settings.7z,
     * H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\Settings.zip]
     * ->[H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\test.txt,
     * H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\test2.xml]
     * ->[H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\testFolder,
     * H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\TestFolder.rar]
     * ->[H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\testFolder2,
     * H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\testInI.ini,
     * H:\DropBox\Programming\NetBeansProjects\FiltIt\TestFolder\tram2.pdf]
     *
     * @param array
     * @param partLength
     * @param maxParts
     * @return
     */
    public static ArrayList<ArrayList> calcEvenList(File[] array, int partLength, int maxParts) {
        ArrayList result = new ArrayList();

        ArrayList tmpList = new ArrayList();
        int counter = 0;
        for (int i = 0; i < array.length; i++) {

            counter++;
            tmpList.add(array[i]);
            if (counter >= partLength
                    && result.size() < maxParts
                    && i + partLength < array.length) {

                result.add(tmpList);
                tmpList = new ArrayList();
                counter = 0;
            }
            if (counter >= partLength && result.size() >= maxParts) {

            }
        }
        result.add(tmpList);

        return result;
    }

    /**
     * Analog to calcEvenList(File[] array, int partLength, int maxParts) .
     *
     * @param al
     * @param partLength
     * @param maxParts
     * @return
     */
    public static ArrayList<ArrayList<Data>> calcEvenList(ArrayList<Data> al, int partLength, int maxParts) {
        ArrayList<ArrayList<Data>> result = new ArrayList();
        ArrayList<Data> tmpList = new ArrayList();

        int counter = 0;
        for (int i = 0; i < al.size(); i++) {

            counter++;
            tmpList.add(al.get(i));
            if (counter >= partLength
                    && result.size() < maxParts
                    && i + partLength < al.size()) {

                result.add(tmpList);
                tmpList = new ArrayList();
                counter = 0;
            }
            if (counter >= partLength && result.size() >= maxParts) {

            }
        }
        result.add(tmpList);

        return result;
    }
}

/**
 * Interessante Links: *
 * http://www.dreamincode.net/forums/topic/22739-message-dialogs-in-java/
 * http://de.wikibooks.org/wiki/Java_Standard:_Grafische_Oberfl%C3%A4chen_mit_Swing:_Top_Level_Container:_javax_swing_JOptionPane
 */
