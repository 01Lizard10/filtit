package filtit;

import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.codegen.CompilerConstants;

/**
 *
 * @author Hans-Georg Schladitz
 */
class SearchThread extends Thread {

    /**
     * FiltIt model.
     */
    private FiltItModel model;
    /**
     * Temp-Model to avoid thread-problems.
     */
    private FiltItModel tempModel;
    /**
     * FiltIt controller.
     */
    private FiltItController controller;

    /**
     * Defaultconstructor.
     */
    public SearchThread(FiltItModel model, FiltItController controller) {
        this.model = model;
        this.controller = controller;

        tempModel = new FiltItModel();

        tempModel.getVM().setActualViewmodi(model.getVM().getActualViewmodi());
        tempModel.setFind(model.getFind());
        tempModel.setFolderPath(model.getFolderPath());

    }

    @Override
    public void run() {

        // In-Progress-Message:
        model.setSearchState(0);

        //model.notifyObservers("progressbarUpdate");
        controller.setProgressbar(this.model.getProgressValue(), model.getDataModel().getDatalist().size());


        // Methode starten:
        tempModel.searchFind(model, controller);

        // Ready-Message:
        model.setSearchState(1);
        controller.setProgressbar(this.model.getProgressValue(), model.getDataModel().getDatalist().size());
        if (isInterrupted()) {
            model.setSearchState(2);
            controller.setProgressbar(this.model.getProgressValue(), model.getDataModel().getDatalist().size());

        }
        // Set results from tempmodel in the current model:
        model.getDataModel().setDatalist(tempModel.getDataModel().getDatalist());

        controller.update();
    }
}
