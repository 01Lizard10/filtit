package filtit;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class TabbedPanel {

    private JTabbedPane tabbedPane;
    private JScrollPane sp02;

    private ArrayList<JComponent> panelAL;

    public TabbedPanel(FiltItController controller) {
        init(controller);
    }

    public JScrollPane getSp() {
        return sp02;
    }

    private JTabbedPane init(FiltItController controller) {

        panelAL = new ArrayList();
        tabbedPane = new JTabbedPane();
        tabbedPane.setMinimumSize(new Dimension(500, 400));
        tabbedPane.setPreferredSize(new Dimension(500, 310)); // Komischer Bug: die breite ändert sich ab und an mal leicht.
        tabbedPane.setMaximumSize(new Dimension(Short.MAX_VALUE, Short.MAX_VALUE));

        IconManager im = new IconManager();
        ImageIcon icon = im.createImageIcon("icon16.png");

        JComponent panel1 = new JPanel(new BorderLayout());
        JComponent panel2 = new JPanel(new BorderLayout());
        JComponent panel3 = new JPanel(new BorderLayout());
        JComponent panel4 = new JPanel(new BorderLayout());

        tabbedPane.addTab("Table", icon, panel1, "Site 1");
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
        panelAL.add(panel1);

        sp02 = new JScrollPane(panel2, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        tabbedPane.addTab("Search/Filter options", icon, sp02, "Site 2");
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
        panelAL.add(panel2);

        tabbedPane.addTab("Application options", icon, panel3, "Site 3");
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
        panelAL.add(panel3);

        //The following line enables to use scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        return tabbedPane;
    }

    /**
     * Getter für das TabbedPane.
     *
     * @return JTabbedPane
     */
    public JTabbedPane getTabbedPanel() {
        return tabbedPane;
    }

    /**
     * Getter für eine Liste von Panels. Jedes Tab hat ein eigenes Panel. Diese
     * werden als Liste zurückgegeben.
     *
     * @return ArrayList PanelListe
     */
    public ArrayList<JComponent> getPanelAL() {
        return panelAL;
    }

    /**
     * Setter für die ArrayListe, die alle TabPane´s enthält.
     *
     * @param panelAL ArrayList
     */
    public void setPanelAL(ArrayList<JComponent> panelAL) {
        this.panelAL = panelAL;
    }
}
