package filtit;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.compress.archivers.ArchiveException;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class SearchEngine {
    private File path;
    private ArrayList<Data> templist = new ArrayList();
    private FiltItModel model;

    /**
     * Constructor.
     *
     * @param actualModel FiltItModel.
     */
    SearchEngine(FiltItModel actualModel) {
        model = actualModel;
    }

    /**
     * Suchen nach allen ordnern mit Unterordnern. Teil 1 (Rekursion).
     *
     * @return ArrayList
     */
    public synchronized ArrayList<Data> getAllDirectoriesAndFiles() {
        templist.clear();
        Data data;

        File[] dics = path.listFiles();
        if (dics != null) {
            for (File dic : dics) {
                if (dic.isDirectory() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("folder");
                    templist.add(data);
                    getAllDirectoriesAndFiles(dic.getPath());
                }
                if (dic.isFile() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("file");
                    templist.add(data);
                }
            }
        }
        return templist;
    }

    /**
     * Suchen nach allen ordnern mit Unterordnern. Teil 2 (Rekursion).
     *
     * @param dicPfad String
     */
    public synchronized void getAllDirectoriesAndFiles(String dicPfad) {

        File temp = new File(dicPfad);
        File[] dics = temp.listFiles();
        Data data;
        if (dics != null) {
            for (File dic : dics) {
                if (dic.isDirectory() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("folder");
                    templist.add(data);
                    getAllDirectoriesAndFiles(dic.getPath());
                }
                if (dic.isFile() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("file");
                    templist.add(data);
                }
            }
        }
    }

    public String readWholeFile(String path) {
        int c;
        String result = "";
        try {
            FileReader f = new FileReader(path);
            while ((c = f.read()) != -1) { // liest zeichenweise int-Werte aus Datei bis -1 kommt
                //System.out.print((char) c);  // wandelt int-Wert in char-Wert               
                result = result + String.valueOf((char) c);
            }
            f.close();
        } catch (IOException e) {
            System.out.println("Error: " + e.toString());
        }
        return result;
    }
    /**
     * Skip the file, if the option for the filetype dont fits.
     * @param vm
     * @param hType
     * @return 
     */
    private Data isSkip(Data e, ViewmodiManagement vm, int hType) {
    String reason01 = "Out of global min-value;";
    String reason02 = "Out of global max-value;";
    String reason03 = "unchecked filetype/filehandlemethode;";
    String reason04 = "Out of filetype min-value;";
    String reason05 = "Out of filetype max-value;";
        Data entity = e;
        // Skip if the entity is out of global min max scope:
       SearchFilterData gsfd = model.getVM().getActualViewmodi().getGlobalMinMax();
        if (gsfd != null) {
            double min, max;

            // Minvariable ermitteln:
            if (gsfd.getGlobalMinTA().equals("")) {
                min = 0;
            } else {
                min = Double.valueOf(gsfd.getMinDocSize());
            }
            // Maxvariable ermitteln:
            if (gsfd.getGlobalMaxTA().equals("")) {
                max = 0;
            } else {
//                max = Double.valueOf(gsfd.getMaxDocSize());
                max = Double.valueOf(gsfd.getMaxDocSize());
            }
            // Wenn die Dateigröße kleiner als die untere Schranke ist:
            if (Double.valueOf(entity.getSize()) < min) {
                entity.setRejectionReason(entity.getRejectionReason() + reason01);
            }
            // Wenn max == 0 ist, heißt dies, dass alle Werte zugelassen werden sollen:
            if (max != 0 && Double.valueOf(entity.getSize()) > max) {
                entity.setRejectionReason(entity.getRejectionReason() + reason02);
            }
        }


        SearchFilterData sfd;
        double min, max;
        sfd = vm.getSfdFromActualViewmodiForFiletypeAndHandlemodus(entity.getFiletype(), hType);
        if (sfd == null) {
            return entity;
        }

        // Wenn es keine Option für die Datei gibt wird  geskipt:
        // Wenn der Dateityp mit handletype kein Haken hat:
        // D.h. es soll nicht in der Datei gesucht werden, aber die Datei soll noch in der Ergebnisliste sein.
        if (sfd.getIsChecked() == false) {
//            entity.setFind("N/A");
//            entity.setFoundInContent("N/A");            
            //return true;

            entity.setRejectionReason(entity.getRejectionReason() + reason03);

        }
        // Wenn der Dateityp mit handletype kein Haken hat:
        // D.h. es soll nicht in der Datei gesucht werden, aber die Datei soll noch in der Ergebnisliste sein.

        if (sfd.getMinDocSize() == null || sfd.getMinDocSize().equals("")) {
            min = 0;
        } else {
            min = Double.valueOf(sfd.getMinDocSize());
        }
        if (sfd.getMaxDocSize() == null || sfd.getMaxDocSize().equals("")) {
            max = 0;
        } else {
            max = Double.valueOf(sfd.getMaxDocSize());
        }

        if (Double.valueOf(entity.getSize()) < min) {
            //return true;
            entity.setRejectionReason(entity.getRejectionReason() + reason04);            
        }

        // Wenn max == 0 ist, heißt dies, dass alle Werte zugelassen werden sollen:
        if (max != 0 && Double.valueOf(entity.getSize()) > max) {
            //return true;
            entity.setRejectionReason(entity.getRejectionReason() + reason05);
        }


        return entity;
    }

    /**
     * True if the entity (Data) ist out of the global min max scope from model.
     *
     * @param entity
     * @return
     */
//    private boolean isOutOfGlobalMinMaxScopeSkip(Data entity) {
//        SearchFilterData sfd = model.getVM().getActualViewmodi().getGlobalMinMax();
//        if (sfd != null) {
//            double min, max;
//
//            // Minvariable ermitteln:
//            if (sfd.getGlobalMinTA().equals("")) {
//                min = 0;
//            } else {
//                min = Double.valueOf(sfd.getMinDocSize());
//            }
//            // Maxvariable ermitteln:
//            if (sfd.getGlobalMaxTA().equals("")) {
//                max = 0;
//            } else {
//                max = Double.valueOf(sfd.getMaxDocSize());
//            }
//
//            // Wenn die Dateigröße kleiner als die untere Schranke ist:
//            if (Double.valueOf(entity.getSize()) < min) {
//                return true;
//            }
//            // Wenn max == 0 ist, heißt dies, dass alle Werte zugelassen werden sollen:
//            if (max != 0 && Double.valueOf(entity.getSize()) > max) {
//                return true;
//            }
//        }
//
//        return false;
//    }

    // ------------------------------------
    /**
     * Maincontentsearch. Defs a file as a special filetype. So you can for
     * example hande a xml-file as a txt-file.
     *
     *
     * @param entity
     * @param find
     * @return Data
     */
    public Data searchWithModi(Data entity, String find) {

        ViewmodiManagement vm = model.getVM();
        // Defs.
        SearchEngine se = new SearchEngine(model);

        // List of all foundpositions in the filecontent:
        ArrayList<Integer> contentFoundList;

        // list of all handleTypes for the actual filetype:
        ArrayList<Integer> handletypeList = model.getModiForFiletype(
                entity.getFiletype(), 
                vm.getActualViewmodi().getSFD_List()
        );


        // Für jeden handle modi:
        for (int i = 0; i < handletypeList.size(); i++) {

            switch (model.getModiForFiletype(entity.getFiletype(), vm.getActualViewmodi().getSFD_List()).get(i)) {
                case 0: // Textsearch:
                    
                    if (!isSkip(entity, vm, 0).getRejectionReason().equals("")) {
                        //return entity;
                        continue;
                    }

                    // Runs.
                    contentFoundList = se.searchInTextFile(entity, find);

                    // Sets.
                    entity.addContentFoundList(contentFoundList);

                    continue;
                case 1: // PDF-Search:
                    // Skip if the size are out of scope:
                    if (!isSkip(entity, vm, 1).getRejectionReason().equals("")) {
                        return entity;
                    }

                    // Runs.
                    contentFoundList = se.searchInPDFFile(entity, find);

                    // Sets.
                    entity.addContentFoundList(contentFoundList);
                    continue;
                case 2:
                    //System.out.println("Zip-Searchmodus for: " + entity.getName());
                    // Skip if the size are out of scope:
                    if (!isSkip(entity, vm, 2).getRejectionReason().equals("")) {
                        continue;
                    }

                    contentFoundList = se.searchInCompressFile("zip", entity, find, vm);
                    entity.addContentFoundList(contentFoundList);
                    continue;
                case 3:
                    //System.out.println("Rar-Searchmodus for: " + entity.getName());
                    // Skip if the size are out of scope:
                    if (!isSkip(entity, vm, 3).getRejectionReason().equals("")) {
                        continue;
                    }

                    contentFoundList = se.searchInCompressFile("rar", entity, find, vm);
                    entity.addContentFoundList(contentFoundList);
                    continue;
                case 4:
                    //System.out.println("7z-Searchmodus for: " + entity.getName());
                    // Skip if the size are out of scope:
                    if (!isSkip(entity, vm, 4).getRejectionReason().equals("")) {
                        continue;
                    }

                    contentFoundList = se.searchInCompressFile("7z", entity, find, vm);

                    entity.addContentFoundList(contentFoundList);
                    continue;
                case 5:
                    System.out.println("marker0");
                    // Skip if the size are out of scope:
                    if (!isSkip(entity, vm, 0).getRejectionReason().equals("")) {
                        continue;
                    }
                    System.out.println("marker1");
                    //System.out.println("Jar-Searchmodus for: " + entity.getName());
                    contentFoundList = se.searchInCompressFile("jar", entity, find, vm);

                    entity.addContentFoundList(contentFoundList);
                    continue;
                default:
                    //System.out.println("Default:" + fm.getModiForFiletype(entity.getFiletype()));
                    continue;
            }
        }

        return entity;
    }

    /**
     * Searchin in a file. It is assumed that the file is a text file.
     *
     * @param entity
     * @param find
     * @return ArrayList with foundpositions.
     */
    private ArrayList<Integer> searchInTextFile(Data entity, String find) {

        String tmp = this.readWholeFile(entity.getFullpath());

        int rest = 0;
        int ipos = tmp.indexOf(find);
        ArrayList<Integer> contentFoundList = new ArrayList();
        while (ipos >= 0) {
            contentFoundList.add(ipos + rest);
            tmp = tmp.substring(ipos + find.length());
            rest = rest + ipos + find.length();
            ipos = tmp.indexOf(find);
        }
        //System.out.println("FoundCount for: " + entity.getName() + " -> " + contentFoundList.size());
        return contentFoundList;
    }

    /**
     * Searchin in a file. It is assumed that the file is a pdf-file.
     *
     * @param entity
     * @param find
     * @return ArrayList with foundpositions.
     */
    private ArrayList<Integer> searchInPDFFile(Data entity, String find) {
        PDFManager pdfm = new PDFManager();
        String tmp = pdfm.readPDF(entity.getFullpath());
        ArrayList< Integer> contentFoundList = new ArrayList();
        int rest = 0;
        int ipos = tmp.indexOf(find);
        while (ipos >= 0) {
            contentFoundList.add(ipos + rest);
            tmp = tmp.substring(ipos + find.length());
            rest = rest + ipos + find.length();
            ipos = tmp.indexOf(find);
        }
        return contentFoundList;
    }

    /**
     * Searching in a compressing File: Jar, Zip, Rar.
     *
     * @param fileIs String der für einen Dateityp steht, welcher bewirkt,dass
     * die aktuelle Datei in entity wie eine Datei des angegebenen Datentyps
     * behandelt wird.
     * @param entity Datensatz für eine Datei/Ordner.
     * @param find Suchausdruch
     * @return ArrayList mit den Positionen der gefundenen Suchausdrücke
     * innerhalb der Datei.
     */
    private ArrayList<Integer> searchInCompressFile(String fileIs, Data entity, String find, ViewmodiManagement vm) {

        String dir = System.getProperty("user.dir");
        int index = 1;
        File tempfolderFile = new File(dir + File.separator + "tempfolder" + index);

        // if the temp folder already exists:
        while (tempfolderFile.exists()) {
            index++;
            tempfolderFile = new File(dir + File.separator + "tempfolder" + index);
        }
        // Tempordner erstellen:
        if (!tempfolderFile.exists()) {
            tempfolderFile.mkdir();
        }

        // Wenn es eine Zip-File oder Jar-File ist:
        if (fileIs.equals("zip")) {
            try {
                // Entpacken
                CompressionManager cm = new CompressionManager();
                cm.extractArchiveZip(new File(entity.getFullpath()), tempfolderFile);

            } catch (ArchiveException | IOException ex) {
                System.err.println("ERROR: ZIP-Error");
                tempfolderFile.delete();
                Logger.getLogger(SearchEngine.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

        // Wenn es eine Zip-File oder Jar-File ist:
        if (fileIs.equals("jar")) {
            CompressionManager cm = new CompressionManager();
            cm.extractArchiveZipJar(new File(entity.getFullpath()), tempfolderFile);
        }

        // Wenn die Datei als Rare-File angegeben wurde:
        if (fileIs.equals("rar")) {
            CompressionManager cm = new CompressionManager();
            cm.extractArchiveRar(new File(entity.getFullpath()), tempfolderFile);
        }

        // Wenn die Datei als Rare-File angegeben wurde:
        if (fileIs.equals("7z")) {
            CompressionManager cm = new CompressionManager();
            cm.extractArchive7z(new File(entity.getFullpath()), tempfolderFile);
        }

        // Dann die entpackten dateien durchsuchen
        ArrayList< Integer> contentFoundList = new ArrayList();

        SearchEngine se = new SearchEngine(model);

        // Suchen im Tempordner:       
        se.setFolder(tempfolderFile.getPath());
        entity.setChildList(se.getAllDirectoriesAndFiles());

        // Für jedes Kind-Element:
        for (int i = 0; i < entity.getChildList().size(); i++) {

            // Für jede gefundene Datei die Werte ermitteln:               
            entity.getChildList().get(i).setFind(find);

            // this entity is virtual:
            entity.getChildList().get(i).setIsVirtual("true");

            entity.getChildList().get(i).determineValues();

            // Rekursiv wieder in diesen Dateien suchen:
            entity.getChildList().set(i, se.searchWithModi(entity.getChildList().get(i), find));

            // Eltern-Element anpassen:
            if (entity.getChildList().get(i) != null) {
                if (entity.getChildList().get(i).getFoundCountInFile().length() > 0) {
                    entity.setFoundInContent("true");
                }
            }
        }

        // entpackte dateien wieder löschen
        return contentFoundList;

    }

    /**
     *
     * @param path
     */
    public void deleteDir(File path) {
        for (File file : path.listFiles()) {
            if (file.isDirectory()) {
                deleteDir(file);
            }
            file.delete();
        }
        path.delete();
    }

    // Getter & Setter:
    public void setFolder(String folderPath) {
        this.path = new File(folderPath);
    }
}
