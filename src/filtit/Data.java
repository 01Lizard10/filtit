package filtit;

import java.io.File;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;

/**
 * Speichert alle Daten einer Ressource (File, Folder) ab.
 *
 * @author Hans-Georg Schladitz
 */
public class Data {

    /**
     * "file" or "folder".
     */
    private String type;
    /**
     * Last modified timestamp.
     */
    private String lastmodified;
    /**
     * Name of the ressource(folder or file). Example: "example.txt"
     */
    private String fullname;
    /**
     * Name of the folder or file without path and without type. Example:
     * example
     */
    private String name;
    /**
     * Searchexpression. Example: "Kohl -Bundeskanzler" -> Regular Expression!!!
     * NOT find
     */
    private String findexpression;
    /**
     * Timestamp at fundtime.
     */
    private String findTime;
    /**
     * File- or Foldersize in byte.
     */
    private String size;
    /**
     * Whole path to the ressource (file or folder). Example:
     * "C:\Folder1\example.txt"
     */
    private String fullpath;
    /**
     * Datatyp of the file, if Data is a file. Example: txt, rar, zip, pdf
     */
    private String filetype;
    /**
     * Count matches depending from findeexpression.
     */
    private String MatchCount;
    /**
     * path to the file or folder without name;
     */
    private String path;
    /**
     * Search-Expression for pathFoundList and contentFoundList;
     */
    private String find;
    /**
     * SystemIcon.
     */
    private Icon systemIcon;
    /**
     * "true" or "false". Is "true", if find was Found in the file.
     */
    private String isFoundInContent;
    /**
     * List of all founds in path. Each value is an int stands for the position
     * in String.
     */
    private ArrayList<Integer> pathFoundList;
    /**
     * List of all founds in filecontent. Each value is an int stands for the
     * position in String.
     */
    private ArrayList<Integer> contentFoundList;
    /**
     * List of all containing files.
     */
    private ArrayList<Data> childList;
    /**
     * Is true if the file is virtual. A File is virtual if they son exist after the search. Means Files in another file.
     */
    private String isVirtual;
    /**
     * Containts the rejection reason for the element.
     */
    private String rejectionReason;
    /**
     * Constructor for Data.
     *
     * @param path String
     */
    public Data(String path) {
        fullpath = path;
        isFoundInContent = "";
        pathFoundList = new ArrayList();
        contentFoundList = new ArrayList();
        childList = new ArrayList();
        isVirtual = "false";
        rejectionReason = "";
    }

    public void searchInPath(String find) {

        this.find = find;
        String tmp = fullpath;
        int rest = 0;
        int ipos = tmp.indexOf(find);

        while (ipos >= 0) {
            pathFoundList.add(ipos + rest);
            tmp = tmp.substring(ipos + find.length());
            rest = rest + ipos + find.length();
            ipos = tmp.indexOf(find);
        }
    }

    /**
     * Searching in Files. Using only file from filetypList like "txt", "xml".
     *
     * @param find String Searchexpression
     * @param filetypList String List of Strings with Filetypes.
     */
    public void searchInContent(String find, ArrayList<String> filetypList) {
        SearchEngine se = new SearchEngine(null);
        //System.out.println("Suche in Datei ... " + filetypList.get(0) + filetype + " " + filetypList.contains(filetype));
        if (filetypList == null || filetypList.isEmpty()) {
            return;
        }
        if (filetypList.contains(filetype)) {

            this.find = find;
            String tmp = se.readWholeFile(this.fullpath);
            //System.out.println(tmp);
            int rest = 0;
            int ipos = tmp.indexOf(find);

            while (ipos >= 0) {
                contentFoundList.add(ipos + rest);
                tmp = tmp.substring(ipos + find.length());
                rest = rest + ipos + find.length();
                ipos = tmp.indexOf(find);
            }
        }
    }

    /**
     * Calculate missing Informations from the ressource.
     *
     * @return Data
     */
    public Data determineValues() {
        return determineValues(this);
    }

    /**
     * Calculate missing Informations from the ressource.
     *
     * @param data Data
     * @return data Data
     */
    public Data determineValues(Data data) {
        File file = new File(data.getFullpath());

        data.setSize(String.valueOf(file.length()));
        data.setLastmodified(String.valueOf(file.lastModified()));
        data.setFullname(file.getName());
        data.setPath(file.getParent());
        data.setSystemIcon(FileSystemView.getFileSystemView().getSystemIcon(file));

        if(file.isFile()){
            data.setType("file");
        }
        if(file.isDirectory()){
            data.setType("folder");
        }

        // ----------------------------------------
        // Ressource isFile:
        if (data.getType().equals("file")) {
            data.setFiletype(calcFileType(file.getPath()));
            data.setName(calcFilename(file.getName()));
        }

        // ----------------------------------------
        // Ressource is Folder:
        if (data.getType().equals("folder")) {
            data.setName(file.getName());
            data.setFiletype("N/A");
        }

        return data;
    }

//    public Data determineValuesAsSubEntity(Data parentEntity) {
//        File file = new File(this.getFullpath());
//
//        this.setSize(String.valueOf(file.length()));
//        this.setLastmodified(String.valueOf(file.lastModified()));
//        this.setFullname(file.getName());
//        this.setPath(file.getParent());
//        this.setSystemIcon(FileSystemView.getFileSystemView().getSystemIcon(file));
//
////        try {
////            data.setPath(file.getCanonicalPath());
////        } catch (IOException ex) {
////            System.err.println("Fehler beim erstellen des kanonischen Pfades! Setze stattdessen einen absoluten");
////            data.setPath(file.getAbsolutePath());
////        }
//        // ----------------------------------------
//        // Ressource isFile:
//        if (this.getType().equals("file")) {
//            this.setFiletype(calcFileType(file.getPath()));
//            this.setName(calcFilename(file.getName()));
//        }
//
//        // ----------------------------------------
//        // Ressource is Folder:
//        if (this.getType().equals("folder")) {
//            this.setName(file.getName());
//            this.setFiletype("N/A");
//        }
//
//        this.setPath(parentEntity.getFullname());
//
//        return this;
//    }
    /**
     * Determine the filetype. Example: "C:\\Test\text.txt" -> "txt".
     *
     * @param path String
     * @return filetype String
     */
    private String calcFileType(String path) {
        if (path.lastIndexOf(".") <= 0) {
            return "N/A";
        }
        return path.substring(path.lastIndexOf("."));
    }

    /**
     * Determine filename from complete filename. Example: "example.pdf" Result:
     * "example"
     *
     * @param name String
     * @return name String without filetype
     */
    private String calcFilename(String name) {
        if (name.lastIndexOf(".") <= 0) {
            return name;
        }
        return name.substring(0, name.lastIndexOf("."));
    }

    /**
     * Knotentext vom JTree.
     *
     * @return name String
     */
    public String toString() {
        return fullpath;
    }

    // -----------------------------------------------
    // Getter & Setter.
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLastmodified() {
        return lastmodified;
    }

    public String getLastmodifiedDateTime() {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm") {
        };
        Date date = new Date(Long.valueOf(this.lastmodified));
        return df.format(date);
    }

    public String getLatmodifiedFull() {
        Date date = new Date(Long.valueOf(this.lastmodified));
        return date.toString();
    }

    public void setLastmodified(String lastmodified) {
        this.lastmodified = lastmodified;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFind() {
        return find;
    }

    public void setFind(String find) {
        this.find = find;
    }

    public String getFindTime() {
        return findTime;
    }

    public void setFindTime(String findTime) {
        this.findTime = findTime;
    }

    public String getSize() {
        return size;
    }

    public String getSizeInKB() {
        if(size == null){
            DecimalFormat df = new DecimalFormat();
            return String.valueOf(df.format(0)) + " KB";
        }
        double result = Math.round(Double.valueOf(size) / 1024 * 1000.) / 1000.;
        DecimalFormat df = new DecimalFormat();
        return String.valueOf(df.format(result)) + " KB";
    }

    public String getSizeInMB() {
        double result = Math.round(Double.valueOf(size) / 1048576 * 1000.) / 1000.;
        DecimalFormat df = new DecimalFormat();
        return String.valueOf(df.format(result)) + " MB";
    }

    public String getSizeInGB() {
        double result = Math.round(Double.valueOf(size) / 1073741824 * 1000.) / 1000.;
        DecimalFormat df = new DecimalFormat();
        return String.valueOf(df.format(result)) + " GB";
    }

    public String getSizeInTB() {
        double result = Math.round(Double.valueOf(size) / 1073741824 / 1024 * 1000.) / 1000.;
        DecimalFormat df = new DecimalFormat();
        return String.valueOf(df.format(result)) + " TB";
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFullpath() {
        return fullpath;
    }

    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getMatchCount() {
        return MatchCount;
    }

    public void setMatchCount(String MatchCount) {
        this.MatchCount = MatchCount;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String getPathFoundList() {
        String result = "";
        for (int i = 0; i < pathFoundList.size(); i++) {
            if (i < pathFoundList.size() - 1) {
                result = result + String.valueOf(pathFoundList.get(i)) + ", ";
            } else {
                result = result + String.valueOf(pathFoundList.get(i));
            }
        }

        if (result.equals("")) {
            return "N/A";
        }
        return result;
    }

    public String getContentFoundList() {
        String result = "";
        if (contentFoundList != null) {
            for (int i = 0; i < contentFoundList.size(); i++) {
                if (i < contentFoundList.size() - 1) {
                    result = result + String.valueOf(contentFoundList.get(i)) + ", ";
                } else {
                    result = result + String.valueOf(contentFoundList.get(i));
                }
            }

            if (result.equals("")) {
                return "N/A";
            }
        }
        return result;
    }

    public void setPathFoundList(ArrayList<Integer> pathFoundList) {
        this.pathFoundList = pathFoundList;
    }

    public void addContentFoundList(ArrayList<Integer> contentFoundList) {
        this.contentFoundList.addAll(contentFoundList);
    }

    /**
     * Retruns a Stringboolean "true" or "false", if find was found in path more
     * than one times or not.
     *
     * @return String "true" or "false".
     */
    public String isFoundInPath() {
        if (pathFoundList.size() > 0) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * Getter.
     *
     * @return String
     */
    public String getFoundCountInPath() {
        return String.valueOf(this.pathFoundList.size());
    }

    public String getFoundInContent() {
        if ("".equals(isFoundInContent)) {
            if (contentFoundList.size() > 0) {
                isFoundInContent = "true";
                return isFoundInContent;
            } else {
                isFoundInContent = "false";
                return isFoundInContent;
            }
        }
        return isFoundInContent;
    }

    /**
     *
     * @param value
     */
    public void setFoundInContent(String value) {
        isFoundInContent = value;
    }

    /**
     * Getter.
     *
     * @return (String) int
     */
    public String getFoundCountInFile() {
        if(contentFoundList != null){
        return String.valueOf(this.contentFoundList.size());
        }else{
            return "0";
        }
    }

    public Icon getSystemIcon() {
        return systemIcon;
    }

    /**
     * Setter.
     *
     * @param systemIcon
     */
    public void setSystemIcon(Icon systemIcon) {
        this.systemIcon = systemIcon;
    }

    /**
     * Getter.
     *
     * @return ArrayList.
     */
    public ArrayList<Data> getChildList() {
        return childList;
    }

    /**
     * Setter.
     *
     * @param childList
     */
    public void setChildList(ArrayList<Data> childList) {
        this.childList = childList;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
   
}
