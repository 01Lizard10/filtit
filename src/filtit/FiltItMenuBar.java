package filtit;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

/**
 * // Quelle:
 * http://docs.oracle.com/javase/tutorial/uiswing/components/menu.html
 *
 * @author Lizard
 */
public class FiltItMenuBar {

    private JMenuBar menuBar;
    private JMenu menuFiltIt, menuView, menuViewmodi, menuTable, menuHelp;
    private JMenuItem itemViewmodiManagement, itemOverwriteActualViewmodi,
            itemSaveAsNewViewmodi, itemAddColumn, itemRemoveColumn, itemAbout;
    //private JRadioButtonMenuItem rbMenuItem;
    private JCheckBoxMenuItem cbAutoColumnResize, cbShowFilterPanel;
    private FiltItController maincontroller;
    private ArrayList<JMenuItem> viewMenuItemList;

    public FiltItMenuBar(FiltItController controller) {
        this.maincontroller = controller;
        this.viewMenuItemList = new ArrayList();
        this.init();
    }

    public final void init() {
        menuBar = new JMenuBar();
        menuFiltIt = new JMenu("  FiltIt  ");
        menuFiltIt.setMnemonic('I');
        //menu.setMnemonic(KeyEvent.VK_A);
        //filtItMenu.getAccessibleContext().setAccessibleDescription(
        //"test description");

        cbShowFilterPanel = new JCheckBoxMenuItem("Show Column Filter Panel");
        cbShowFilterPanel.setMnemonic('F');
        cbShowFilterPanel.setActionCommand("bShowFilterPanel");
        cbShowFilterPanel.addActionListener(maincontroller);
        menuFiltIt.add(cbShowFilterPanel);

        menuBar.add(menuFiltIt);

        menuView = new JMenu("   View  ");
        menuView.setMnemonic('V');
        menuBar.add(menuView);

        cbAutoColumnResize = new JCheckBoxMenuItem("Auto Columnwidth resize");
        cbAutoColumnResize.addActionListener(maincontroller);        
        cbAutoColumnResize.setActionCommand("autoColumnResize");
        //cbAutoColumnResize.setMnemonic(KeyEvent.VK_C);

        menuView.add(cbAutoColumnResize);

        menuView.addSeparator();

        menuViewmodi = new JMenu("  Viewmodes  ");
        menuViewmodi.setMnemonic('O');
        menuView.add(menuViewmodi);

        menuView.addSeparator();

        itemOverwriteActualViewmodi = new JMenuItem("Overwrite actual Viewmode");
        itemOverwriteActualViewmodi.setMnemonic('W');
        itemOverwriteActualViewmodi.setActionCommand("overwriteActualVM");
        itemOverwriteActualViewmodi.addActionListener(maincontroller);
        menuView.add(itemOverwriteActualViewmodi);

        itemSaveAsNewViewmodi = new JMenuItem("Save as a new Viewmode");
        itemSaveAsNewViewmodi.setMnemonic('S');
        itemSaveAsNewViewmodi.setActionCommand("saveViewmodi");
        itemSaveAsNewViewmodi.addActionListener(maincontroller);
        menuView.add(itemSaveAsNewViewmodi);

        // Deaktivate if no viewmodie hase been choosed:
        if (maincontroller.getVM().getActualViewmodi() != null) {
            itemOverwriteActualViewmodi.setEnabled(true);
        } else {
            itemOverwriteActualViewmodi.setEnabled(false);
        }

        itemViewmodiManagement = new JMenuItem(" Viewmodemanagement ");
        itemViewmodiManagement.setMnemonic('M');
        itemViewmodiManagement.setActionCommand("vmManagement");
        itemViewmodiManagement.addActionListener(maincontroller);
        menuView.add(itemViewmodiManagement);

        menuView.addSeparator();

        menuTable = new JMenu("  Table ");
        menuTable.setMnemonic('T');
        menuView.add(menuTable);

        itemAddColumn = new JMenuItem("Add Column(s)");
        itemAddColumn.setMnemonic('A');
        itemAddColumn.setActionCommand("addColumn");
        itemAddColumn.addActionListener(maincontroller);
        menuTable.add(itemAddColumn);

        menuHelp = new JMenu("Help");
        menuHelp.setMnemonic('H');
        menuBar.add(menuHelp);

        itemAbout = new JMenuItem("About");
        itemAbout.setMnemonic('A');
        itemAbout.setActionCommand("about");
        itemAbout.addActionListener(maincontroller);
        menuHelp.add(itemAbout);

//        itemRemoveColumn = new JMenuItem("Remi");
//        itemRemoveColumn.setMnemonic('A');
//        itemRemoveColumn.setActionCommand("addColumn");
//        itemRemoveColumn.addActionListener(maincontroller);
//        menuTable.add(itemRemoveColumn);
    }

    /**
     * Getter für die MenuBar.
     *
     * @return JMenuBar.
     */
    public JMenuBar getMenuBar() {
        return this.menuBar;
    }

    /**
     * Getter for AutoColumnResize.
     *
     * @return Boolean AutoColumnResize
     */
    public boolean getAutoColumnResize() {
        return this.cbAutoColumnResize.isSelected();
    }

    /**
     * Setter für CheckBoxMenuItem.
     *
     * @param state Boolean
     */
    public void setAutoColumnResize(boolean state) {
        this.cbAutoColumnResize.setSelected(state);
    }

    /**
     * Getter.
     *
     * @return
     */
    public JCheckBoxMenuItem getCbShowFilterPanel() {
        return cbShowFilterPanel;
    }

    /**
     * Setter.
     *
     * @param cbShowFilterPanel
     */
    public void setCbShowFilterPanel(JCheckBoxMenuItem cbShowFilterPanel) {
        this.cbShowFilterPanel = cbShowFilterPanel;
    }

    /**
     * Initiate Menubar with viewmodi.
     *
     * @param vm ViewmodiManagement
     */
    void initViewmodi(ViewmodiManagement vm) {
        menuViewmodi.removeAll();
        JMenuItem viewMenuItem;
        int key = KeyEvent.VK_0;
        for (int i = 0; i < vm.getvList().size(); i++) {
            // Nur die ersten Zehn dem Menu hinzufügen
            if (i >= 10) {
                break;
            }

            viewMenuItem = new JMenuItem(vm.getvList().get(i).getCaption());
            viewMenuItem.addActionListener(maincontroller);
            viewMenuItemList.add(viewMenuItem);
            viewMenuItem.setActionCommand("viewmodi" + i);
            viewMenuItem.setMnemonic((key + i));
            viewMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                    key + i, ActionEvent.CTRL_MASK));
            menuViewmodi.add(viewMenuItem);
        }
    }

    public JMenuItem getOverwriteActualVM() {
        return itemOverwriteActualViewmodi;
    }

}
