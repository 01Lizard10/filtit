package filtit;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class FiltItModel extends Observable {

    /**
     * MainSearchThread.
     */
    private SearchThread thread;
    /**
     *
     */
    private SearchFileFolderThread searchFileFolderThread;
    /**
     * Objekt, dass die Datenstrucktren enthält und händelt. Darunter die
     * gefundenen Ergebnisse, und die Tabellenstruktur.
     */
    private DataModel dm;
    /**
     * Objekt, dass die Suchalgorithmen händelt.
     */
    private SearchEngine se;
    /**
     * Aktuell betrachteter Ordnerpfad, der durchsucht werden soll.
     */
    private String folderPath = "";
    /**
     * Aktuell eingegebener Suchausdruck, der als Suchparameter dienen soll.
     */
    private String find = "";
    /**
     * Manage Viewmodi.
     */
    private ViewmodiManagement vm;
    /**
     * Actual regarded row;
     */
    private int row;
    /**
     * Actual regarded column; Hinweis: Wird zwar nicht genutzt, aber könnte
     * später mal gebraucht werden. Analog zu row.
     */
    private int column;
    /**
     * Actual progressValue for the progressbar
     * Count of processed entities.
     */
    private int progressvalue;
    /**
     * Searchstate of the search. -1 -> searcherror, 0 -> running, 1 -> Search
     * completed, 2 -> Search canceled by user, 3 -> Unable to run a new
     * search,4 ready to search. Another search still in process.
     *
     */
    private int searchState;
    /**
     * All implemented searchmodi.
     */
    private final String[] handleMethodes
            = {"TXT-Search",
                "PDF-Search",
                "ZIP-Search",
                "RAR-Search",
                "7z-Search",
                "JAR-Search"};
    /**
     * Tooltip-information for each searchmodi.
     */
    private final String[] handleMethodesToolTip
            = {"Handle a file as a textfile. For example, you can search in XML-file like a TXT-file.",
                "Handle a given filetyp like a PDF-file.",
                "Handle a given filetyp like a ZIP-file.",
                "Handle a given filetyp like a RAR-file.",
                "Handle a given filetyp like a 7z-file.",
                "Handle a given filetyp like a JAR-file."
            };

    /**
     * Standardconstructor.
     */
    public FiltItModel() {
        dm = new DataModel();
        se = new SearchEngine(this);
        vm = new ViewmodiManagement();

        // Check: is there a property file?
        FiltItProperties p = new FiltItProperties();
        if (p.readPropFile() < 0) {

            // create a new property-file:
            p.writePropFile();

            vm.setActualViewmodi(vm.getDefaultViewmodi());
            vm.setChangedViewmodi(vm.getActualViewmodi());
            vm.writeNewViewmodiForViewmodiList(vm.getActualViewmodi());
        }

        // Initialisiere Viewmodi:
        vm.readViewmodiList();

        // Read last used viewmod:
        Viewmodi v = vm.readASingleViewmodi("lastUsedViewmode");
        // if there is a last used viewmodi:
        if (v != null) {
            vm.removeASingleViewmodi("lastUsedViewmode");
            vm.setChangedViewmodi(v);
            vm.setActualViewmodi(v);
            
        } else {// if there is no last used viewmodi:
            vm.setActualViewmodi(vm.getDefaultViewmodi());
            vm.setChangedViewmodi(vm.getActualViewmodi());
        }
//        update();
        // Programmstate ready:
        searchState = 4;
    }

    /**
     * Default Constructor.
     *
     * @param o
     */
    public FiltItModel(Object o) {
    }

    /**
     * Search in all folder all files and add them to the DataModel.
     */
    public void searchFilesAndFolder() {
        // -----------------------------------------------------
        // 1. SuchEngine dem Ordnerpfad geben:
//        se.setFolder(folderPath);
        // Suche alle Dateien und Ordner.
        MultiThreadMethods mtm = new MultiThreadMethods();

        // Set ThreadCount:
        mtm.setThreadCount(vm.getActualViewmodi().getSearchFilesFolderThreadCount());

        try {
            dm.insertDatas(mtm.findFolderAndFiles_WithCallables(new File(folderPath)));
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(FiltItModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Diese dem Model übergeben.
//        dm.insertDatas(se.getAllDirectoriesAndFiles());
        // -----------------------------------------------------
        // ToDo: Auch nebenläufig machen?!
        // 2. Jede Datei und Ordner analysieren nach Metadaten:
        for (int i = 0; i < dm.getDatalist().size(); i++) {
            dm.getDatalist().set(i, dm.getDatalist().get(i).determineValues());
        }
        // -----------------------------------------------------
    }

    /**
     * Write the windowsposition and windowssize in the propertyfile.
     *
     * @param x windowsposition int
     * @param y windowsposition int
     * @param width windowssize int
     * @param height windowssize int
     */
    public void saveWindowsPositionAndSize(int x, int y, int width, int height) {
        // read viewmodi from property-file:
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        p.setProperty("windowsPosX", String.valueOf(x));
        p.setProperty("windowsPosY", String.valueOf(y));
        p.setProperty("WindowsWidth", String.valueOf(width));
        p.setProperty("WindowsHeight", String.valueOf(height));

        p.writePropFile();
    }

    /**
     * Read the windowsposition and windowssize from propertyfile. If there is
     * no value in the propertyfile, it returns defaultvalues.
     *
     * @return integer[] {x,y,width,heigth}
     */
    public int[] readWindowsPositionAndSize() {
        int[] result = new int[4];
        int[] defaultResult = new int[4];

        // set defaultvalues:
        defaultResult[0] = (Toolkit.getDefaultToolkit().getScreenSize().width - 650) / 2;
        defaultResult[1] = (Toolkit.getDefaultToolkit().getScreenSize().height - 650) / 2;
        defaultResult[2] = 650; // Windows size
        defaultResult[3] = 650; // Windows size

        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        if (p.getProperty("windowsPosX") == null) {
            return defaultResult;
        } else {
            result[0] = Integer.valueOf(p.getProperty("windowsPosX"));
        }

        if (p.getProperty("windowsPosY") == null) {
            return defaultResult;
        } else {
            result[1] = Integer.valueOf(p.getProperty("windowsPosY"));
        }

        if (p.getProperty("WindowsWidth") == null) {
            return defaultResult;
        } else {
            result[2] = Integer.valueOf(p.getProperty("WindowsWidth"));
        }

        if (p.getProperty("WindowsHeight") == null) {
            return defaultResult;
        } else {
            result[3] = Integer.valueOf(p.getProperty("WindowsHeight"));
        }

        return result;
    }

    /**
     * Stop the SearchThread.
     */
    public void stopSearchThread() {
        thread.interrupt();
    }

    /**
     * Start the SearchThread.
     *
     * @param controller FiltItController
     */
    public void startSearchInThread(FiltItController controller) {

        // First start:
        if (thread == null) {
            thread = new SearchThread(this, controller);
            thread.start();
        }

        // Start exisiting thread again if he isnt in progress:
        if (!thread.isAlive()) {
            thread = new SearchThread(this, controller);
            thread.start();

        } else {
            searchState = 3;
        }
    }

    /**
     * A thread to search all files and folders.
     * @param controller
     */
    public void startFileFolderSearchThread(FiltItController controller) {


        File file = new File(folderPath);

        if (folderPath.equals("") || !file.exists()) {
            return;
        }

        // First start:
        if (searchFileFolderThread == null) {
            searchFileFolderThread = new SearchFileFolderThread(this, controller);
            searchFileFolderThread.start();
        }

        // Start exisiting thread again if he isnt in progress:
        if (!searchFileFolderThread.isAlive()) {
            searchFileFolderThread = new SearchFileFolderThread(this, controller);
            searchFileFolderThread.start();
        }
    }


    /**
     * Searching the key in all files and in the filename. Seraching also in
     * archivefiles.
     *
     */
    public void searchFind(FiltItModel model, FiltItController controller) {

        //TODO: check if folderPath is a valid path, else show user an Error!
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        File file = new File(folderPath);

        if (folderPath.equals("") || !file.exists()) {
            return;
        }

        // 0. Alte Suche entfernen: --------------------------------------------
        clearLastResults();

        // 1. Starte Suche nach Folder oder und Datein: ------------------------
        searchFilesAndFolder();

        // --- For Processbar:
        model.getDataModel().setDatalist(this.getDataModel().getDatalist());
        controller.setProgressbar(model.getProgressValue(),model.getDataModel().getDatalist().size());
        // ---

        // TODO: Default only the first folder layer

        // Wenn kein Key (find) angegebenen wurde:
        if (find.equals("")) {
            return;
        }

        // 2. Pfade nach keyword durchsuchen: ----------------------------------
        for (int i = 0; i < dm.getDatalist().size(); i++) {
            dm.getDatalist().get(i).searchInPath(this.find);
        }

        // 3. In den Datein und Ordnern suchen: --------------------------------
        MultiThreadMethods mtm = new MultiThreadMethods(model, controller);
        mtm.setThreadCount(vm.getActualViewmodi().getSearchInFileContentThreadCount());
        try {
            dm.setDatalist(mtm.searchInFileContent_WithCallables(dm.getDatalist(), this));
        } catch (InterruptedException | ExecutionException ex) {
            System.err.println("Error: " + ex.getMessage());
            Logger.getLogger(FiltItModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Lösche alle Tempordner:
        removeTempFolder();
    }

    /**
     * Removing all tempfiles which was creating for archivefile-search.
     */
    public void removeTempFolder() {

        ArrayList<File> tempList = new ArrayList();
        ArrayList<File> toRemoveList = new ArrayList();

        File[] dics = new File(System.getProperty("user.dir")).listFiles();
        if (dics != null) {
            for (File dic : dics) {
                if (dic.isDirectory() && (!dic.equals("null"))) {
                    tempList.add(new File(dic.getPath()));
                    // System.out.println("Gefundene Tempfiles: " + dic.getPath());
                }
            }
        }

        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).getName().contains("tempfolder") && tempList.get(i).getName().indexOf("tempfolder") == 0) {
                // System.out.println("Zu löschenene Dateien: " + tempList.get(i).getName());

                // Schaue, ob der Rest des Namens des Ordners ohne "tempfolder" nur eine Zahl ist:
                try {
                    Integer.valueOf(tempList.get(i).getName().replace("tempfolder", ""));
                } catch (Exception e) { // name kann nicht in integer umgewandelt werden -> Ordner ist nicht der Norm nach:
                    continue;
                }
                // Wenn bis hier her gekommen wird, kann der Ordner gelöscht werden:
                toRemoveList.add(tempList.get(i));
            }
        }
        for (int i = 0; i < toRemoveList.size(); i++) {
            //System.out.println("Lösche: " + toRemoveList.get(i).getPath());
            deleteDir(toRemoveList.get(i));
        }
    }

    /**
     * Deletes a folder with subfolders.
     *
     * @param path
     */
    private void deleteDir(File path) {
        for (File file : path.listFiles()) {
            if (file.isDirectory()) {
                deleteDir(file);
            }
            file.delete();
        }
        path.delete();
        if (path.exists()) {
            System.err.println("Error: Unable to delete tempfile: " + path.getPath() + " ->  try again.");
            //deleteDir(path);
        }
    }

    /**
     * Set changed and notify observers.
     */
    public void update() {
        setChanged();
        notifyObservers();
    }

    /**
     * Open the Folder in Explorer for the actual regarded cell (entity).
     */
    public void openContainingFolderForActualRegardedCell() {

        // get entity for the cell:
        //System.out.println("Test: " + dm.getDatalist().get(row).getFullname());
        String folder = dm.getDatalist().get(row).getPath();
        try {
            // Open folder for the path of the file:
            Desktop.getDesktop().open(new File(folder));
        } catch (IOException ex) {
            System.out.println("Error: unable to open the Folder: " + folder);
        }
    }

    /**
     * Open the file in the OS-specific default application.
     */
    void openInDefaultApplicationForActualRegardedCell() {
        // If the Cell is a folder:
        if (dm.getDatalist().get(row).getType().equals("folder")) {
            openContainingFolderForActualRegardedCell();
        } else {// Cell is a file and not a folder:

            try {
                File file = new File(dm.getDatalist().get(row).getFullpath());
                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                System.err.println("Error: unable to open the file with a default application.");
            }
        }
    }

    /**
     * Getter.
     *
     * @return ViewmodiManagement
     */
    public synchronized ViewmodiManagement getVM() {
        return vm;
    }

    /**
     * Clear the last results.
     */
    public void clearLastResults() {
        dm.getDatalist().clear();
    }

    /**
     * Removes all elements with a filesize of Data greater or lower than the
     * GLOBAL filesize in SearchFilterData from viewmodimanagement.
     *
     * @param datalist
     * @return
     */
    public ArrayList<Data> filterGlobalFiletypeSize(ArrayList<Data> datalist, ViewmodiManagement vm) {

        System.out.println("diese Methode neu bauen 01");
//        if (vm.getActualGlobalMinMax() != null) {
//            double min, max;
//            for (int i = 0; i < datalist.size(); i++) {
//
//                if (vm.getActualGlobalMinMax().getGlobalMinTA().equals("")) {
//                    min = 0;
//                } else {
//                    min = Double.valueOf(vm.getActualGlobalMinMax().getMinDocSize());
//                }
//
//                if (vm.getActualGlobalMinMax().getGlobalMaxTA().equals("")) {
//                    max = 0;
//                } else {
//                    max = Double.valueOf(vm.getActualGlobalMinMax().getMaxDocSize());
//                }
//
//                // Wenn die Dateigröße kleiner als die untere Schranke ist:
//                if (Double.valueOf(datalist.get(i).getSize()) < min) {
//                    datalist.remove(i);
//                    i--;
//                    continue;
//                }
//
//                // Wenn max == 0 ist, heißt dies, dass alle Werte zugelassen werden sollen:
//                if (max != 0 && Double.valueOf(datalist.get(i).getSize()) > max) {
//                    datalist.remove(i);
//                    i--;
//                }
//            }
//        }
        return datalist;
    }

    // --------------------------- Getter &  Setter ----------------------
    /**
     * Getter for the SearchEngine.
     *
     * @return SearchEngine
     */
    public SearchEngine getSearchEngine() {
        return se;
    }

    /**
     * Getter for the choosen folder-path.
     *
     * @return String folderPath
     */
    public String getFolderPath() {
        return folderPath;
    }

    /**
     * Setter for the folder-path.
     *
     * @param folderPath
     */
    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    /**
     * Getter for the keyword.
     *
     * @return find String
     */
    public String getFind() {
        return find;
    }

    /**
     * Setter for the keyword.
     *
     * @param find String
     */
    public void setFind(String find) {
        this.find = find;
    }

    /**
     * Getter for the DataModel.
     *
     * @return DataModel
     */
    public DataModel getDataModel() {
        return dm;
    }

    /**
     * Setter for RememberWindowsPosAndSize.
     *
     * @param val
     */
    public void setRememberWindowsPosAndSize(Boolean val) {
        if (vm.getActualViewmodi() != null) {
            vm.getActualViewmodi().setRememberWindowsPosAndSize(val);
        }
    }

    /**
     * Setter for actual regarded row and column. Is called from controller
     * after mouseclicc on a cell.
     *
     * @param row actual regarded row as integer.
     * @param column actual regarded column as integer.
     */
    public void setCell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Getter for the progressvalue.
     *
     * @return int progressvalue
     */
    public int getProgressValue() {
        return progressvalue;
    }

    /**
     * Set default last used SFD-Data.
     */
    public void setLastUsedDefaultSFD() {

        // apply defaultvalues:
//        vm.setLastUsedSFD(getDefaultSFD());
        System.out.println("methode noch sinnvoll?");
        // set and apply defaultvalues too:
//        vm.setLastUsedGlobalMinMax(getDefaultGlobalMinMax());
    }

    /**
     * Returns the modiList as integer-ArrayList from the filetype. Mind: One
     * Document type can have more than one handle types.
     *
     * @param filetype document type.
     * @param sfdList
     * @return ArrayList with choosen modi for the document type.
     */
    public synchronized ArrayList<Integer> getModiForFiletype(String filetype, ArrayList<SearchFilterData> sfdList) {

        ArrayList<Integer> resultList = new ArrayList();

        // Wenn der document type is not in the list:
        for (int i = 0; i < sfdList.size(); i++) {
            // Parameter-Datentyp entspricht dem in der Liste:
            if (sfdList.get(i).getDocType().equals(filetype)) {
                resultList.add(Integer.valueOf(sfdList.get(i).getHandleMethode()));
            }
        }
        return resultList;
    }

    public String[] getHandleMethodes() {
        return handleMethodes;
    }

    public String[] getHandleMethodesToolTip() {
        return handleMethodesToolTip;
    }

    public int getSearchState() {
        return searchState;
    }

    public void setSearchState(int searchState) {
        this.searchState = searchState;
    }

    public void setProgressvalue(int progressvalue) {
        this.progressvalue = progressvalue;
    }
}
