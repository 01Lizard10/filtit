package filtit;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;

// pdfbox 1.8.9
//import org.apache.pdfbox.util.PDFTextStripper;

/**
 *
 * @author Lizard
 */
public class PDFManager {
    /**
     * Standard-Constructor.
     */
    public PDFManager() {
    }
    /**
     * Reads a PDF-File and returns the content as a string.
     * @param path String
     * @return text String
     */
    public String readPDF(String path) {
        String result = "";
        File file = new File(path);
        PDDocument doc;
        
        try {
            doc = PDDocument.load(file);

            //PDFRenderer pdfRenderer = new PDFRenderer(doc);

            // pdfbox 1.8.9:
            PDFTextStripper str = new PDFTextStripper();
            result = str.getText(doc);
//            for (PDPage page : doc.getPages())
//            {
//                //org.apache.commons.io.IOUtils
//                result += IOUtils.toString(page.getContents(), "UTF-8");
//            }

            doc.close();
        } catch (IOException ex) {
            System.out.println("Warning for: " + path);
            Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    /**
     * Test-Main-Funktion.
     * @param args 
     */
    public static void main(String[] args) {
        //String test = "C:\\Users\\Lizard\\Documents\\Citavi 4\\Settings\\tram2.pdf";
        String test = "C:\\Users\\Lizard\\Documents\\Citavi 4\\Master.pdf";
        PDFManager pdfm = new PDFManager();
        String result = pdfm.readPDF(test);
//        System.out.println("Result: " + result);

    }
}
