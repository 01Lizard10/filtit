package filtit;

import javax.swing.JFrame;

/**
 * Manage a ViewModiFrame.
 *
 * @author Lizard
 */
public class ViewModiFrame extends JFrame {

    private ViewmodiManagement vm;

    public ViewModiFrame(String title, ViewmodiManagement vm) {
        super(title);
        this.vm = vm;
    }

    public ViewmodiManagement callViewModiFrame() {
        return vm;
    }

    /**
     * Testfunktion:
     *
     * @param args Sting[]
     */
    public static void main(String[] args) {
        ViewmodiManagement vm = new ViewmodiManagement();
        vm.readViewmodiList();
        ViewModiFrame vf = new ViewModiFrame("ExampleTitle", vm);
        vf.callViewModiFrame();
        System.exit(0);
    }
}
