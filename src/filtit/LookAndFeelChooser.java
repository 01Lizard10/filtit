package filtit;

import java.awt.Dimension;
import javax.swing.JComboBox;
import javax.swing.UIManager;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class LookAndFeelChooser extends JComboBox {

    private UIManager.LookAndFeelInfo looks[];
    private String lookAndFeelNames[];

    public LookAndFeelChooser() {

        looks = UIManager.getInstalledLookAndFeels();
        lookAndFeelNames = new String[looks.length];

        // Set Names:
        for (int i = 0; i < looks.length; i++) {
            lookAndFeelNames[i] = looks[i].getName();
        }
        // Add items:
        for (int i = 0; i < looks.length; i++) {
            this.addItem(lookAndFeelNames[i]);
        }

        // Set Size:
        int width = 200, height = 25;
        this.setMinimumSize(new Dimension(width, height));
        this.setPreferredSize(new Dimension(width, height));
        this.setMaximumSize(new Dimension(width, height));
    }

    /**
     * Get the selected look and feel class.
     *
     * @return
     */
    public UIManager.LookAndFeelInfo getSelectedLookAndFeelClass() {
        return looks[getSelectedIndex()];
    }

    /**
     * Get the selected item look and feel className.
     *
     * @return
     */
    public String getSelectedLookAndFeelClassName() {
        return looks[getSelectedIndex()].getClassName();
    }

    /**
     * Set selection to the item with the className of the argument.
     *
     * @param className
     */
    public void setLookAndFeelSelectionToClassName(String className) {

        for (int i = 0; i < looks.length; i++) {
            if (looks[i].getClassName().equals(className)) {
                //System.out.println("className: " + className + " - " + looks[i].getClassName());
                this.setSelectedIndex(i); // geht irgendwie nicht     
                //this.setSelectedItem(looks[i].getClassName());
                return;
            }
        }
    }
}
