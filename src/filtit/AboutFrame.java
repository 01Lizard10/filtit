package filtit;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * The About-Window.
 * @author Hans-Georg Schladitz
 */
class AboutFrame extends JFrame implements WindowListener{

    private JPanel mainpanel;
    private Box vbox00 = Box.createVerticalBox();
    private Box hbox00 = Box.createHorizontalBox();
    private Box vbox01 = Box.createVerticalBox();

    
    public AboutFrame() {
        setTitle("FiltIt about");
        
        mainpanel = new JPanel();
        
        add(mainpanel);
        
        mainpanel.add(vbox00);
        
        vbox00.add(vbox01);
        
        vbox01.add(Box.createVerticalStrut(50));
        
        vbox01.add(new JLabel("By Hans-Georg Wu, né Schladitz"));
        vbox01.add(new JLabel("Email: hansgeorgschladitz@gmail.com"));
        
        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - getSize().width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2;
        this.setBounds(x, y, 300, 250);
        
        this.setVisible(true);
    }
    @Override
    public void windowOpened(WindowEvent e) {        
    }
    @Override
    public void windowClosing(WindowEvent e) {   
        this.dispose();
    }
    @Override
    public void windowClosed(WindowEvent e) {       
    }
    @Override
    public void windowIconified(WindowEvent e) {       
    }
    @Override
    public void windowDeiconified(WindowEvent e) {       
    }
    @Override
    public void windowActivated(WindowEvent e) {        
    }
    @Override
    public void windowDeactivated(WindowEvent e) {        
    }        
}