package filtit;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Speicht und verwaltet zu ausgewählten Dateitypen und deren
 * Suchfunktionen(handletypes). Allgemein gilt: Ein Dateityp kann mehrere
 * handletypes haben.
 *
 * @author Hans-Georg Schladitz
 */
class SFDManagement {

    /**
     * List of SearchFilterOptions(handletypes).
     */
    private ArrayList<SearchFilterData> sfoList;

    /**
     * Constructor.
     */
    public SFDManagement() {
        sfoList = new ArrayList();
    }

    /**
     * Returns the modiList as integer-ArrayList from the filetype. Mind: One
     * Document type can have more than one handle types.
     *
     * @param filetype document type.
     * @return ArrayList with choosen modi for the document type.
     */
//    public ArrayList<Integer> getModiForFiletype(String filetype) {
//
//        ArrayList<Integer> resultList = new ArrayList();
//
//        // Wenn der document type is not in the list:
//        for (int i = 0; i < sfoList.size(); i++) {
//            // Parameter-Datentyp entspricht dem in der Liste:
//            if (sfoList.get(i).getDocType().equals(filetype)) {
//                resultList.add(Integer.valueOf(sfoList.get(i).getHandleMethode()));
//            }
//        }
//        return resultList;
//    }



    /**
     * Sets the actual sfoList to default.
     */
    public void setDefaultSFO() {
//        sfoList = getDefaultSFO();
//        add_docType_handleMethode(".txt", 0);
//        add_docType_handleMethode(".xml", 0);
//        add_docType_handleMethode(".pdf", 1);
//        add_docType_handleMethode(".zip", 2);
//        add_docType_handleMethode(".rar", 3);
//        add_docType_handleMethode(".7z", 4);
//        add_docType_handleMethode(".jar", 5);
//        add_docType_handleMethode(".html", 0);
//        add_docType_handleMethode(".java", 0);
//        add_docType_handleMethode(".log", 0);
//        add_docType_handleMethode(".js", 0);
//        add_docType_handleMethode(".ini", 0);
//
//        for (int i = 0; i < sfoList.size(); i++) {
//            sfoList.get(i).setIsChecked(Boolean.TRUE);
//            sfoList.get(i).setMaxTA_Value("0");
//            sfoList.get(i).setMaxTA_Value("0");
//            sfoList.get(i).setMaxDocSize("0");
//            sfoList.get(i).setMinDocSize("0");
//        }
    }

    /**
     * Setter for easy addition SFO-File.
     *
     * @param docType
     * @param handleMethode
     */
//    private void add_docType_handleMethode(String docType, int handleMethode) {
//        SearchFilterData data = new SearchFilterData();
//        data.setDocType(docType);
//        data.setHandleMethode(String.valueOf(handleMethode));
//        sfoList.add(data);
//    }


    /**
     * Getter.
     *
     * @return ArrayList sfoList the list of all SearchFilterOptions.
     */
    public ArrayList<SearchFilterData> getSfoList() {
        return sfoList;
    }

    /**
     * Setter.
     *
     * @param sfoList SearchFilterOptionList.
     */
    public void setSfoList(ArrayList<SearchFilterData> sfoList) {
        this.sfoList = sfoList;
    }

    /**
     * Sortiert die SFO-Daten. Vorteil: Übersicht für den User und leichteres
     * Valisieren.
     */
    public void sortSFO() {
        Collections.sort(sfoList);
    }

    /**
     * Clear both lists.
     */
    public void clearData() {
        sfoList.clear();
    }
}
