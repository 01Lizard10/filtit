package filtit;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Verwaltet verschiedene Messageboxen.
 *
 * @author Hans-Georg Schladitz
 */
class MessageBox extends JFrame implements ActionListener, KeyListener, WindowListener {

    /**
     * Result for askForStringMB.
     */
    private String askforStringResult = "";
    /**
     * Resultvalue for all MB. -2 means: Still no MB called or MB waits for
     * useractions. -1 means: MB closed/abort by user. 0 means: All okay/yes
     */
    private int Resultvalue = -2;
    /**
     *
     */
    private boolean resultboolean = false;
    /**
     * Textarea.
     */
    private JTextArea ta;
    /**
     * FiltItController.
     */
    private ActionListener mainController;
//    /**
//     * To add an identifier and other Informations like standardtexts and so on.
//     */
//    private Map property;
    /**
     * constructor.
     *
     * @param mainController
     */
    public MessageBox(ActionListener mainController) {
        this.mainController = mainController;
    }
 

    /**
     * Messagebox, die nach einem String fragt.
     */
    public void askForStringMB(String title, String labeltext, String defaulttext, String iconFileName) {
        this.Resultvalue = -2;
        this.setTitle(title);
        this.addWindowListener(this);
        this.setSize(500, 200);
        this.setAlwaysOnTop(true);

        // Set elements:
        JPanel mainpanel = new JPanel(new BorderLayout());
        JLabel label = new JLabel(labeltext);
        int taHeight = 40;
        ta = new JTextArea(defaulttext, 0, taHeight);
        JScrollPane sp = new JScrollPane(ta, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        JButton btn = new JButton(" O.K. ");
        btn.addActionListener(this);
        btn.setActionCommand("MBok");

        ta.setName("ta");
        ta.addKeyListener(this);
        sp.setMinimumSize(new Dimension(50, taHeight));
        sp.setPreferredSize(new Dimension(50, taHeight));
        sp.setMaximumSize(new Dimension(Short.MAX_VALUE, taHeight));
        if (iconFileName != null) {
            IconManager im = new IconManager();
            this.setIconImage(im.createImageIcon(iconFileName).getImage());
        }

        Box mainbox = Box.createVerticalBox();
        Box vbox01 = Box.createVerticalBox(); // for label
        Box vbox02 = Box.createVerticalBox(); // for sp (ta)
        Box vbox03 = Box.createVerticalBox(); // for btn
        Box hbox01 = Box.createHorizontalBox(); // for vbox01
        Box hbox02 = Box.createHorizontalBox(); // vor vbox02 ans vbox03
        mainpanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox01.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox02.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        int patternLR = 35;
        this.add(mainpanel);

        mainpanel.add(mainbox);

        mainbox.add(Box.createVerticalGlue());
        mainbox.add(hbox01);
        mainbox.add(hbox02);
        mainbox.add(Box.createVerticalGlue());

        hbox01.add(Box.createHorizontalStrut(patternLR));
        hbox01.add(vbox01);
        hbox01.add(Box.createHorizontalGlue());
        vbox01.add(label);

        hbox02.add(Box.createHorizontalStrut(patternLR));
        hbox02.add(vbox02);
        hbox02.add(Box.createHorizontalStrut(patternLR));
        hbox02.add(vbox03);
        hbox02.add(Box.createHorizontalStrut(patternLR));
        vbox02.add(sp);
        vbox03.add(btn);

        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - this.getSize().width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - this.getSize().height) / 2;
        this.setBounds(x, y, 500, 200);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
    }

    /**
     * Messagebox, die nach einem Boolean (ja, nein) fragt.
     */
    public void askForBooleanMB(String title, String labeltext, String iconFileName) {
        this.Resultvalue = -2;
        this.setTitle(title);
        this.addWindowListener(this);
        this.setSize(500, 200);

        // Set elements:
        JPanel mainpanel = new JPanel(new BorderLayout());
        JLabel label = new JLabel(labeltext);
        int taHeight = 40;

        JButton btn = new JButton(" ja ");
        btn.addActionListener(this);
        btn.setActionCommand("ja");

        JButton btn2 = new JButton(" nein ");
        btn2.addActionListener(this);
        btn2.setActionCommand("nein");

        int bWidth = 80;
        int bHeight = 30;

        btn.setPreferredSize(new Dimension(bWidth, bHeight));
        btn.setMaximumSize(new Dimension(bWidth, bHeight));
        btn.setMinimumSize(new Dimension(bWidth, bHeight));

        btn2.setPreferredSize(new Dimension(bWidth, bHeight));
        btn2.setMaximumSize(new Dimension(bWidth, bHeight));
        btn2.setMinimumSize(new Dimension(bWidth, bHeight));

        if (iconFileName != null) {
            IconManager im = new IconManager();
            this.setIconImage(im.createImageIcon(iconFileName).getImage());
        }

        Box mainbox = Box.createVerticalBox();
        Box hbox01 = Box.createHorizontalBox();

        mainpanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox01.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));

//        mainbox.setBorder(new LineBorder(Color.BLACK));
//        hbox01.setBorder(new LineBorder(Color.GREEN));
        this.add(mainpanel);

        mainpanel.add(mainbox);

        mainbox.add(Box.createVerticalStrut(35));
        mainbox.add(hbox01);
        hbox01.add(Box.createHorizontalStrut(35));
        hbox01.add(label);
        hbox01.add(Box.createHorizontalStrut(15));
        hbox01.add(btn);
        hbox01.add(Box.createHorizontalStrut(20));
        hbox01.add(btn2);

        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - this.getSize().width) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - this.getSize().height) / 2;
        this.setBounds(x, y, 500, 200);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Wenn ok geklickt wurde:
//        if (e.getActionCommand().equals("MBok")) {
//            this.askforStringResult = ta.getText();
//            this.Resultvalue = 0;
//            mainController.actionPerformed(e);
//            this.dispose();
//        }
//
//        // Wenn ok geklickt wurde:
//        if (e.getActionCommand().equals("ja")) {
//            this.resultboolean = true;
//            this.Resultvalue = 0;
//            mainController.actionPerformed(e);
//            this.dispose();
//        }
//
//        // Wenn ok geklickt wurde:
//        if (e.getActionCommand().equals("nein")) {
//            this.resultboolean = false;
//            this.Resultvalue = 0;
//            mainController.actionPerformed(e);
//            this.dispose();
//        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Wenn Enter gedrückt wurde:
        if (e.getExtendedKeyCode() == 10 && e.getComponent().getName().equals("ta")) {
            this.askforStringResult = ta.getText();
            this.Resultvalue = 0;
            ActionEvent ae = new ActionEvent(this, 1, "MBok");
            this.actionPerformed(ae);
            this.setVisible(false);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        // MB closed by User;
        this.Resultvalue = -1;
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    /**
     * Getter.
     *
     * @return String result. Call this Getter after MessageBox has been used.
     */
    public String getAskforStringResult() {
        return askforStringResult;
    }

    /**
     * Getter.
     *
     * @return int result. Call this Getter after MessageBox has been used.
     */
    public int getResultvalue() {
        return Resultvalue;
    }
    /**
     * Getter.
     * @return 
     */
    public boolean isResultboolean() {
        return resultboolean;
    }
    /**
     * Close the Window and release resources.
     */
    public void close() {
        this.dispose();
    }
}
