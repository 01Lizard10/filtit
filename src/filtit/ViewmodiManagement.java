package filtit;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Verwaltet die verschiedenen Viewmodi. Rules: All viewmodi in vList have an
 * unique caption.
 *
 * @author Hans-Georg Schladitz
 */
public class ViewmodiManagement {

    /**
     * Liste alle Viewmodi.
     */
    private ArrayList<Viewmodi> vList;
    /**
     * Actual viewmodi.
     */
    private Viewmodi actualViewmodi;
    /**
     * Changed/unsaved viewmodi.
     */
    private Viewmodi changedViewmodi;

    /**
     * Defaultconstructor.
     */
    public ViewmodiManagement() {
        vList = new ArrayList();
    }

    /**
     * Reads all values from propertyfile. Clearing vList first.
     */
    public void readViewmodiList() {

        vList.clear();
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();
        Viewmodi v;
        if (p.getProperty("viewmodiCount") != null) {
            int vc = Integer.valueOf(p.getProperty("viewmodiCount"));
            for (int i = 0; i < vc; i++) {
                v = new Viewmodi();
                v.setColumnCode(String.valueOf(p.getProperty("columnCode_" + i)));
                v.setColumnWidthList(String.valueOf(p.getProperty("columnWidthList_" + i)));
                v.setAutoColumnResize(Boolean.valueOf(p.getProperty("autoColumnResize_" + i)));
                v.setCaption(String.valueOf(p.getProperty("caption_" + i)));
                v.setShowFilterPanel(Boolean.valueOf(p.getProperty("showFilterPanel_" + i)));
                v.setHideAllFiles(Boolean.valueOf(p.getProperty("hideAllFiles_" + i)));
                v.setHideAllFolder(Boolean.valueOf(p.getProperty("hideAllFolder_" + i)));
                v.setLookAndFeel(String.valueOf(p.getProperty("lookAndFeel_" + i)));
                v.setRememberWindowsPosAndSize(Boolean.valueOf(p.getProperty("rememberWindowsPosAndSize_" + i)));
                v.setSearchFilesFolderThreadCount(Integer.valueOf(p.getProperty("searchFilesFolderThreadCount_" + i)));
                v.setSearchInFileContentThreadCount(Integer.valueOf(p.getProperty("searchInFileContentThreadCount_" + i)));

                SearchFilterData sfd;
                ArrayList<SearchFilterData> sfdList = new ArrayList();
                if (p.getProperty("viewmodiCountSFD_" + i) != null) {
                    int sc = Integer.valueOf(p.getProperty("viewmodiCountSFD_" + i));
                    for (int s = 0; s < sc; s++) {
                        sfd = new SearchFilterData();
                        sfd.setDocType(p.getProperty("filetype_" + i + "," + s));
                        sfd.setHandleMethode(p.getProperty("handlemethode_" + i + "," + s));
                        sfd.setIsChecked(Boolean.valueOf(p.getProperty("filetypecheck_" + i + "," + s)));
                        sfd.setMinDocSize(p.getProperty("min_" + i + "," + s));
                        sfd.setMaxDocSize(p.getProperty("max_" + i + "," + s));
                        sfd.setMaxCB_Value(Integer.valueOf(p.getProperty("maxCB_Value_" + i + "," + s)));
                        sfd.setMinCB_Value(Integer.valueOf(p.getProperty("minCB_Value_" + i + "," + s)));
                        sfd.setMaxTA_Value(p.getProperty("maxTA_Value_" + i + "," + s));
                        sfd.setMinTA_Value(p.getProperty("minTA_Value_" + i + "," + s));
                        sfdList.add(sfd);
                    }

                    v.setSFD_List(sfdList);

                    SearchFilterData gSFD = new SearchFilterData();
                    gSFD.setMaxDocSize(p.getProperty("globalCalcMaxTA_" + i));
                    gSFD.setMinDocSize(p.getProperty("globalCalcMinTA_" + i));
                    gSFD.setGlobalMaxCB(Integer.valueOf(p.getProperty("globalMaxCB_" + i)));
                    gSFD.setGlobalMinCB(Integer.valueOf(p.getProperty("globalMinCB_" + i)));
                    gSFD.setGlobalMaxTA(p.getProperty("globalMaxTA_" + i));
                    gSFD.setGlobalMinTA(p.getProperty("globalMinTA_" + i));
                    v.setGlobalMinMax(gSFD);

                }

                this.vList.add(v);
            }
        }
    }

    /**
     * Write all viewmodi from vList in PropertyList. Clearing propertylist
     * fist.
     */
    public void writeALLViewmodi() {
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        if (p.getProperty("viewmodiCount") == null) {
            System.err.println("Error: Unable to read property-file.");
            return;
        }

        // Count of viewmodi:
        int readViewmodiCount = Integer.valueOf(p.getProperty("viewmodiCount"));

        // Delete all viewmodi first to avoid artefacts:
        for (int i = 0; i < readViewmodiCount; i++) {
            p.remove("columnCode_" + i);
            p.remove("columnWidthList_" + i);
            p.remove("autoColumnResize_" + i);
            p.remove("caption_" + i);
            p.remove("showFilterPanel_" + i);
            p.remove("hideAllFiles_" + i);
            p.remove("hideAllFolder_" + i);
            p.remove("lookAndFeel_" + i);
            p.remove("rememberWindowsPosAndSize_" + i);
            p.remove("searchFilesFolderThreadCount_" + i);
            p.remove("searchInFileContentThreadCount_" + i);

            if (p.getProperty("viewmodiCountSFD_" + i) != null) {

                for (int k = 0; k < Integer.valueOf(p.getProperty("viewmodiCountSFD_" + i)); k++) {
                    p.remove("filetype_" + i + "," + k);
                    p.remove("filetypecheck_" + i + "," + k);
                    p.remove("handlemethode_" + i + "," + k);
                    p.remove("min_" + i + "," + k);
                    p.remove("max_" + i + "," + k);
                    p.remove("minCB_Value_" + i + "," + k);
                    p.remove("maxCB_Value_" + i + "," + k);
                    p.remove("minTA_Value_" + i + "," + k);
                    p.remove("maxTA_Value_" + i + "," + k);
                }
                p.remove("viewmodiCountSFD_" + i);
            }

            p.writePropFile();
        }

        p.setProperty("viewmodiCount", String.valueOf(vList.size()));

        // Überschreibe viewmodi mit den richtigen werten:
        for (int i = 0; i < vList.size(); i++) {
            p.setProperty("columnCode_" + i, vList.get(i).getColumnCode());
            p.setProperty("columnWidthList_" + i, vList.get(i).getColumnWidthList());
            p.setProperty("autoColumnResize_" + i, String.valueOf(vList.get(i).getAutoColumnResize()));
            p.setProperty("caption_" + i, vList.get(i).getCaption());
            p.setProperty("showFilterPanel_" + i, String.valueOf(vList.get(i).getShowFilterPanel()));
            p.setProperty("hideAllFiles_" + i, String.valueOf(vList.get(i).getHideAllFiles()));
            p.setProperty("hideAllFolder_" + i, String.valueOf(vList.get(i).getHideAllFiles()));
            p.setProperty("lookAndFeel_" + i, String.valueOf(vList.get(i).getLookAndFeel()));
            p.setProperty("rememberWindowsPosAndSize_" + i, String.valueOf(vList.get(i).getRememberWindowsPosAndSize()));
            p.setProperty("searchFilesFolderThreadCount_" + i, String.valueOf(vList.get(i).getSearchFilesFolderThreadCount()));
            p.setProperty("searchInFileContentThreadCount_" + i, String.valueOf(vList.get(i).getSearchInFileContentThreadCount()));

            // SFD-Data for this viewmodi: -----------------------------------------------------------------
            ArrayList<SearchFilterData> sfdList = vList.get(i).getSFD_List();
            // For example: viewmodiCountSFD_3=7 means: there are 7 SFD-Date for viewmodi Nr. 3.
            p.setProperty("viewmodiCountSFD_" + i, String.valueOf(sfdList.size()));

            // Überschreibe viewmodi mit den richtigen werten:
            for (int s = 0; s < sfdList.size(); s++) {

                p.setProperty("filetype_" + i + "," + s, sfdList.get(s).getDocType());
                p.setProperty("filetypecheck_" + i + "," + s, String.valueOf(sfdList.get(s).getIsChecked()));
                p.setProperty("handlemethode_" + i + "," + s, sfdList.get(s).getHandleMethode());
                p.setProperty("min_" + i + "," + s, sfdList.get(s).getMinDocSize());
                p.setProperty("max_" + i + "," + s, sfdList.get(s).getMaxDocSize());
                p.setProperty("minCB_Value_" + i + "," + s, String.valueOf(sfdList.get(s).getMinCB_Value()));
                p.setProperty("maxCB_Value_" + i + "," + s, String.valueOf(sfdList.get(s).getMaxCB_Value()));
                p.setProperty("minTA_Value_" + i + "," + s, sfdList.get(s).getMinTA_Value());
                p.setProperty("maxTA_Value_" + i + "," + s, sfdList.get(s).getMaxTA_Value());
            }

            SearchFilterData globalMinMax = vList.get(i).getGlobalMinMax();
            // Global MinMax-Data: --------------------------------------------------------
            for (int g = 0; g < vList.size(); g++) {
                p.setProperty("globalCalcMinTA_" + i, globalMinMax.getMinDocSize());
                p.setProperty("globalCalcMaxTA_" + i, globalMinMax.getMaxDocSize());
                p.setProperty("globalMinTA_" + i, globalMinMax.getGlobalMinTA());
                p.setProperty("globalMaxTA_" + i, globalMinMax.getGlobalMaxTA());
                p.setProperty("globalMinCB_" + i, String.valueOf(globalMinMax.getGlobalMinCB()));
                p.setProperty("globalMaxCB_" + i, String.valueOf(globalMinMax.getGlobalMaxCB()));
            }
        }

    }

    /**
     * Saving a new Viewmodi.
     *
     * @param v Viewmodi.
     */
    public void writeNewViewmodiForViewmodiList(Viewmodi v) {

        // read viewmodi from property-file:
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        int i = 0;
        if (p.getProperty("viewmodiCount") != null) {
            i = Integer.valueOf(p.getProperty("viewmodiCount"));
        }

        p.setProperty("columnCode_" + i, v.getColumnCode());
        p.setProperty("columnWidthList_" + i, v.getColumnWidthList());
        p.setProperty("autoColumnResize_" + i, String.valueOf(v.getAutoColumnResize()));
        p.setProperty("caption_" + i, v.getCaption());
        p.setProperty("showFilterPanel_" + i, String.valueOf(v.getShowFilterPanel()));
        p.setProperty("hideAllFiles_" + i, String.valueOf(v.getHideAllFiles()));
        p.setProperty("hideAllFolder_" + i, String.valueOf(v.getHideAllFolder()));
        p.setProperty("lookAndFeel_" + i, String.valueOf(v.getLookAndFeel()));
        p.setProperty("rememberWindowsPosAndSize_" + i, String.valueOf(v.getRememberWindowsPosAndSize()));
        p.setProperty("searchFilesFolderThreadCount_" + i, String.valueOf(v.getSearchFilesFolderThreadCount()));
        p.setProperty("searchInFileContentThreadCount_" + i, String.valueOf(v.getSearchInFileContentThreadCount()));

        // SFD-Data for this viewmodi: -----------------------------------------------------------------
        // For example: viewmodiCountSFD_3=7 means: there are 7 SFD-Date for viewmodi Nr. 3.
        p.setProperty("viewmodiCountSFD_" + i, String.valueOf(v.getSFD_List().size()));

        // Überschreibe viewmodi mit den richtigen werten:
        for (int s = 0; s < v.getSFD_List().size(); s++) {

            p.setProperty("filetype_" + i + "," + s, v.getSFD_List().get(s).getDocType());
            p.setProperty("filetypecheck_" + i + "," + s, String.valueOf(v.getSFD_List().get(s).getIsChecked()));
            p.setProperty("handlemethode_" + i + "," + s, v.getSFD_List().get(s).getHandleMethode());
            p.setProperty("min_" + i + "," + s, v.getSFD_List().get(s).getMinDocSize());
            p.setProperty("max_" + i + "," + s, v.getSFD_List().get(s).getMaxDocSize());
            p.setProperty("minCB_Value_" + i + "," + s, String.valueOf(v.getSFD_List().get(s).getMinCB_Value()));
            p.setProperty("maxCB_Value_" + i + "," + s, String.valueOf(v.getSFD_List().get(s).getMaxCB_Value()));
            p.setProperty("minTA_Value_" + i + "," + s, v.getSFD_List().get(s).getMinTA_Value());
            p.setProperty("maxTA_Value_" + i + "," + s, v.getSFD_List().get(s).getMaxTA_Value());
        }

        // Global MinMax-Data: --------------------------------------------------------
        p.setProperty("globalCalcMinTA_" + i, v.getGlobalMinMax().getMinDocSize());
        p.setProperty("globalCalcMaxTA_" + i, v.getGlobalMinMax().getMaxDocSize());
        p.setProperty("globalMinTA_" + i, v.getGlobalMinMax().getGlobalMinTA());
        p.setProperty("globalMaxTA_" + i, v.getGlobalMinMax().getGlobalMaxTA());
        p.setProperty("globalMinCB_" + i, String.valueOf(v.getGlobalMinMax().getGlobalMinCB()));
        p.setProperty("globalMaxCB_" + i, String.valueOf(v.getGlobalMinMax().getGlobalMaxCB()));

        p.setProperty("viewmodiCount", String.valueOf((i + 1)));
        p.writePropFile();

        vList.add(v);
    }

    public void removeASingleViewmodi(String viewmodiName) {
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        p.remove(viewmodiName + "_columnCode_");
        p.remove(viewmodiName + "_columnWidthList_");
        p.remove(viewmodiName + "_autoColumnResize_");
        p.remove(viewmodiName + "_caption_");
        p.remove(viewmodiName + "_showFilterPanel_");
        p.remove(viewmodiName + "_hideAllFiles_");
        p.remove(viewmodiName + "_hideAllFolder_");
        p.remove(viewmodiName + "_lookAndFeel_");
        p.remove(viewmodiName + "_rememberWindowsPosAndSize_");
        p.remove(viewmodiName + "_searchFilesFolderThreadCount_");
        p.remove(viewmodiName + "_searchInFileContentThreadCount_");

        if (p.getProperty(viewmodiName + "_viewmodiCountSFD_") != null) {
            int sc = Integer.valueOf(p.getProperty(viewmodiName + "_viewmodiCountSFD_"));
            for (int s = 0; s < sc; s++) {
                p.remove(viewmodiName + "_filetype_" + s);
                p.remove(viewmodiName + "_handlemethode_" + s);
                p.remove(viewmodiName + "_filetypecheck_" + s);
                p.remove(viewmodiName + "_min_" + s);
                p.remove(viewmodiName + "_max_" + s);
                p.remove(viewmodiName + "_maxCB_Value_" + s);
                p.remove(viewmodiName + "_minCB_Value_" + s);
                p.remove(viewmodiName + "_maxTA_Value_" + s);
                p.remove(viewmodiName + "_minTA_Value_" + s);
            }
            p.remove(viewmodiName + "_viewmodiCountSFD_");
            p.remove(viewmodiName + "_globalCalcMaxTA_");
            p.remove(viewmodiName + "_globalCalcMinTA_");
            p.remove(viewmodiName + "_globalMaxCB_");
            p.remove(viewmodiName + "_globalMinCB_");
            p.remove(viewmodiName + "_globalMaxTA_");
            p.remove(viewmodiName + "_globalMinTA_");
        }

        p.writePropFile();
    }

    public Viewmodi readASingleViewmodi(String viewmodiName) {
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        if (p.getProperty(viewmodiName + "_columnCode_") == null) {
            return null;
        }

        Viewmodi v;

        v = new Viewmodi();
        v.setColumnCode(String.valueOf(p.getProperty(viewmodiName + "_columnCode_")));
        v.setColumnWidthList(String.valueOf(p.getProperty(viewmodiName + "_columnWidthList_")));
        v.setAutoColumnResize(Boolean.valueOf(p.getProperty(viewmodiName + "_autoColumnResize_")));
        v.setCaption(String.valueOf(p.getProperty(viewmodiName + "_caption_")));
        v.setShowFilterPanel(Boolean.valueOf(p.getProperty(viewmodiName + "_showFilterPanel_")));
        v.setHideAllFiles(Boolean.valueOf(p.getProperty(viewmodiName + "_hideAllFiles_")));
        v.setHideAllFolder(Boolean.valueOf(p.getProperty(viewmodiName + "_hideAllFolder_")));
        v.setLookAndFeel(String.valueOf(p.getProperty(viewmodiName + "_lookAndFeel_")));
        v.setRememberWindowsPosAndSize(Boolean.valueOf(p.getProperty(viewmodiName + "_rememberWindowsPosAndSize_")));
        v.setSearchFilesFolderThreadCount(Integer.valueOf(p.getProperty(viewmodiName + "_searchFilesFolderThreadCount_")));
        v.setSearchInFileContentThreadCount(Integer.valueOf(p.getProperty(viewmodiName + "_searchInFileContentThreadCount_")));

        SearchFilterData sfd;
        ArrayList<SearchFilterData> sfdList = new ArrayList();
        if (p.getProperty(viewmodiName + "_viewmodiCountSFD_") != null) {
            int sc = Integer.valueOf(p.getProperty(viewmodiName + "_viewmodiCountSFD_"));
            for (int s = 0; s < sc; s++) {
                sfd = new SearchFilterData();
                sfd.setDocType(p.getProperty(viewmodiName + "_filetype_" + s));
                sfd.setHandleMethode(p.getProperty(viewmodiName + "_handlemethode_" + s));
                sfd.setIsChecked(Boolean.valueOf(p.getProperty(viewmodiName + "_filetypecheck_" + s)));
                sfd.setMinDocSize(p.getProperty(viewmodiName + "_min_" + s));
                sfd.setMaxDocSize(p.getProperty(viewmodiName + "_max_" + s));
                sfd.setMaxCB_Value(Integer.valueOf(p.getProperty(viewmodiName + "_maxCB_Value_" + s)));
                sfd.setMinCB_Value(Integer.valueOf(p.getProperty(viewmodiName + "_minCB_Value_" + s)));
                sfd.setMaxTA_Value(p.getProperty(viewmodiName + "_maxTA_Value_" + s));
                sfd.setMinTA_Value(p.getProperty(viewmodiName + "_minTA_Value_" + s));
                sfdList.add(sfd);
            }

            v.setSFD_List(sfdList);

            SearchFilterData gSFD = new SearchFilterData();
            gSFD.setMaxDocSize(p.getProperty(viewmodiName + "_globalCalcMaxTA_"));
            gSFD.setMinDocSize(p.getProperty(viewmodiName + "_globalCalcMinTA_"));
            gSFD.setGlobalMaxCB(Integer.valueOf(p.getProperty(viewmodiName + "_globalMaxCB_")));
            gSFD.setGlobalMinCB(Integer.valueOf(p.getProperty(viewmodiName + "_globalMinCB_")));
            gSFD.setGlobalMaxTA(p.getProperty(viewmodiName + "_globalMaxTA_"));
            gSFD.setGlobalMinTA(p.getProperty(viewmodiName + "_globalMinTA_"));
            v.setGlobalMinMax(gSFD);
        }

        return v;
    }

    public void writeASingleViewmodi(Viewmodi v, String viewmodiName) {
        // read viewmodi from property-file:
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        p.setProperty(viewmodiName + "_columnCode_", v.getColumnCode());
        p.setProperty(viewmodiName + "_columnWidthList_", v.getColumnWidthList());
        p.setProperty(viewmodiName + "_autoColumnResize_", String.valueOf(v.getAutoColumnResize()));
        p.setProperty(viewmodiName + "_caption_", v.getCaption());
        p.setProperty(viewmodiName + "_showFilterPanel_", String.valueOf(v.getShowFilterPanel()));
        p.setProperty(viewmodiName + "_hideAllFiles_", String.valueOf(v.getHideAllFiles()));
        p.setProperty(viewmodiName + "_hideAllFolder_", String.valueOf(v.getHideAllFolder()));
        p.setProperty(viewmodiName + "_lookAndFeel_", String.valueOf(v.getLookAndFeel()));
        p.setProperty(viewmodiName + "_rememberWindowsPosAndSize_", String.valueOf(v.getRememberWindowsPosAndSize()));
        p.setProperty(viewmodiName + "_searchFilesFolderThreadCount_", String.valueOf(v.getSearchFilesFolderThreadCount()));
        p.setProperty(viewmodiName + "_searchInFileContentThreadCount_", String.valueOf(v.getSearchInFileContentThreadCount()));

        // SFD-Data for this viewmodi: -----------------------------------------------------------------
        // For example: viewmodiCountSFD_3=7 means: there are 7 SFD-Date for viewmodi Nr. 3.
        p.setProperty(viewmodiName + "_viewmodiCountSFD_", String.valueOf(v.getSFD_List().size()));

        // Überschreibe viewmodi mit den richtigen werten:
        for (int s = 0; s < v.getSFD_List().size(); s++) {

            p.setProperty(viewmodiName + "_filetype_" + s, v.getSFD_List().get(s).getDocType());
            p.setProperty(viewmodiName + "_filetypecheck_" + s, String.valueOf(v.getSFD_List().get(s).getIsChecked()));
            p.setProperty(viewmodiName + "_handlemethode_" + s, v.getSFD_List().get(s).getHandleMethode());
            p.setProperty(viewmodiName + "_min_" + s, v.getSFD_List().get(s).getMinDocSize());
            p.setProperty(viewmodiName + "_max_" + s, v.getSFD_List().get(s).getMaxDocSize());
            p.setProperty(viewmodiName + "_minCB_Value_" + s, String.valueOf(v.getSFD_List().get(s).getMinCB_Value()));
            p.setProperty(viewmodiName + "_maxCB_Value_" + s, String.valueOf(v.getSFD_List().get(s).getMaxCB_Value()));
            p.setProperty(viewmodiName + "_minTA_Value_" + s, v.getSFD_List().get(s).getMinTA_Value());
            p.setProperty(viewmodiName + "_maxTA_Value_" + s, v.getSFD_List().get(s).getMaxTA_Value());
        }

        // Global MinMax-Data: --------------------------------------------------------
        p.setProperty(viewmodiName + "_globalCalcMinTA_", v.getGlobalMinMax().getMinDocSize());
        p.setProperty(viewmodiName + "_globalCalcMaxTA_", v.getGlobalMinMax().getMaxDocSize());
        p.setProperty(viewmodiName + "_globalMinTA_", v.getGlobalMinMax().getGlobalMinTA());
        p.setProperty(viewmodiName + "_globalMaxTA_", v.getGlobalMinMax().getGlobalMaxTA());
        p.setProperty(viewmodiName + "_globalMinCB_", String.valueOf(v.getGlobalMinMax().getGlobalMinCB()));
        p.setProperty(viewmodiName + "_globalMaxCB_", String.valueOf(v.getGlobalMinMax().getGlobalMaxCB()));

        p.writePropFile();
    }

    /**
     * Generate and returns a viewmodi with default values.
     *
     * @return View
     */
    public Viewmodi getDefaultViewmodi() {

        // set defaultvalues in viewmodi constructor:
        Viewmodi v = new Viewmodi();

        v.setSFD_List(this.getDefaultSFD());
        v.setGlobalMinMax(new SearchFilterData());
        return v;
    }

    /**
     * Setter for easy addition SFD-File.
     *
     * @param docType
     * @param handleMethode
     */
    private ArrayList<SearchFilterData> listadd_docType_handleMethode_to_a_SFOList(String docType, int handleMethode, ArrayList<SearchFilterData> list) {
        SearchFilterData data = new SearchFilterData();
        data.setDocType(docType);
        data.setHandleMethode(String.valueOf(handleMethode));
        list.add(data);
        return list;
    }

    /**
     * Creats and returns a defaultvalues-set for SFD.
     *
     * @return defaultvalues-set.
     */
    private ArrayList<SearchFilterData> getDefaultSFD() {
        ArrayList<SearchFilterData> list = new ArrayList();

        list = listadd_docType_handleMethode_to_a_SFOList(".txt", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".xml", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".pdf", 1, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".zip", 2, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".rar", 3, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".7z", 4, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".jar", 5, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".html", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".java", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".log", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".js", 0, list);
        list = listadd_docType_handleMethode_to_a_SFOList(".ini", 0, list);

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setIsChecked(Boolean.TRUE);
            list.get(i).setMaxTA_Value("");
            list.get(i).setMinTA_Value("");
            list.get(i).setMaxDocSize("");
            list.get(i).setMinDocSize("");
        }

        return list;
    }

    /**
     * Overwrite the values of the actual viewmodi.
     *
     * @param v
     * @param actualVMCount
     */
    public void overwriteActualViewmodi(Viewmodi v, int actualVMCount) {

        // read viewmodi from property-file:
        FiltItProperties p = new FiltItProperties();
        p.readPropFile();

        int vmIndex = actualVMCount;

        if (!"null".equals(p.getProperty("viewmodiCountSFD_" + vmIndex))) {
            // Delete SFD for actual viewmodi:
            for (int k = 0; k < Integer.valueOf(p.getProperty("viewmodiCountSFD_" + vmIndex)); k++) {
                p.remove("filetype_" + vmIndex + "," + k);
                p.remove("filetypecheck_" + vmIndex + "," + k);
                p.remove("handlemethode_" + vmIndex + "," + k);
                p.remove("min_" + vmIndex + "," + k);
                p.remove("max_" + vmIndex + "," + k);
                p.remove("minCB_Value_" + vmIndex + "," + k);
                p.remove("maxCB_Value_" + vmIndex + "," + k);
                p.remove("minTA_Value_" + vmIndex + "," + k);
                p.remove("maxTA_Value_" + vmIndex + "," + k);
            }
            p.remove("viewmodiCountSFD_" + vmIndex);
        }
        p.setProperty("columnCode_" + vmIndex, v.getColumnCode());
        p.setProperty("columnWidthList_" + vmIndex, v.getColumnWidthList());
        p.setProperty("autoColumnResize_" + vmIndex, String.valueOf(v.getAutoColumnResize()));
        p.setProperty("caption_" + vmIndex, v.getCaption());
        p.setProperty("showFilterPanel_" + vmIndex, String.valueOf(v.getShowFilterPanel()));
        p.setProperty("hideAllFiles_" + vmIndex, String.valueOf(v.getHideAllFiles()));
        p.setProperty("hideAllFolder_" + vmIndex, String.valueOf(v.getHideAllFolder()));
        p.setProperty("lookAndFeel_" + vmIndex, String.valueOf(v.getLookAndFeel()));
        p.setProperty("rememberWindowsPosAndSize_" + vmIndex, String.valueOf(v.getRememberWindowsPosAndSize()));
        p.setProperty("searchFilesFolderThreadCount_" + vmIndex, String.valueOf(v.getSearchFilesFolderThreadCount()));
        p.setProperty("searchInFileContentThreadCount_" + vmIndex, String.valueOf(v.getSearchInFileContentThreadCount()));

        // SFD-Data for this viewmodi: -----------------------------------------------------------------
        // For example: viewmodiCountSFD_3=7 means: there are 7 SFD-Date for viewmodi Nr. 3.
        p.setProperty("viewmodiCountSFD_" + vmIndex, String.valueOf(v.getSFD_List().size()));

        // Überschreibe viewmodi mit den richtigen werten:
        for (int s = 0; s < v.getSFD_List().size(); s++) {
            p.setProperty("filetype_" + vmIndex + "," + s, v.getSFD_List().get(s).getDocType());
            p.setProperty("filetypecheck_" + vmIndex + "," + s, String.valueOf(v.getSFD_List().get(s).getIsChecked()));
            p.setProperty("handlemethode_" + vmIndex + "," + s, v.getSFD_List().get(s).getHandleMethode());
            p.setProperty("min_" + vmIndex + "," + s, v.getSFD_List().get(s).getMinDocSize());
            p.setProperty("max_" + vmIndex + "," + s, v.getSFD_List().get(s).getMaxDocSize());
            p.setProperty("minCB_Value_" + vmIndex + "," + s, String.valueOf(v.getSFD_List().get(s).getMinCB_Value()));
            p.setProperty("maxCB_Value_" + vmIndex + "," + s, String.valueOf(v.getSFD_List().get(s).getMaxCB_Value()));
            p.setProperty("minTA_Value_" + vmIndex + "," + s, v.getSFD_List().get(s).getMinTA_Value());
            p.setProperty("maxTA_Value_" + vmIndex + "," + s, v.getSFD_List().get(s).getMaxTA_Value());
        }

        // Global MinMax-Data: --------------------------------------------------------
        p.setProperty("globalCalcMinTA_" + vmIndex, v.getGlobalMinMax().getMinDocSize());
        p.setProperty("globalCalcMaxTA_" + vmIndex, v.getGlobalMinMax().getMaxDocSize());
        p.setProperty("globalMinTA_" + vmIndex, v.getGlobalMinMax().getGlobalMinTA());
        p.setProperty("globalMaxTA_" + vmIndex, v.getGlobalMinMax().getGlobalMaxTA());
        p.setProperty("globalMinCB_" + vmIndex, String.valueOf(v.getGlobalMinMax().getGlobalMinCB()));
        p.setProperty("globalMaxCB_" + vmIndex, String.valueOf(v.getGlobalMinMax().getGlobalMaxCB()));

        p.writePropFile();
    }

    /**
     * Delete a viewmodi from vList.
     *
     * @param i Index int means the position in vList.
     */
    public void deleteViewmodi(int i) {
        vList.remove(i);
    }

    /**
     * Delete a viewmodi from vList.
     *
     * @param rows Index int means the position in vList.
     */
    public void deleteViewmodi(int[] rows) {

        ArrayList<Viewmodi> result = new ArrayList();
        for (int i = 0; i < rows.length; i++) {
            vList.set(rows[i], null);
        }
        for (int l = 0; l < vList.size(); l++) {
            if (vList.get(l) != null) {
                result.add(vList.get(l));
            }
        }
        vList = result;
    }

    /**
     * Check if there is already a viewmodi with the given name registered.
     *
     * @param result
     * @return true if there is a viewmodi withe the same name, elso no.
     */
    boolean viewmodiNameAlreadyTaken(String result) {
        Viewmodi v = new Viewmodi();
        v.setCaption(result);
        if (getIndexOfViewmodi(v) < 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Calculate the index from a viewmodi.
     *
     * @param v viewmodi
     * @return index from viewmodi-list.
     */
    public int getIndexOfViewmodi(Viewmodi v) {

        // for an choosen viewmodi:
        if (v != null) {
            for (int i = 0; i < vList.size(); i++) {
                if (vList.get(i).getCaption().equals(v.getCaption())) {
                    return i;
                }
            }
        } else {// if parameter v == null, use actual viewmodi from this class:
            v = this.getActualViewmodi();

            // if a viewmodi has been choosed:
            if (v != null) {
                for (int i = 0; i < vList.size(); i++) {
                    if (vList.get(i).getCaption().equals(v.getCaption())) {
                        return i;
                    }
                }
            }
        }

        return -1;
    }

    /**
     * Exchange the position of two viewmodi in the viewmodi-list.
     *
     * @param from pos of viewmodi 1
     * @param to pos of viewmodi 2
     */
    public void exchangeVM(int from, int to) {
        // Wenn Input nicht unlogisch:
        if (from >= 0 && to >= 0 && to + 1 < this.vList.size()) {
            Viewmodi v;
            v = vList.get(to);
            vList.set(to, vList.get(from));
            vList.set(from, v);
        }
    }

    /**
     * Sorting all items from in the Lost.
     */
    public void sortSFO(ArrayList<SearchFilterData> sfd) {
        if (sfd != null && !sfd.isEmpty()) {
            Collections.sort(sfd);
        }
    }

    /**
     * Get the optionsetting for an filetyp and its handlemodus.
     * @param filetype
     * @param handlemodus
     * @return SearchFilterData
     */
    public SearchFilterData getSfdFromActualViewmodiForFiletypeAndHandlemodus(String filetype, Integer handlemodus) {
        ArrayList<SearchFilterData> actualSFD = this.actualViewmodi.getSFD_List();
        // Für jeden Eintrag in den SearchFolderOptions:
        for (int i = 0; i < actualSFD.size(); i++) {
            if (actualSFD.get(i).getDocType().equals(filetype)
                    && Integer.valueOf(actualSFD.get(i).getHandleMethode()) == handlemodus) {

                return actualSFD.get(i);
            }
        }
        // null if there is no search option set from user -> skip!
        return null;
    }

    public boolean hasViewmodiSameConfiguration(Viewmodi v1, Viewmodi v2) {

        if (v1.getAutoColumnResize() != v2.getAutoColumnResize()) {
            return false;
        }

        if (v1.getHideAllFiles() != v2.getHideAllFiles()) {
            return false;
        }
        if (v1.getHideAllFolder() != v2.getHideAllFolder()) {
            return false;
        }

        if (v1.getRememberWindowsPosAndSize() != v2.getRememberWindowsPosAndSize()) {
            return false;
        }

        if (v1.getShowFilterPanel() != v2.getShowFilterPanel()) {
            return false;
        }

        if (!v1.getColumnCode().equals(v2.getColumnCode())) {
            return false;
        }
        if (!v1.getAutoColumnResize()) {
            if (v1.getColumnWidthList().equals(v2.getColumnWidthList())) {
                return false;
            }
        }

        
        if (!v1.getGlobalMinMax().equals(v2.getGlobalMinMax())) {
            return false;
        }

        if (!v1.getLookAndFeel().equals(v2.getLookAndFeel())) {
            return false;
        }

        if (v1.getSearchFilesFolderThreadCount() != v2.getSearchFilesFolderThreadCount()) {
            return false;
        }

        if (v1.getSearchInFileContentThreadCount() != v2.getSearchInFileContentThreadCount()) {
            return false;
        }

        if (v1.getSFD_List().size() != v2.getSFD_List().size()) {
            return false;
        }

        // check also all SFD-Data from SFD-List:
        for (int i = 0; i < v1.getSFD_List().size(); i++) {
            if (!v1.getSFD_List().get(i).equals(v2.getSFD_List().get(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if two objects have the same values.
     *
     * @param list1
     * @param list2
     * @return
     */
    public boolean isSameList(ArrayList<SearchFilterData> list1, ArrayList<SearchFilterData> list2) {
        if (list1 == null || list2 == null) {
            return false;
        }
        if (list1.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list1.size(); i++) {

            // check minTA-Values:
            if (!list1.get(i).getMinTA_Value().equals(list2.get(i).getMinTA_Value())) {
                return false;
            }
            // check maxTA-Values:
            if (!list1.get(i).getMaxTA_Value().equals(list2.get(i).getMaxTA_Value())) {
                return false;
            }
            // check ischecked:
            if (list1.get(i).getIsChecked() != list2.get(i).getIsChecked()) {
                return false;
            }
            // check minCB-Values:
            if (list1.get(i).getMinCB_Value() != list2.get(i).getMinCB_Value()) {
                return false;
            }
            // check minCB-Values:
            if (list1.get(i).getMaxCB_Value() != list2.get(i).getMaxCB_Value()) {
                return false;
            }
        }
        return true;
    }

    // ---------------- Default Getter & Setter --------------------------
    /**
     * Getter.
     *
     * @return ArrayList.
     */
    public ArrayList<Viewmodi> getvList() {
        return vList;
    }

    /**
     * Setter.
     *
     * @param vList ArrayList.
     */
    public void setvList(ArrayList<Viewmodi> vList) {
        this.vList = vList;
    }

    public Viewmodi getActualViewmodi() {
        return actualViewmodi;
    }

    public void setActualViewmodi(Viewmodi actualViewmodi) {
        this.actualViewmodi = Viewmodi.getDeepCopy(actualViewmodi);
    }

    public Viewmodi getChangedViewmodi() {
        return changedViewmodi;
    }

    public void setChangedViewmodi(Viewmodi changedViewmodi) {
        this.changedViewmodi = Viewmodi.getDeepCopy(changedViewmodi);
    }
}
