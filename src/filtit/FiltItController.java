package filtit;

import java.awt.Image;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.EventListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.JTableHeader;

/**
 * Main-Controller.
 *
 * @author Hans-Georg Schladitz
 */
public class FiltItController implements ActionListener, EventListener,
        KeyListener, ComponentListener, WindowListener,
        MouseListener, PopupMenuListener, TableColumnModelListener,
        DocumentListener, ItemListener
//        , FocusListener
{

    /**
     * View.
     */
    private final FiltItView view;
    /**
     * Model.
     */
    private final FiltItModel model;

    private final String ChooseFolderDialogTitle = "Choose a path";
    private final String srcTFfolderpath = "Insert folder path here!";
    private final String srcTFfind = "Insert a keyword here!";

    /**
     * Constructor.
     */
    public FiltItController() {
        model = new FiltItModel();
        view = new FiltItView(this);

        // Set observer:
        model.addObserver(view);

        if (model.getVM().getActualViewmodi() != null) {
            if (model.getVM().getActualViewmodi().getRememberWindowsPosAndSize()) {
                view.setFrameStartBounds(model.readWindowsPositionAndSize());
            } else {
                // Set windowsposition and -size:
                view.setFrameStartBounds(null);
            }
        } else {
            view.setFrameStartBounds(null);
        }// Set observer:

        model.update();
        validate("");
    }

    /**
     * Starte Suche im Model: 1. Suche nach Folder oder und Dateien. 2. Starte
     * Filtern, wenn Suchbegriff(Find) gesetzt.
     */
    private void startSearchAfterFolderInput() {

        // if no viewmodi is selected:
        if (model.getVM().getActualViewmodi() == null) {
            // use actual setted values as acutal viewmodi:
            Viewmodi v;
            v = getActualSettingsAsViewmodiFromView();
            model.getVM().setActualViewmodi(v);
        }
        // Starte file- and folder search
        // BUT only if there are no old searchresults:
        if(model.getDataModel().getDatalist().size() < 1) {
            model.startFileFolderSearchThread(this);
        }
    }
    /**
     * Starte Suche im Model: 1. Suche nach Folder oder und Dateien. 2. Starte
     * Filtern, wenn Suchbegriff(Find) gesetzt.
     */
    private void startSearch() {

        // if no viewmodi is selected:
        if (model.getVM().getActualViewmodi() == null) {
            // use actual setted values as acutal viewmodi:
            Viewmodi v;
            v = getActualSettingsAsViewmodiFromView();
            model.getVM().setActualViewmodi(v);
        }

        // 2. Starte Filtern wenn Suchbegriff(Find) gesetzt
        model.startSearchInThread(this);
    }

    /**
     * Setze FolderPath und Suchbegriff(Find) im Model, die aktuell in der View
     * stehen.
     */
    private void setFolderPathAndFind() {
        if (view.getFolderPathTextArea().equals(srcTFfolderpath)) {
            model.setFolderPath("");
            view.setFolderPathTextArea(srcTFfolderpath);
        } else {
            model.setFolderPath(view.getFolderPathTextArea());
        }
        if (view.getFindTextArea().equals(srcTFfind)) {
            model.setFind("");
            view.setFindTextArea(srcTFfind);
        } else {
            model.setFind(view.getFindTextArea());
        }
    }

    /**
     * Manage ChooseFolder-Dialog.
     *
     * @return String Path from choosen file.
     */
    private String chooseFolder() {
        File temp;
        JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(null);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setDialogTitle(ChooseFolderDialogTitle);
        int value = fc.showDialog(view, ChooseFolderDialogTitle);
        // Wenn nichts gewählt wurde:
        if (value == 1) {
            return "";
        }
        // Sonst, wenn kein Fehler:
        fc.setVisible(true);
        temp = fc.getSelectedFile();

        return temp.getPath();
    }

    /**
     * Wird aufgerufen durch ViewmodiManagementFrame. Siehe ActionListoner.
     */
    public void updateViewModiList() {
        view.getFiltItMenuBar().initViewmodi(model.getVM());
        model.update();
    }

    /**
     * Bekommt vom ColumnManagementFrame den Aufruf mit gewählten Spalten in
     * richtiger Reihenfolge via CheckboxList.
     *
     * @param checkBoxList ArrayList
     */
    public void refreshColumnStructure(ArrayList<JCheckBox> checkBoxList) {

        ArrayList<Integer> columnCode = new ArrayList();

        for (int i = 0; i < checkBoxList.size(); i++) {
            if (checkBoxList.get(i).isSelected()) {
                columnCode.add(Integer.valueOf(checkBoxList.get(i).getName()));
            }
        }

        model.getVM().getChangedViewmodi().setColumnCode(Utilities.castArrayListToString(columnCode));
        model.update();
    }

    /**
     * Check Folder- & File-input for invalids and corrects it.
     */
    private void clearInput() {
        // Entferne Zeilenumbrüche für FolderInput:
        String tmp = view.getFolderPathTextArea();
        tmp = tmp.replace("\n\r", "");
        tmp = tmp.replace("\n", "");
        tmp = tmp.trim();
        view.setFolderPathTextArea(tmp);
        // Entferne Zeilenumbrüche für FindInput:
        tmp = view.getFindTextArea();
        tmp = tmp.replace("\n\r", "");
        tmp = tmp.replace("\n", "");
        tmp = tmp.trim();
        view.setFindTextArea(tmp);
    }

    /**
     * Check if an event triggers a popupmenu.
     *
     * @param e event
     */
    private void checkPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            if (e.getComponent().getName() != null) {
                // Wenn in ein normales Tabellenfeld Rechtsklick gemacht wurde:
                if (e.getComponent().getName().equals("ttv")) {
                    //System.out.println("ttv");
                    view.getTablePopupmenu().getPopup().show(e.getComponent(), e.getX(), e.getY());
                }
                // Wenn in den Header Rechtsklick gemacht wurde:
                if (e.getComponent().getName().equals("Header")) {
                    view.getHPMPopup().getPopup().show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }

    /**
     * Get viewmodimanagement.
     *
     * @return Viewmodimanagement.
     */
    public ViewmodiManagement getVM() {
        return model.getVM();
    }

    /**
     * Overwrite the settings from actual viewmodi.
     */
    private void overwriteActualViewmodiSettings() {
        // if a viewmodi has been choosed:
        if (model.getVM().getIndexOfViewmodi(null) >= 0) {
            Viewmodi v = getActualSettingsAsViewmodiFromView();
            v.setCaption(model.getVM().getChangedViewmodi().getCaption());
            int index = model.getVM().getIndexOfViewmodi(null);
            model.getVM().overwriteActualViewmodi(v, index);

            model.getVM().readViewmodiList();
            changeViewmodi(index);

        }
    }

    /**
     * Validate all. validate syntax and semantic.
     */
    private void validate(String option) {

        // 1. syntaxcheck:
        boolean isCorrectSyntax = view.isValide();

        // 2. semanticcheck:
        boolean isThereAChange = !model.getVM().hasViewmodiSameConfiguration(
                model.getVM().getActualViewmodi(),
                getActualSettingsAsViewmodiFromView()
        );

        // if discard is pressed:
        // normal validate is impossible, because update still in progress 
        // after discard-button is clicked       
        if (option.equals("disableDiscard")) {
            isThereAChange = false;
        }

        // on error prevent an apply and save:
        if (isCorrectSyntax) {
            view.enableApplySaveButtons(true);
        } else {
            view.enableApplySaveButtons(false);
        }

        // on change activate discardButtons:
        if (isThereAChange) {
            view.enableDiscardButtons(true);
        } else {
            view.enableDiscardButtons(false);
        }
    }

    // ActionListenerEvents: --------------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {

//        System.out.println("ActionPerformed: " + e.getActionCommand());
        // Wenn im Menu AutoColumnResize geklickt wurde:        
        if (e.getActionCommand().contains("autoColumnResize")) {
            autoColumnResizeOption();

            validate("");
        }
        // Wenn ein Viewmodi angeklickt wurde:
        if (e.getActionCommand().contains("viewmodi")) {
            changeViewmodi(Integer.valueOf(e.getActionCommand().substring(8)));
        }
        // Wenn ein Viewmodi-Management angeklickt wurde:
        if (e.getActionCommand().contains("vmManagement")) {
            ViewmodiManagementFrame vmf = new ViewmodiManagementFrame("Viewmode Management", this, model.getVM());
        }
        // Wenn overwriteActualVM geklickt wurde:
        if (e.getActionCommand().contains("overwriteActualVM")) {
            overwriteActualViewmodiSettings();
        }
        // Wenn overwriteActualVM geklickt wurde:
        if (e.getActionCommand().contains("bShowFilterPanel")) {
            filterPanelOption();
        }
        // if the start-button is clicked:
        if (e.getActionCommand().contains("startSearchButton")) {
            clearInput();
            setFolderPathAndFind();
            startSearch();
        }
        // if the clear-button is clicked:
        if (e.getActionCommand().contains("clearResultsButton")) {
            model.clearLastResults();
            this.update();
        }

        // Wenn ein FolderPath über den Button eingegeben wurde.        
        if (e.getActionCommand().equals("chooseFolderBtn")) {
            view.setFolderPathTextArea(chooseFolder());
            setFolderPathAndFind();
            startSearchAfterFolderInput();
        }
        // Wenn "Add column" geklickt wurde:
        if (e.getActionCommand().equals("addColumn")) {
            // Öffne ColumnManagementFrame:
            ColumnManagementFrame cmf = new ColumnManagementFrame("Column Management", model.getDataModel(), this, model.getVM().getChangedViewmodi());
        }
        // Wenn im HeaderPopupMenu "deleteColumn" angeklickt wurde:
        if (e.getActionCommand().equals("deleteColumn")) {

            if (model.getVM().getChangedViewmodi() != null) {
                int deleteCount = view.getHPMPopup().getChoosedColumn();

                // get values from viewmodi:
                ArrayList<Integer> columnCode = Utilities.castStringToArrayList(model.getVM().getChangedViewmodi().getColumnCode());
                ArrayList<Integer> withList = Utilities.castStringToArrayList(model.getVM().getChangedViewmodi().getColumnWidthList());

                // remove values:
                columnCode.remove(deleteCount);
                if (columnCode.size() + 1 == withList.size()) {
                    withList.remove(deleteCount);
                }

                // set values in viewmodi:
                model.getVM().getChangedViewmodi().setColumnCode(Utilities.castArrayListToString(columnCode));
                model.getVM().getChangedViewmodi().setColumnWidthList(Utilities.castArrayListToString(withList));

            }
            update();

        }
        // Wenn im HeaderPopupMenu "save as new viewmodi" angeklickt wurde:
        if (e.getActionCommand().equals("saveViewmodi")) {

            String iconName = "icon32.png";
            IconManager im = new IconManager();
            Image img = im.createImageIcon(iconName).getImage();

            // Dialog-MessageBox:
            Object oResult = JOptionPane.showInputDialog(null,
                    "Please insert a viewmode name!", "New viewmode",
                    JOptionPane.QUESTION_MESSAGE, new ImageIcon(img), null,
                    "viewmode" + this.model.getVM().getvList().size());

            // If user clicked abourt:
            if (oResult == null) {
                return;
            }

            // Cast to String:
            String result = (String) oResult;

            // Error-Check:
            while (result.equals("") && !model.getVM().viewmodiNameAlreadyTaken(result)) {
                result = (String) JOptionPane.showInputDialog(null,
                        "Invalid input! Try to insert a valid input!",
                        "Add a new viewmode", JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon(img), null, "viewmode" + this.model.getVM().getvList().size());
            }


            model.getVM().setChangedViewmodi(getActualSettingsAsViewmodiFromView());

            Viewmodi v = getActualSettingsAsViewmodiFromView();

            v.setCaption(result);

            // write the new viewmodi in the property-file:
            model.getVM().writeNewViewmodiForViewmodiList(v);

            // reinitiate all viewmoid for menu-refresh:
            view.initViewmodiInMenubar(model.getVM());

            changeViewmodi(model.getVM().getvList().size() - 1);
        }
        // Wenn im TablePopupMenu "open containing folder" angeklickt wurde:
        if (e.getActionCommand().equals("openContainingFolder")) {
            model.openContainingFolderForActualRegardedCell();
        }
        // Wenn im TablePopupMenu "open containing folder" angeklickt wurde:
        if (e.getActionCommand().equals("openDefaultApplication")) {
            model.openInDefaultApplicationForActualRegardedCell();
        }

        // Wenn der Button für den Filter gedrückt wurde:
        if (e.getActionCommand().equals("filterPanel")) {
            model.update();
            view.useColumnFilter();
        }

        // Wenn 'About' angeklickt wurde:
        if (e.getActionCommand().equals("about")) {
            AboutFrame aboutview = new AboutFrame();
        }

        // If 'hide all files' checked oder unchecked:
        if (e.getActionCommand().equals("hideFiles")) {
            JCheckBox cb = (JCheckBox) e.getSource();
            model.getVM().getChangedViewmodi().setHideAllFiles(cb.isSelected());
            model.update();
        }

        // If 'hide all folder' checked oder unchecked:
        if (e.getActionCommand().equals("hideFolder")) {
            JCheckBox cb = (JCheckBox) e.getSource();
            model.getVM().getChangedViewmodi().setHideAllFolder(cb.isSelected());
            model.update();
        }
        // If 'remember windowspos and -size' checked oder unchecked:
        if (e.getActionCommand().equals("rememberWindowsPosAndSize")) {
            JCheckBox cb = (JCheckBox) e.getSource();
            model.getVM().getChangedViewmodi().setRememberWindowsPosAndSize(cb.isSelected());
        }
        // SFO-View: Restore defaultvalues after button-click:
        if (e.getActionCommand().equals("restoreDefaultValues")) {
            model.getVM().setChangedViewmodi(model.getVM().getDefaultViewmodi());
            model.update();
        }
        // SFO-View: If just 'Apply' has been clicked:
        if (e.getActionCommand().equals("apply")) {
            model.getVM().setActualViewmodi(this.getActualSettingsAsViewmodiFromView());
            model.getVM().setChangedViewmodi(model.getVM().getActualViewmodi());
        }
        // SFO-View: Save and Add--Button clicked:
        if (e.getActionCommand().equals("saveAndAdd")) {

            // save all values in the model:
            overwriteActualViewmodiSettings();

            model.update();
            validate("disableDiscard");
        }

        // SFO-View: Stelle alle alten Werte (alle aktuell gesetzen) wieder her:
        if (e.getActionCommand().equals("discard")) {
            model.getVM().setChangedViewmodi(model.getVM().getActualViewmodi());
            model.update();
            validate("disableDiscard");
        }
        // SFO-View: Wenn deleteOption gedrückt wurde:
        if (e.getActionCommand().contains("deleteType")) {

            // Zahlen gehen nicht von 0 bis x, sondern von 1 bis x+1:
            int index = Integer.valueOf(e.getActionCommand().substring("deleteType".length(), e.getActionCommand().length()));
            ArrayList<SearchFilterData> sfdL = (ArrayList<SearchFilterData>) model.getVM().getChangedViewmodi().getSFD_List().clone();
            sfdL.remove(index);
            model.getVM().getChangedViewmodi().setSFD_List(sfdL);
            model.update();
            validate("");
        }
        // SFO-View: Wenn deleteALLOption gedrückt wurde:
        if (e.getActionCommand().equals("deleteAllOption")) {
            ArrayList<SearchFilterData> sfdL = new ArrayList();
            model.getVM().getChangedViewmodi().setSFD_List(sfdL);
            model.update();
            validate("");
        }

        // SFO-View: Füge eine Optionszeile hinzu:
        if (e.getActionCommand().equals("addDocumenttype")) {
            addDocumentType();
            model.update();
            validate("");
        }
    }

    /**
     * Add a documentType as a SearchFolderOption. A line that represents an
     * Option for one documenttype an a handletype.
     */
    private void addDocumentType() {
        String iconName = "icon32.png";
        IconManager im = new IconManager();
        Image img = im.createImageIcon(iconName).getImage();

        // Dialog-MessageBox:
        Object oResult = JOptionPane.showInputDialog(null,
                "Insert your document type!", "Add new document type",
                JOptionPane.QUESTION_MESSAGE, new ImageIcon(img), null,
                ".txt");

        // If user clicked abourt:
        if (oResult == null) {
            return;
        }

        // Cast to String:
        String result = (String) oResult;

        // Error-Check:
        while (result.equals("")) {
            result = (String) JOptionPane.showInputDialog(null,
                    "Invalid input! Try to insert a valid input!",
                    "Add new document type", JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon(img), null, ".txt");
        }

        // Wenn SFO-Daten geladen wurden (Nicht Default-Values genommen wurden):      
        SearchFilterData data = new SearchFilterData();
        data.setDocType(result);
        ArrayList<SearchFilterData> sfdL = (ArrayList<SearchFilterData>) model.getVM().getChangedViewmodi().getSFD_List().clone();
        sfdL.add(data);
        model.getVM().getChangedViewmodi().setSFD_List(sfdL);
        model.update();
    }

    /**
     * Change the actual viewmodi in the Model.
     *
     * @param viewmodiIndex integer
     */
    private void changeViewmodi(int viewmodiIndex) {

        model.getVM().setActualViewmodi(model.getVM().getvList().get(viewmodiIndex));
        model.getVM().setChangedViewmodi(model.getVM().getvList().get(viewmodiIndex));

        model.update();
        validate("");
    }

    /**
     * Triggered after autoColumnResizeOption was clicked. If True: the columns
     * must resize immediately
     */
    private void autoColumnResizeOption() {
        // Wenn True:
        if (view.getAutoColumnResize()) {
            view.setColumnSize();
        }
    }

    /**
     * Is performed from filterPanelOption after Menu-click.
     */
    private void filterPanelOption() {

        // if the item is checked:
        if (view.getFiltItMenuBar().getCbShowFilterPanel().isSelected()) {
            view.showFilterPanel(true);
            if (model.getVM().getChangedViewmodi() != null) {
                model.getVM().getChangedViewmodi().setShowFilterPanel(Boolean.TRUE);
            }
        } else { // of the item is not checked:
            view.showFilterPanel(false);
            if (model.getVM().getChangedViewmodi() != null) {
                model.getVM().getChangedViewmodi().setShowFilterPanel(Boolean.FALSE);
            }
        }
    }

    /**
     * Generates a view of the actual Settings.
     *
     * @return Viewmode
     */
    private Viewmodi getActualSettingsAsViewmodiFromView() {
        Viewmodi v = new Viewmodi();

        v.setAutoColumnResize(view.getAutoColumnResize());
        v.setColumnCode(model.getVM().getChangedViewmodi().getColumnCode());
        v.setColumnWidthList(view.getColumnWidthAsString());
        v.setHideAllFiles(view.getCbHideFiles().isSelected());
        v.setHideAllFolder(view.getCbHideFolder().isSelected());
        v.setLookAndFeel(view.getCbLookAndFeel());
        v.setRememberWindowsPosAndSize(view.getCbRememberWindowsPosAndSize().isSelected());
        v.setSearchFilesFolderThreadCount(view.getSearchFilesFolderThreadCount());
        v.setSearchInFileContentThreadCount(view.getSearchInFileContentThreadCount());
        v.setShowFilterPanel(view.getFiltItMenuBar().getCbShowFilterPanel().isSelected());

        v.setSFD_List(view.getSFD_FromView());

        // if there is still nothing loaded in view:
        if (view.getSFD_FromView().size() == 0) {
            v.setSFD_List(model.getVM().getChangedViewmodi().getSFD_List());
        }

        v.setGlobalMinMax(view.getGlobalMinMax_FromView());

        return v;
    }

    /**
     * Saves the windowsposition and -size from the view in the property-file.
     */
    public void saveWindowsPositionAndSize() {
        model.saveWindowsPositionAndSize(view.getX(), view.getY(), view.getWidth(), view.getHeight());
    }

    /**
     * reads and sets the windowsposition and -size in the view.
     */
    public void loadWindowsPositionAndSize() {
        view.setFrameStartBounds(model.readWindowsPositionAndSize());
    }

    // KeyListenerEvents: -----------------------------------------------------
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e
    ) {
        if(e.getExtendedKeyCode() == 10 && (e.getComponent().getName().equals("folderpath") || e.getComponent().getName().equals("find"))){
            clearInput();
            setFolderPathAndFind();
            startSearch();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
//        System.out.println("keyReleased" + e.getExtendedKeyCode());
//        System.out.println("Component: " + e.getComponent().getName());

        // Wenn Strg + F im Fenster gedrückt wurde:
        if (e.isControlDown() && e.getExtendedKeyCode() == 70) {
            if (view.getFiltItMenuBar().getCbShowFilterPanel().isSelected()) {
                view.getFiltItMenuBar().getCbShowFilterPanel().setSelected(false);
            } else {
                view.getFiltItMenuBar().getCbShowFilterPanel().setSelected(true);
            }
            filterPanelOption();
        }
        // change to folder-textarea after backspace key:
        if (e.getExtendedKeyCode() == 9 && e.getComponent().getName().equals("folderpath")) {
            view.getTaFind().requestFocus();
            view.getTaFolder().setText(view.getTaFolder().getText().replaceAll("\\t",""));
        }
        // change to find-textarea after backspace key:
        if (e.getExtendedKeyCode() == 9 && e.getComponent().getName().equals("find")) {
            view.getSearchButton().requestFocus();
            view.getTaFind().setText(view.getTaFind().getText().replaceAll("\\t",""));
        }
        // start search after enter if the button is selected:
        if (e.getExtendedKeyCode() == 10 && view.getSearchButton().isSelected()) {
            clearInput();
            setFolderPathAndFind();
            startSearchAfterFolderInput();
        }

        if(e.getComponent() instanceof JButton){
            if(((JButton) e.getComponent()).getActionCommand().equals("startSearchButton")){
                if(e.getExtendedKeyCode() == 9){
                    view.getTaFolder().requestFocus();

                }
            }
        }

        // Enter in folderPath-Field
//        if (e.getExtendedKeyCode() == 10 && e.getComponent().getName().equals("folderpath")) {
//            clearInput();
//            setFolderPathAndFind();
//            startSearchAfterFolderInput();
//        }
//        // Enter in find-Field:
//        if (e.getExtendedKeyCode() == 10 && e.getComponent().getName().equals("find")) {
//            clearInput();
//            setFolderPathAndFind();
//            startSearchAfterFolderInput();
//        }
//        // Strg + V bei folderPath-Field
//        if (e.isControlDown() && e.getExtendedKeyCode() == 86 && e.getComponent().getName().equals("folderpath")) {
//            clearInput();
//            setFolderPathAndFind();
//            startSearchAfterFolderInput();
//        }
//        // Strg + V bei find-Field
//        if (e.isControlDown() && e.getExtendedKeyCode() == 86 && e.getComponent().getName().equals("find")) {
//            clearInput();
//            setFolderPathAndFind();
//            startSearchAfterFolderInput();
//        }
    }

    // CompunentListenerEvents: -----------------------------------------------
    @Override
    public void componentResized(ComponentEvent e) {
        // Wenn im View ein Haken bei AutoColumnResize gesetzt wurde
        if (view != null) {
            if (view.getAutoColumnResize()) {
                // Aktualisiere die Breiten der Spalten
                view.setColumnSize();
            }
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    // WindowListenerEvents: --------------------------------------------------
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {

        Viewmodi v = getActualSettingsAsViewmodiFromView();
        v.setCaption("Last used viewmode.");
        // save last used settings:
        model.getVM().writeASingleViewmodi(v, "lastUsedViewmode");
        // Save windowsposition and windowssize:
        saveWindowsPositionAndSize();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    // MouseListenerEvents: ---------------------------------------------------
    @Override
    public void mouseClicked(MouseEvent e) {
        // Wenn Doppelklick:
//        if (e.getClickCount() == 2) {
////            JTable target = (JTable) e.getSource();
////            int row = target.getSelectedRow();
////            int column = target.getSelectedColumn();
//            // do some action if appropriate column
//        }
        // Wenn Rechtsklick:
//        if (e.getButton() == 3) {
//
//        }
        // Wenn Rechtsklick auf den Header gemacht wurde:
//        if (e.getComponent().getName() != null) {
//            if (e.getButton() == 3 && e.getComponent().getName().equals("Header")) {
//                //System.out.println("RigthClick on Header.");
//            }
//        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getSource() != null) {
            // If a JTableHeader has been clicked:
            if (e.getSource() instanceof JTableHeader) {
                JTableHeader target = (JTableHeader) e.getSource();
                int column = target.getColumnModel().getColumnIndexAtX(e.getX());
                //System.out.println("Pressed: " + column);
                view.getHPMPopup().setChoosedColumn(column);
                checkPopup(e);
            }
            // If a JTable has been clicked:
            if (e.getSource() instanceof JTable) {
                JTable target = (JTable) e.getSource();

                // select a Cell:
                // select row:
                int r = target.rowAtPoint(e.getPoint());
                if (r >= 0 && r < target.getRowCount()) {
                    target.setRowSelectionInterval(r, r);
                } else {
                    target.clearSelection();
                }
                // select column:
                int c = target.columnAtPoint(e.getPoint());
                if (c >= 0 && c < target.getColumnCount()) {
                    target.setColumnSelectionInterval(c, c);
                } else {
                    target.clearSelection();
                }

                // ONLY if a cell is selcted:
                int row = target.getSelectedRow();
                int column = target.getSelectedColumn();

                model.setCell(row, column);
                checkPopup(e);
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    // PopupMenuListenerEvents: -----------------------------------------------
    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        //System.out.println("Popup menu will be visible!");        
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        //System.out.println("Popup menu will be invisible!");
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        //System.out.println("Popup menu is hidden!");
    }

    @Override
    public void columnAdded(TableColumnModelEvent e) {
    }

    @Override
    public void columnRemoved(TableColumnModelEvent e) {
    }

    @Override
    public void columnMoved(TableColumnModelEvent e) {
        if (model.getVM().getChangedViewmodi() != null) {

            ArrayList<Integer> columnCode = Utilities.castStringToArrayList(model.getVM().getChangedViewmodi().getColumnCode());
            // Wenn der SpaltenCode nicht leer ist:
            if (columnCode != null && columnCode.size() > 0) {

                // Wenn Spalten verändert werden:
                int to = columnCode.get(e.getToIndex());
                int from = columnCode.get(e.getFromIndex());
                columnCode.set(e.getToIndex(), from);
                columnCode.set(e.getFromIndex(), to);

                // ColumnCode im Datenmodel aktuallisieren:
                model.getVM().getChangedViewmodi().setColumnCode(Utilities.castArrayListToString(columnCode));
            }
        }
    }

    @Override
    public void columnMarginChanged(ChangeEvent e) {
        //TODO: turn autoresize off if tcolumnMarginChanged
        // Wird beim laden des fensters schon für jede Spalte geprogt
    }

    @Override
    public void columnSelectionChanged(ListSelectionEvent e) {}

    // DokumentListenerEvents: ------------------------------------------------
    @Override
    public void insertUpdate(DocumentEvent e) {
        view.useColumnFilter();
        validate("");
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        view.useColumnFilter();
        validate("");
    }

    @Override
    public void changedUpdate(DocumentEvent e) {}

    // ItemListenerEvents -----------------------------------------------------
    @Override
    public void itemStateChanged(ItemEvent e) {

        Object comp = e.getSource();
        if (comp instanceof LookAndFeelChooser) {

            if (e.getStateChange() == ItemEvent.SELECTED) {
                LookAndFeelChooser source = (LookAndFeelChooser) e.getSource();
                model.getVM().getChangedViewmodi().setLookAndFeel(source.getSelectedLookAndFeelClass().getClassName());
                view.loadLookAndFeel(source.getSelectedLookAndFeelClass().getClassName());
            }
        }
        // validate if JComboBox is changed:
        if (comp instanceof JComboBox) {
            validate("");
        }
    }

    public void update() {
        model.update();
    }

    public void setProgressbar(int val, int max) {
        if(max != 0)
        view.manageProgressbar(val,max);
    }
    // FocusListener-Events ---------------------------------------------------------
//    @Override
//    public void focusGained(FocusEvent e) {
////        if(e.getComponent() instanceof  JTabbedPane){
////            view.getTaFolder().setF
////        }
//    }
//
//    @Override
//    public void focusLost(FocusEvent e) {
//
//        // set focus to start element if button or another element is selected:
////        if (e.getComponent() instanceof JButton) {
////            if(((JButton) e.getComponent()).getActionCommand().equals("startSearchButton")){
////                view.getTaFolder().requestFocus();
////            }
////        }
//    }
}
