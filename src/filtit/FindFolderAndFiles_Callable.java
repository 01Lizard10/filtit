package filtit;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class FindFolderAndFiles_Callable implements Callable {

    private ArrayList<Data> templist;
    private ArrayList<File> fileList;

    public FindFolderAndFiles_Callable(ArrayList<File> fileList) {
        this.fileList = fileList;
        templist = new ArrayList();
        templist.clear();

    }

    @Override
    public Object call() throws Exception {

        Data data;
        for (int i = 0; i < fileList.size(); i++) {

            // If the element is a directory:
            if (fileList.get(i).isDirectory()) {
                data = new Data(fileList.get(i).getPath());
                data.setType("folder");
                templist.add(data);

                File[] dics = fileList.get(i).listFiles();
                if (dics != null) {
                    for (File dic : dics) {
                        if (dic.isDirectory() && (!dic.equals("null"))) {

                            data.setType("folder");
                            templist.add(data);
                            getAllDirectoriesAndFiles(dic.getPath());
                        }
                        if (dic.isFile() && (!dic.equals("null"))) {
                            data = new Data(dic.getPath());
                            data.setType("file");
                            templist.add(data);
                        }
                    }
                }
            } else {  // If the element is a file:
                data = new Data(fileList.get(i).getPath());
                data.setType("file");
                templist.add(data);

            }
        }

        return templist;
    }

    private void getAllDirectoriesAndFiles(String dicPfad) {

        File temp = new File(dicPfad);
        File[] dics = temp.listFiles();
        Data data;
        if (dics != null) {
            for (File dic : dics) {
                if (dic.isDirectory() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("folder");
                    templist.add(data);
                    getAllDirectoriesAndFiles(dic.getPath());
                }
                if (dic.isFile() && (!dic.equals("null"))) {
                    data = new Data(dic.getPath());
                    data.setType("file");
                    templist.add(data);
                }
            }
        }
    }
}
