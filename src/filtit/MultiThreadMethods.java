package filtit;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Best Practise für Nebenläufigkeit (concurrency): 1. Use local variables 2.
 * Prefere immutable Classes 3. Minimize locking scope 4. Prefer Thread Pool
 * Executors instead of Threads 5. Prefer Synchronization utility over wait
 * notify 6. Prefer BlockingQueue for Producer-Consumer design 7. Prefer
 * Concurrent Collections over synchronized Collection 8. Use Semaphore to
 * create bounds 9. Prefer synchronized block over synchronized method 10. Avoid
 * Using static variables 11. Prefer Lock over synchronized keyword
 *
 * @author Hans-Georg Schladitz
 */
public class MultiThreadMethods {

    /**
     * Main model.
     * used to update values live to feed the progressbar.
     */
    private FiltItModel model;
    private FiltItController controller;
    // ------------------------


    /**
     * Manage the cout of threads.
     * This happens already before one of this methods are invoked.
     */
    private int threadCount = 4;

    public MultiThreadMethods(FiltItModel model, FiltItController controller) {
        this.model = model;
        this.controller = controller;
    }

    public MultiThreadMethods() {

    }

    /**
     *
     * @param dataList result list
     * @param model MVC model
     * @return result list with new informations
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public ArrayList<Data> searchInFileContent_WithCallables(ArrayList<Data> dataList, FiltItModel model) throws InterruptedException, ExecutionException {
        // On invalid input abort:
        if (dataList == null || dataList.isEmpty()) {
            return dataList;
        }
        ArrayList<Data> templist = new ArrayList();

        ArrayList<Callable<ArrayList<Data>>> callables = new ArrayList();
        Callable<ArrayList<Data>> callable;
        ArrayList<Future<ArrayList<Data>>> futureList = new ArrayList();
        Future<ArrayList<Data>> future;

        ExecutorService executor = Executors.newCachedThreadPool();

        int part = (dataList.size() / this.threadCount);

        // Split Array in a ArrayList of ArrayList with same length:
        ArrayList<ArrayList<Data>> resultList = Utilities.calcEvenList(dataList, part, this.threadCount);

        // Callables erstellen und der Liste hinzufügen:
        for (int i = 0; i < resultList.size(); i++) {
            // Model darf und soll nur lesend verwendet werden (Optionen lesen):
            callable = new SearchInFileConent_Callable(resultList.get(i), model);
            callables.add(callable);
        }
        model.getDataModel().getDatalist().clear();
        // callables ausführen:
        for (int i = 0; i < callables.size(); i++) {
            future = executor.submit(callables.get(i));
            futureList.add(future);
        }


        try {
        // Auf Ergebnisse warten und wenn sie da sind der ErgebnisListe hinzufügen:
        if(futureList != null) {
            for (int i = 0; i < futureList.size(); i++) {

                    if (futureList.get(i).get() != null) {
                        templist.addAll(futureList.get(i).get());
                        // --- For Processbar:
//                        System.out.println("test: " + dataList.size() + " " + model.getDataModel().getDatalist().size());
                        this.model.setProgressvalue(templist.size());
                        this.model.getDataModel().setDatalist(dataList);
                        controller.setProgressbar(this.model.getProgressValue(), model.getDataModel().getDatalist().size());
                        // ---
                    }

            }
        }
    }catch (Exception e){
        System.out.println("Error in a search thread:");
    }
        executor.shutdown();
        return templist;
    }

    /**
     * Searching all files and folder for a given folder.
     * @param file
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public ArrayList<Data> findFolderAndFiles_WithCallables(File file) throws InterruptedException, ExecutionException {
        ArrayList<Data> templist = new ArrayList();

        ArrayList<Callable<ArrayList<Data>>> callables = new ArrayList();
        Callable<ArrayList<Data>> callable;
        ArrayList<Future<ArrayList<Data>>> futureList = new ArrayList();
        Future<ArrayList<Data>> future;

        ExecutorService executor = Executors.newCachedThreadPool();

        File[] dics = file.listFiles();
        int part = (dics.length / this.threadCount);

        if (dics != null) {
            // Split array in a ArrayList of ArrayList with same length:
            ArrayList<ArrayList> resultList = Utilities.calcEvenList(dics, part, this.threadCount);

            // Callables erstellen und der Liste hinzufügen:
            for (int i = 0; i < resultList.size(); i++) {
                callable = new FindFolderAndFiles_Callable(resultList.get(i));
                callables.add(callable);
            }

            // callables ausführen:
            for (int i = 0; i < callables.size(); i++) {
                future = executor.submit(callables.get(i));
                futureList.add(future);
            }

            // Auf Ergebnisse warten und wenn sie da sind der ErgebnisListe hinzufügen:
            for (int i = 0; i < futureList.size(); i++) {
                templist.addAll(futureList.get(i).get());
            }

            executor.shutdown();
        }

        return templist;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

}
