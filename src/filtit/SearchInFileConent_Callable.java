package filtit;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class SearchInFileConent_Callable implements Callable {

    private ArrayList<Data> resultList;
    private final FiltItModel model;

    public SearchInFileConent_Callable(ArrayList<Data> al, FiltItModel model) {
        this.resultList = al;
        this.model = model;
    }

    @Override
    public Object call() throws Exception {

        Data entity;
        SearchEngine se = new SearchEngine(model);        
        
        for (int i = 0; i < resultList.size(); i++) {
            entity = se.searchWithModi(resultList.get(i), model.getFind());
            if (entity != null) {
                resultList.set(i, entity);
            } else {
                resultList.set(i, null);
            }
        }

        // Neugefundene Datien anpassen und der Datenliste hinzufügen:  
        for (int i = 0; i < resultList.size(); i++) {

            if (resultList.get(i) != null) {
                for (int k = 0; k < resultList.get(i).getChildList().size(); k++) {

                    // Set path from in archive founded files the path from the archive:
                    resultList.get(i).getChildList().get(k).setPath(resultList.get(i).getPath());

                    // Set FULLpath from in archive founded files the path from the archive:
                    resultList.get(i).getChildList().get(k).setFullpath(resultList.get(i).getFullpath());
                    // HINWEIS: Durch gleiche FULL-Path, könnten Dateien gruppiert werden.

                    // Add to Datalist:
                    resultList.add(resultList.get(i).getChildList().get(k));
                }
            }
        }
        model.getDataModel().getDatalist().addAll(resultList);
        return resultList;
    }

}
