package filtit;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class TablePopupMenu extends JPanel {

    private JPopupMenu popup;
    private FiltItController controller;
    private JMenuItem propertyItem, openContainingFolderItem, openInDefaultApplication;

    /**
     * Constructor.
     *
     * @param controller
     */
    public TablePopupMenu(FiltItController controller) {
        popup = new JPopupMenu();
        this.controller = controller;
        init();
    }

    private void init() {
        IconManager im = new IconManager();
        propertyItem = new JMenuItem("Properties", im.createImageIcon("icon16.png"));
        propertyItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        propertyItem.addActionListener(controller);
        popup.add(propertyItem);

        popup.setBorder(new BevelBorder(BevelBorder.RAISED));
        popup.addPopupMenuListener(controller);
        popup.addMouseListener(controller);

        openContainingFolderItem = new JMenuItem("Open containing folder", im.createImageIcon("icon16.png"));
        openContainingFolderItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        openContainingFolderItem.addActionListener(controller);
        openContainingFolderItem.setActionCommand("openContainingFolder");
        popup.add(openContainingFolderItem);

        openInDefaultApplication = new JMenuItem("Open in default application", im.createImageIcon("icon16.png"));
        openInDefaultApplication.setHorizontalTextPosition(JMenuItem.RIGHT);
        openInDefaultApplication.addActionListener(controller);
        openInDefaultApplication.setActionCommand("openDefaultApplication");
        popup.add(openInDefaultApplication);

    }

    public JPopupMenu getPopup() {
        return popup;
    }

}
