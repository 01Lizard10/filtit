package filtit;

import java.io.File;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;

/**
 * Speichert die Ergebnisse der Suche ab. Ermöglicht die Umwandlung in anderen
 * Datenmodellen für verschiedene Darstellungen. Das Modell entspricht einer
 * großen Tablle mit allen Werten. Ein Modell hat alle Ergebnisse einer Suche in
 * sich. ToDo: Bei ensprechender Größe Ergebnisse auf die Festplatte schreiben.
 *
 * @author Hans-Georg Schladitz
 */
public class DataModel {

    /**
     * Liste aller möglichen Spaltenüberschriften. Hinweis: Diese wird nie
     * direkt verwendet. Es wird die verwendete Liste mittels Codierung
     * zusammengesetzt.
     */
//    final private String[] columnNames
//            = {"Name", "Pfad", "Letzte Änderung", "Größe", "Datentyp",
//                "Voller Name", "Kompletter Pfad", "Dateityp", "Größe in KB",
//                "Größe in MB", "Größe in GB", "Größe in TB", "Änderungsdatum", "Änderungszeit voll"
//            };
    final private String[] columnNames
            = {
                "Name", // 0
                "Path", // 1
                "Last modified", // 2
                "Size", // 3
                "Datatype", // 4
                "Full name", // 5
                "Complete path", // 6
                "Filetype", // 7
                "Size in KB", // 8
                "Size in MB", // 9
                "Size in GB", // 10
                "Size in TB", // 11
                "Changedate", // 12
                "Full changetime",// 13
                "Search-expression/Key", // 14
                "Found in path", // 15
                "foundpositions in path", // 16
                "Foundcount in path",// 17
                "Found in file", // 18
                "foundpositions in file", // 19
                "Foundcount in file", // 20
                "Icon", // 21
                "virtual", // 22
                "Reason for rejection" // 23
            };

    /**
     * Example values for ColumnManagementFrame.
     */
    final private Object[] exampleValues
            = {
                "aFileName", // 0
                "C:\\folder\\subfolder", //1  
                "1422394202330", // 2
                "739332", // 3
                "file", // 4
                "aFileName.exp", // 5 
                "c:\\folder\\subfolder\\aFileName.exp", // 6
                ".exp", // 7
                "722,004 KB", // 8
                "0,705 MB", // 9
                "0,001 GB", // 10
                "0 TB", // 11
                "25.11.2014 22:22", // 12
                "Tue Nov 25 22:22:04 CST 2014", //13
                "find", // 14
                "true", // 15
                "6, 19, 20", // 16 
                "3", // 17
                "true", // 18
                "5,14,53", // 19
                "3", // 20
                FileSystemView.getFileSystemView().getSystemIcon(new File(System.getProperty("user.dir"))), // 21
                "false", // 22
                "out of global min/max-value, " // 23
            };
    /**
     * Liste mit allen Ergebnissdatensätzen.
     */
    private ArrayList<Data> datalist = new ArrayList();

    /**
     * Standardkonstruktor.
     */
    public DataModel() {
    }

    /**
     * Fügt die Elemente aus der als Parameter übergebenen Liste der
     * DataModell-Liste hinzu.
     *
     * @param al ArrayList
     */
    public void insertDatas(ArrayList<Data> al) {
        if (datalist != null || !datalist.isEmpty()) {
            for (Data al1 : al) {
                datalist.add(al1);
            }
        }
    }

    // --------------------------------------------------------------------
    // Getter & Setter:
    // --------------------------------------------------------------------
    /**
     * Generiert aus columnNames eine Teilliste abhängig des ColumnCode´s.
     *
     * @param columnCode
     * @return String[]
     */
//    public String[] getColumns(ArrayList<Integer> columnCode) {
//
//        // Wenn Parameterliste fehlerhaft:
//        if (columnCode == null) {
//            return this.columnNames;
//        }
//        // Erstelle eine Liste von Integern, welche für die jeweilige Spalten stehen.        
//        String[] columnList = new String[columnCode.size()];
//        for (int i = 0; i < columnCode.size(); i++) {
//            columnList[i] = this.columnNames[columnCode.get(i)];
//        }
//        return columnList;
//    }
    public String[] getColumnNameMapedByColumnCode(ArrayList<Integer> columnCode) {

        // Wenn Parameterliste fehlerhaft:
        if (columnCode == null) {
            return this.columnNames;
        }
        // Erstelle eine Liste von Integern, welche für die jeweilige Spalten stehen.        
        String[] columnList = new String[columnCode.size()];
        for (int i = 0; i < columnCode.size(); i++) {
            columnList[i] = this.columnNames[columnCode.get(i)];
        }
        return columnList;
    }

    /**
     * Getter für die Ergebnisliste.
     *
     * @return ArrayList
     */
    public synchronized ArrayList<Data> getDatalist() {
        return datalist;
    }

    /**
     * Setter für die Ergebnisliste.
     *
     * @param datalist ArrayList
     */
    public synchronized void setDatalist(ArrayList<Data> datalist) {
        this.datalist = datalist;
    }

    /**
     * Erstellt einen Datensatz für die aktuelle Tabllenstruktur. ABHÄNGIG vom
     * SpaltenCode. Dieser kann als Object Zeile für Zeile der Tabelle angefügt
     * werden. Benutzt dafür die getEntityFromIndex(i)-Funktion.
     *
     * @param i Integer Zeile
     * @param v
     * @return String[] = {"...", "...", "...",...}.
     */
    public Object[] getDataObjectFromIndex(int i, Viewmodi v) {
        Object[] obj = new Object[Utilities.castStringToArrayList(v.getColumnCode()).size()];
        ArrayList<Object> entity = this.getCalculateEntityFromIndex(i, v);

        for (int k = 0; k < entity.size(); k++) {
            if (entity.get(k) instanceof String) {
                obj[k] = entity.get(k);
            }
            if (entity.get(k) instanceof Icon) {
                obj[k] = entity.get(k);
            }
        }

        return obj;
    }

    /**
     * Erstellt ein Entity des betrachteten Indexes bezogen auf die
     * Ergebnisliste (datalist), welche Abhängig von dem SpaltenCode.
     *
     * @param index Integer
     * @return entity ArrayList
     */
    public ArrayList<Object> getCalculateEntityFromIndex(int index, Viewmodi v) {
        ArrayList<Integer> columnCode = Utilities.castStringToArrayList(v.getColumnCode());
        ArrayList<Object> entity = new ArrayList();
        Data data = datalist.get(index);

        // Setze das Array obj abhängig vom Spaltencode zusammen:
        for (int k = 0; k < columnCode.size(); k++) {
            if (getColumnValueFromData(columnCode.get(k), data) instanceof String) {
                entity.add((String) getColumnValueFromData(columnCode.get(k), data));
            }
            if (getColumnValueFromData(columnCode.get(k), data) instanceof Icon) {
                entity.add((Icon) getColumnValueFromData(columnCode.get(k), data));
            }
        }
        return entity;
    }

    public Object getColumnValueFromData(int column, Data data) {

        switch (column) {
            case 0:
                return data.getName();
            case 1:
                return data.getPath();
            case 2:
                return data.getLastmodified();
            case 3:
                return data.getSize();
            case 4:
                return data.getType();
            case 5:
                return data.getFullname();
            case 6:
                return data.getFullpath();
            case 7:
                return data.getFiletype();
            case 8:
                return data.getSizeInKB();
            case 9:
                return data.getSizeInMB();
            case 10:
                return data.getSizeInGB();
            case 11:
                return data.getSizeInTB();
            case 12:
                return data.getLastmodifiedDateTime();
            case 13:
                return data.getLatmodifiedFull();
            case 14:
                return data.getFind();
            case 15:
                return data.isFoundInPath();
            case 16:
                return data.getPathFoundList();
            case 17:
                return data.getFoundCountInPath();
            case 18:
                return data.getFoundInContent();
            case 19:
                return data.getContentFoundList();
            case 20:
                return data.getFoundCountInFile();
            case 21:
                return data.getSystemIcon();
            case 22:
                return data.getIsVirtual();
            case 23:
                return data.getRejectionReason();
            default:
                return "ColumnCodeError!";
            // Hinweis: Das Hinzufügen neuer Spalten benötigt, 
            // benötigt auch das Hinzufügen neue Spaltennamen!     
        }
    }

    /**
     * Getter.
     *
     * @return String[] ColumnNames
     */
    public String[] getColumnNames() {
        return columnNames;
    }

    /**
     * Getter.
     *
     * @return String[] ExampleValues
     */
    public Object[] getExampleValues() {
        return exampleValues;
    }

    /**
     * Insert an entity after index i.
     *
     * @param newData Data
     * @param index integer
     */
    public void addToDatalistAfterIndex(Data newData, int index) {
        ArrayList<Data> result = new ArrayList<Data>();

        for (int i = 0; i < this.getDatalist().size(); i++) {
            if (i > index) {
                result.add(newData);
            }
            result.add(getDatalist().get(i));
        }
        datalist = result;
    }

}
