package filtit;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class TTVTableModel extends DefaultTableModel {

    @Override
//    public Class getColumnClass(int c) {
//        return getValueAt(0, c).getClass();
//    }
    public Class getColumnClass(int column)
        {
            for (int row = 0; row < getRowCount(); row++)
            {
                Object o = getValueAt(row, column);

                if (o != null)
                {
                    return o.getClass();
                }
            }

            return Object.class;
        }
}
