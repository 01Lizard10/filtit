package filtit;

import java.awt.Component;
import java.awt.MenuComponent;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

/**
 *
 * @author Hans-Georg Schladitz
 */
public class HeaderPopupMenu extends JPanel {

    private JPopupMenu popup;
    private FiltItController controller;
    private JMenuItem addColumnItem, deleteColumnItem, saveAsNewViewModi, overwriteActualVM;
    /**
     * Actual column under mouse after a click.
     */
    private int choosedColumn;

    public HeaderPopupMenu(FiltItController controller) {
        popup = new JPopupMenu();
        this.controller = controller;
        init();
    }

    private void init() {
        IconManager im = new IconManager();
        addColumnItem = new JMenuItem("Add column", im.createImageIcon("icon16.png"));
        addColumnItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        addColumnItem.addActionListener(controller);
        addColumnItem.setActionCommand("addColumn");
        popup.add(addColumnItem);

        deleteColumnItem = new JMenuItem("Delete column", im.createImageIcon("icon16.png"));
        deleteColumnItem.setHorizontalTextPosition(JMenuItem.RIGHT);
        deleteColumnItem.addActionListener(controller);
        deleteColumnItem.setActionCommand("deleteColumn");
        popup.add(deleteColumnItem);

        overwriteActualVM = new JMenuItem("Overwrite actual Viewmode", im.createImageIcon("icon16.png"));
        overwriteActualVM.setMnemonic('W');
        overwriteActualVM.setActionCommand("overwriteActualVM");
        overwriteActualVM.addActionListener(controller);
        popup.add(overwriteActualVM);

        // Deaktivate if no viewmodie hase been choosed:
        if (controller.getVM().getActualViewmodi() != null) {
            overwriteActualVM.setEnabled(true);
        } else {
            overwriteActualVM.setEnabled(false);
        }

        saveAsNewViewModi = new JMenuItem("Save as new Viewmode", im.createImageIcon("icon16.png"));
        saveAsNewViewModi.setHorizontalTextPosition(JMenuItem.RIGHT);
        saveAsNewViewModi.addActionListener(controller);
        saveAsNewViewModi.setActionCommand("saveViewmodi");
        popup.add(saveAsNewViewModi);

        popup.setBorder(new BevelBorder(BevelBorder.RAISED));
        popup.addPopupMenuListener(controller);
        popup.addMouseListener(controller);
    }

    public JPopupMenu getPopup() {
        return popup;
    }

    public void setChoosedColumn(int column) {
        this.choosedColumn = column;
    }

    public int getChoosedColumn() {
        return this.choosedColumn;
    }

    public JMenuItem getOverwriteActualVM() {
        return overwriteActualVM;
    }
    
}
