package filtit;

import java.util.ArrayList;

/**
 * Collect all values of a Viewmodi.
 *
 * @author Hans-Georg Schladitz
 */
public class Viewmodi {

    /**
     * Column-Code for this viewmodi.
     */
    private String columnCode;
    /**
     * Column widths of the table for this viewmodi.
     */
    private String columnWidthList;
    /**
     * Option: Column resize-option-on/off.
     */
    private Boolean autoColumnResize;
    /**
     * Option: Show FilterPanel true/false.
     */
    private Boolean showFilterPanel;
    /**
     * Name of the viewmodi.
     */
    private String caption;
    /**
     * Option: Hide-all-Files: True/False.
     */
    private Boolean hideAllFiles;
    /**
     * Option: Hide-all-Folder: True/False.
     */
    private Boolean hideAllFolder;
    /**
     * Option: Look and Feel:
     */
    private String lookAndFeel;
    /**
     * Option: Windowsposition and windowssize.
     */
    private Boolean rememberWindowsPosAndSize;
    /**
     * Count of threads for the file- and foldersearch.
     */
    private int searchFilesFolderThreadCount;
    /**
     * Count of threads for filecontent-searching.
     */
    private int searchInFileContentThreadCount;
    /**
     * Contains the last used gloabalMinMax-valus. So the values must be valid.
     */
    private SearchFilterData globalMinMax;
    /**
     * Contains the last used SFD-Values. So the values must be valid.
     */
    private ArrayList<SearchFilterData> sfd_List;

    /**
     * Default-constructor which sets defaultvalues for a viewmodi. BUT: SFD and
     * globalSFD still not initiate.
     */
    public Viewmodi() {
        columnCode = "21,5,8,12,14,19";
        columnWidthList = "";
        autoColumnResize = true;
        showFilterPanel = false;
        caption = "default viewmode";
        hideAllFiles = false;
        hideAllFolder = false;
        lookAndFeel = "javax.swing.plaf.metal.MetalLookAndFeel";
        rememberWindowsPosAndSize = true;
        searchFilesFolderThreadCount = 4;
        searchInFileContentThreadCount = 2;
        sfd_List = new ArrayList();
        globalMinMax = new SearchFilterData();
    }
    /**
     * 
     * @param s
     * @return 
     */
    public static Viewmodi getDeepCopy(Viewmodi s){
        Viewmodi v = new Viewmodi();
        
        v.setAutoColumnResize(s.getAutoColumnResize());
        v.setColumnCode(s.getColumnCode());
        v.setCaption(s.getCaption());
        v.setColumnWidthList(s.getColumnWidthList());
        v.setGlobalMinMax(s.getGlobalMinMax());
        v.setHideAllFiles(s.getHideAllFiles());
        v.setHideAllFolder(s.getHideAllFolder());
        v.setLookAndFeel(s.getLookAndFeel());
        v.setRememberWindowsPosAndSize(v.getRememberWindowsPosAndSize());
        v.setSFD_List(s.getSFD_List());
        v.setSearchFilesFolderThreadCount(s.getSearchFilesFolderThreadCount());
        v.setSearchInFileContentThreadCount(s.getSearchInFileContentThreadCount());
        v.setShowFilterPanel(s.getShowFilterPanel());
        
        return v;
    }

    /**
     *
     * Achtung: verbesserungspotential!
     *
     * @param choosedColumn
     */
    public void removeElementFromColumnWidthList(int choosedColumn) {
        String list = this.getColumnWidthList();
        int ipos = list.indexOf(",");
        ArrayList<Integer> al = new ArrayList();

        // read columnwidthList. Example: 13,10,15,
        while (ipos >= 0) {
            al.add(Integer.valueOf(list.substring(0, ipos)));
            list = list.substring(ipos + 1);
            ipos = list.indexOf(",");
        }
        if (list.equals("")) {
            columnWidthList = "";
            return;
        }
        al.add(Integer.valueOf(list.substring(ipos + 1)));

        if (choosedColumn > 0 && al.size() == 1) {
            al.remove(choosedColumn - 1);
        } else {
            this.columnWidthList = "";
            return;
        }

        // create new columnWidthList:
        String result = "";
        for (int i = 0; i < al.size(); i++) {
            if (i == al.size() - 1) {
                result = result + al.get(i);
                break;
            }
            result = result + al.get(i) + ",";
        }

        this.columnWidthList = result;
    }

    // ----------- Getter & Setter -----------------
    public String getColumnCode() {
        return columnCode;
    }

    public void setColumnCode(String columnCode) {
        this.columnCode = columnCode;
    }

    public String getColumnWidthList() {
        if (columnWidthList == null) {
            return "";
        }
        return columnWidthList;
    }

    public void setColumnWidthList(String columnWidthList) {
        this.columnWidthList = columnWidthList;
    }

    public Boolean getAutoColumnResize() {
        return autoColumnResize;
    }

    public void setAutoColumnResize(Boolean AutoColumnResize) {
        this.autoColumnResize = AutoColumnResize;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setShowFilterPanel(Boolean value) {
        showFilterPanel = value;
    }

    public Boolean getShowFilterPanel() {
        return showFilterPanel;
    }

    public Boolean getHideAllFiles() {
        return hideAllFiles;
    }

    public void setHideAllFiles(Boolean hideAllFiles) {
        this.hideAllFiles = hideAllFiles;
    }

    public Boolean getHideAllFolder() {
        return hideAllFolder;
    }

    public void setHideAllFolder(Boolean hideAllFolder) {
        this.hideAllFolder = hideAllFolder;
    }

    public String getLookAndFeel() {
        return lookAndFeel;
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public Boolean getRememberWindowsPosAndSize() {
        return rememberWindowsPosAndSize;
    }

    public void setRememberWindowsPosAndSize(Boolean rememberWindowsPosAndSize) {
        this.rememberWindowsPosAndSize = rememberWindowsPosAndSize;
    }

    public int getSearchFilesFolderThreadCount() {
        return searchFilesFolderThreadCount;
    }

    public void setSearchFilesFolderThreadCount(int searchFilesFolderThreadCount) {
        this.searchFilesFolderThreadCount = searchFilesFolderThreadCount;
    }

    public int getSearchInFileContentThreadCount() {
        return searchInFileContentThreadCount;
    }

    public void setSearchInFileContentThreadCount(int searchInFileContentThreadCount) {
        this.searchInFileContentThreadCount = searchInFileContentThreadCount;
    }

    public SearchFilterData getGlobalMinMax() {
        return globalMinMax;
    }

    public void setGlobalMinMax(SearchFilterData globalMinMax) {
        this.globalMinMax = globalMinMax;
    }

    public ArrayList<SearchFilterData> getSFD_List() {
        return sfd_List;
    }

    public void setSFD_List(ArrayList<SearchFilterData> SFD_List) {
        this.sfd_List = SFD_List;
    }
}
