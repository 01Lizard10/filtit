package filtit;

import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Versuch eine TreeTable mit einer normalen Tabelle zu verwirklichen.
 *
 * @author Lizard
 */
public class TTV extends JTable {

    private ArrayList<Integer> groupList;

    /**
     * Standardkonstruktor.
     */
    public TTV() {
        super();
    }

    /**
     * Setze neue Spaltennamen, genauer Tabellensruktur. Entfernt nicht zu dem
     * Columncode passende Spalten.
     *
     * @param columns String [] Spaltennamen
     */
    public void changeColumnNames(String[] columns) {
        JTableHeader th = this.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();
        for (int i = 0; i < columns.length; i++) {

            TableColumn tc = tcm.getColumn(i);
            tc.setHeaderValue(columns[i]);
        }
        // Entferne alle nicht Spalten die < coulmns.length
        if (columns.length < tcm.getColumnCount()) {
            for (int i = columns.length; i < tcm.getColumnCount(); i++) {
                tcm.removeColumn(tcm.getColumn(i));
            }
        }
        th.repaint();
    }

//    @Override
//    public Component prepareRenderer(
//            TableCellRenderer renderer, int row, int column) {
//        //System.out.println(row + " -> " + column);
//
//        Component c = super.prepareRenderer(renderer, row, column);
//       
//        //Default:
//        c.setBackground(Color.WHITE);
//        
//        // jede zweite Reihe grau machen:
////        if(row % 2 == 0){
////        //  add custom rendering here
////            System.out.println("row" + row);
////            c.setBackground(Color.LIGHT_GRAY);
////        }else{
////            c.setBackground(Color.WHITE);
////        }
////        if (groupList != null) {
////            if (groupList.size() > row) {
////                if (groupList.get(row) == 0) {
////                    c.setBackground(Color.WHITE);
////                }
////                if (groupList.get(row) == 1) {
////                    c.setBackground(Color.decode("#dcdcdc"));
////                }
////            }
////        }
//
//        return c;
//    }

    // Getter.
    public void setGroupList(ArrayList<Integer> groupList) {
        this.groupList = groupList;
    }

}
