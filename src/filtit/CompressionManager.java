package filtit;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.impl.FileVolumeManager;
import com.github.junrar.rarfile.FileHeader;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Manage all compressionsmethodes.
 *
 * @author Hans-Georg Schladitz
 */
public class CompressionManager {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        CompressionManager cm = new CompressionManager();
        //cm.extractZipFiles("d:\\asda.7z", "d:\\");
        //cm.unpackRarTest();
        //cm.unpack7zTest();
        cm.unpackZipV2Test();
    }

    public void unpackZipV2Test() throws ArchiveException, IOException {
        String filename = "D:\\Citavi 4.zip";
        CompressionManager cm = new CompressionManager();
        cm.extractArchiveZip(new File(filename), new File("D:\\"));
    }

    public void unpack7zTest() {
        String filename = "E:\\Rartest\\test2.7z";
        CompressionManager cm = new CompressionManager();
        cm.extractArchive7z(new File(filename), new File("E:\\"));
    }

//    public static void extractZipFiles(String filename, String destinationname) {
//        try {
//            // destination folder to extract the contents         
//            //String destinationname = "d:\\temp\\testZip\\";          
//
//            byte[] buf = new byte[1024];
//            ZipInputStream zipinputstream = null;
//            ZipEntry zipentry;
//            zipinputstream = new ZipInputStream(new FileInputStream(filename));
//            zipentry = zipinputstream.getNextEntry();
//
//            while (zipentry != null) {
//
//                // for each entry to be extracted
//                String entryName = zipentry.getName();
//
//                int n;
//                FileOutputStream fileoutputstream;
//                File newFile = new File(entryName);
//
//                String directory = newFile.getParent();
//
//                // to creating the parent directories
//                if (directory == null) {
//                    if (newFile.isDirectory()) {
//                        break;
//                    }
//                } else {
//                    new File(destinationname + directory).mkdirs();
//                }
//
//                if (!zipentry.isDirectory()) {
//                    // System.out.println("File to be extracted....." + entryName);
//                    fileoutputstream = new FileOutputStream(destinationname + entryName);
//                    while ((n = zipinputstream.read(buf, 0, 1024)) > -1) {
//                        fileoutputstream.write(buf, 0, n);
//                    }
//                    fileoutputstream.close();
//                }
//
//                zipinputstream.closeEntry();
//                zipentry = zipinputstream.getNextEntry();
//            }// while
//
//            zipinputstream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    /**
     * Extract zip-Archive v2, without Malformed-Error, if a file in the archive
     * usinge strange char-codes.
     *
     * @param archive Zip to extract.
     * @param destDir sourcefolder String.
     */
    public void extractArchiveZip(File archive, File destDir) throws FileNotFoundException, ArchiveException, IOException {

        File input = archive;
        InputStream is = new FileInputStream(input);
        ArchiveInputStream in = new ArchiveStreamFactory().createArchiveInputStream("zip", is);
        ZipArchiveEntry entry = (ZipArchiveEntry) in.getNextEntry();
        OutputStream out = new FileOutputStream(new File(destDir, entry.getName()));

        while (entry != null) {
            File f = new File(destDir + File.separator + entry.getName());
            if (entry.isDirectory()) {

                f.mkdirs();
                entry = (ZipArchiveEntry) in.getNextEntry();
                continue;
            }

            // Check if the folder of the file exist.
            File tmp = new File(f.getParent());
            if (!tmp.exists()) {
                //System.out.println(" Erstelle: " + tmp.getPath());
                tmp.mkdirs();
            }

            out = new FileOutputStream(new File(destDir, entry.getName()));
            IOUtils.copy(in, out);
            entry = (ZipArchiveEntry) in.getNextEntry();

        }
        out.close();
        in.close();
    }

    /**
     * Extract zips and jars, but throws hard errors if the archive is
     * malformed.
     *
     * @param archive
     * @param destDir
     */
    public void extractArchiveZipJar(File archive, File destDir) {

        try {
            if (!destDir.exists()) {
                destDir.mkdir();
            }

            ZipFile zipFile = new ZipFile(archive, Charset.forName("UTF-8"));
            Enumeration entries = zipFile.entries();

            byte[] buffer = new byte[16384];
            int len;

            while (entries.hasMoreElements()) {
                // IllegalArgumentException: MALFORMED
                ZipEntry entry = (ZipEntry) entries.nextElement();

                String entryFileName = entry.getName();

                File dir = buildDirectoryHierarchyFor(entryFileName, destDir);
                if (!dir.exists()) {
                    dir.mkdirs();
                }

                if (!entry.isDirectory()) {
                    BufferedOutputStream bos = new BufferedOutputStream(
                            new FileOutputStream(new File(destDir, entryFileName)));

                    BufferedInputStream bis = new BufferedInputStream(zipFile
                            .getInputStream(entry));

                    while ((len = bis.read(buffer)) > 0) {
                        bos.write(buffer, 0, len);
                    }

                    bos.flush();
                    bos.close();
                    bis.close();
                }
            }
            zipFile.close();
        } catch (IOException ex) {
            System.out.println("Error: " + ex);
        }
    }

    /**
     * http://commons.apache.org/proper/commons-compress/examples.html , tar,
     * zip, gzip, XZ, Pack200, bzip2, 7z, arj, lzma, snappy, DEFLATE and Z
     * files.
     *
     * @param archive
     * @param destDir
     */
    public void extractArchive7z(File archive, File destDir) {
        try {
            SevenZFile sevenZFile = new SevenZFile(archive);

            SevenZArchiveEntry entry = sevenZFile.getNextEntry();
            FileOutputStream out = null;
            while (entry != null) {

                File f = new File(destDir + File.separator + entry.getName());
                if (entry.isDirectory()) {

                    f.mkdirs();
                    entry = sevenZFile.getNextEntry();
                    continue;
                }

                // Check if the folder of the file exist.
                File tmp = new File(f.getParent());
                if (!tmp.exists()) {
                    //System.out.println(" Erstelle: " + tmp.getPath());
                    tmp.mkdirs();
                }

                out = new FileOutputStream(new File(destDir, entry.getName()));

                byte[] content = new byte[(int) entry.getSize()];
                sevenZFile.read(content, 0, content.length);
                out.write(content);

                entry = sevenZFile.getNextEntry();

            }
            out.close();
            sevenZFile.close();

        } catch (FileNotFoundException ex) {
            System.err.println("ERROR: Datei konnte nicht gefunden werden.");
            Logger.getLogger(CompressionManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.err.println("ERROR: IO-Problem.");
            Logger.getLogger(CompressionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private File buildDirectoryHierarchyFor(String entryName, File destDir) {
        int lastIndex = entryName.lastIndexOf('/');
        String entryFileName = entryName.substring(lastIndex + 1);
        String internalPathToEntry = entryName.substring(0, lastIndex + 1);
        return new File(destDir, internalPathToEntry);
    }

    public void extractArchiveRar(File rarfile, File srcFile) {

        Archive a = null;
        try {
            a = new Archive(new FileVolumeManager(rarfile));
        } catch (RarException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (a != null) {
            //a.getMainHeader().print();
            FileHeader fh = a.nextFileHeader();
            while (fh != null) {
                try {

                    if (fh.isDirectory()) {
                        File out = new File(srcFile.getPath() + File.separator + fh.getFileNameString().trim());
                        out.mkdirs();
                        fh = a.nextFileHeader();
                        continue;
                    }

                    File out = new File(srcFile.getPath() + File.separator + fh.getFileNameString().trim());

                    // Check if the folder of the file exist.
                    File tmp = new File(out.getParent());
                    if (!tmp.exists()) {
                        //System.out.println(" Erstelle: " + tmp.getPath());
                        tmp.mkdirs();
                    }

                    //System.out.println(out.getAbsolutePath());
                    FileOutputStream os = new FileOutputStream(out);
                    a.extractFile(fh, os);
                    os.close();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (RarException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                fh = a.nextFileHeader();
            }
        }
    }
}
