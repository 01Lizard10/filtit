package filtit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Hans-Georg Wu
 */
public class FiltItView extends JFrame implements Observer {

    // -----------------------------------------------------
    // Sprach-Einstellung:
    private final String sWindowsTitle = "FiltIt Beta";
    private final String sChooseFolderButtonCaption = "Choose folder!";
    private final String sFolderpathLbl = "Folderpath: ";
    private final String srcTFfolderpath = "Insert folderpath here!";
    private final String sChooseFolderDialogTitle = "Choose a path";
    private final String sFindLbl = "Finde: ";   
    private final String sFilterExpression = "Insert a Filter Expression.";
    private final String sSearchStart = "start your search.";
    private String srcTFfind = "Insert a keyword here!";
    
    // -----------------------------------------------------
    // Membervariablen:
    private FiltItController controller;
    private JPanel mainpanel;
    private JLabel lblFolderpath;
    private JTextArea taFolder;
    private JScrollPane spSrcFolder;
    private JButton btnStartSearch;
    private JButton btnClearResults;
    private JButton btnChooseFolder;
    private final JButton btnSaveNowAndInViewmodi = new JButton("Apply and Save");
    private final JButton btnDiscard = new JButton("Discard");
    private JButton btnRummageMarkedElements = new JButton("rummage marked elements");
    private final JLabel lblFilterPanel = new JLabel("Filter expression:");
    private JLabel findLbl;
    private JTextArea taFind;
    private JScrollPane spSrcFind;
    private JTextArea taColumnFilter;
    private JScrollPane spColumnFilter;
    private TabbedPanel tabbedpanel;
    private FiltItMenuBar menuBar;
    private TablePopupMenu menuTablePopup;
    private HeaderPopupMenu menuHeaderPopup;
    private JLabel lblSearchresult;
    // Tabellenvaribalen:        ------------
    private TTV ttv;
    private JScrollPane ttvsp;
    private TTVTableModel tm = new TTVTableModel();
    private TableRowSorter<TableModel> sorter;

    // Tabllenvaribalen End     ------------
    private JCheckBox cbHideFiles;
    private JCheckBox cbHideFolder;
    private JCheckBox cbRememberWindowsPosAndSize;
    private JProgressBar progressbar;
    //private JButton btnAbortSearch;
    private LookAndFeelChooser laf;
    // Panel02-Elements:
    private JPanel mainPanel_p2;
    private final JButton btnRestoreDefaultValues = new JButton("Restore default values");
    private final JButton btnApply = new JButton("Apply");
    private final JButton btnSaveNowAndInViewmodi_p2 = new JButton("Apply and Save");
    private final JButton btnDiscard_OnPanel_02 = new JButton("Discard");
    private final JButton btnAddDocumenttype = new JButton("Add document-type");
    private final JButton btnDeleteAllOption = new JButton("Delete all options");
    private MinMaxCollectorFileSizeComponent globalMinMax = new MinMaxCollectorFileSizeComponent();
    private final JLabel lblGlobalMinMax = new JLabel("Global minimum/maximum filesize:");
    private final JLabel lblDocType = new JLabel("Document type:");
    private final JLabel lblHandelMethode = new JLabel("Handel methode:");
    private final JLabel lblPrivateMinMax = new JLabel("Private minimum/maximum size:");
    private final int textAreaMaxHeigth = 45;

    // TextArea: searchFilesFolderThreadCount
    private JTextArea taSearchFilesFolderThreadCount = new JTextArea("", 0, textAreaMaxHeigth);
    private JScrollPane spSearchFilesFolderThreadCount = new JScrollPane(taSearchFilesFolderThreadCount, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    // TextArea: searchInFileContentThreadCount
    private JTextArea taSearchInFileContentThreadCount = new JTextArea("", 0, textAreaMaxHeigth);
    private JScrollPane spSearchInFileContentThreadCount = new JScrollPane(taSearchInFileContentThreadCount, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    private JLabel lblSearchFilesFolderThreadCount = new JLabel(" File- and foldersearch threadcount.");
    private JLabel lblsearchInFileContentThreadCount = new JLabel(" Filecontent threadcount.");
    private JLabel lblResultCount = new JLabel("");

    private final Box vbox000_p2 = Box.createVerticalBox();
    private final Box vbox00_p2 = Box.createVerticalBox();
    private final Box hbox00_p2 = Box.createHorizontalBox();
    private final Box hbox01_p2 = Box.createHorizontalBox();
    private final Box hbox02_p2 = Box.createHorizontalBox(); // lblMinMax
    private final Box hbox03_p2 = Box.createHorizontalBox(); // globalMinMax
    private final Box hbox04_p2 = Box.createHorizontalBox(); // Labels
    private final Box hbox05_p2 = Box.createHorizontalBox(); // Content
    private final Box hbox06_p2 = Box.createHorizontalBox(); // add documenttype
    private final Box hbox07_p2 = Box.createHorizontalBox(); // btns
    private final Box vbox01_Label_p2 = Box.createVerticalBox(); // Counter-Label
    private final Box vbox01_p2 = Box.createVerticalBox(); // doctypes
    private final Box vbox02_p2 = Box.createVerticalBox(); // handleMethode
    private final Box vbox03_p2 = Box.createVerticalBox(); // minMax
    private final Box vbox04_p2 = Box.createVerticalBox(); // delete Button

    /**
     * Contains all labels.
     */
    private ArrayList<JLabel> labelList = new ArrayList();
    /**
     * Contains all checkboxes from DocumentTyps.
     */
    private ArrayList<JCheckBox> checkboxList = new ArrayList();
    /**
     * Contains all comboboxes with the handle-methods.
     */
    private ArrayList<JComboBox> comboboxList = new ArrayList();
    /**
     * Contains all min-max-controllpanels for the document-type.
     */
    private ArrayList<MinMaxCollectorFileSizeComponent> minMaxList = new ArrayList(); // Handle Methodes
    /**
     * List with all deleteButtons.
     */
    private ArrayList<JButton> buttonList = new ArrayList();

    // Tab mainpanel:
    private Box vbox00_MainPanel = Box.createVerticalBox();
    private Box hbox02_MainPanel = Box.createHorizontalBox();
    private Box hbox03_MainPanel = Box.createHorizontalBox();
    private Box hbox04_MainPanel = Box.createHorizontalBox();
    private Box hbox05_MainPanel = Box.createHorizontalBox();
    private Box hbox05_5_MainPanel = Box.createHorizontalBox();
    private Box hbox06_MainPanel = Box.createHorizontalBox();
    private Box hbox07_MainPanel = Box.createHorizontalBox();
    private Box hboxEnd_MainPanel = Box.createHorizontalBox();

    // Tab filted elements
    private Box vbox00_FiltedPanel = Box.createVerticalBox();
    private Box hbox01_FiltedPanel = Box.createHorizontalBox();
    private Box hbox02_FiltedPanel = Box.createHorizontalBox();

    // Tab option tab:
    private Box vb00_OptionPanel = Box.createVerticalBox(); // contains all hbxx-boxes
    private Box hb02_OptionPanel = Box.createHorizontalBox(); // hide all files
    private Box hb03_OptionPanel = Box.createHorizontalBox(); // hide all folder
    private Box hb04_OptionPanel = Box.createHorizontalBox(); // look and feel
    private Box hb05_OptionPanel = Box.createHorizontalBox(); // remenber windowspos and -size
    private Box hb06_OptionPanel = Box.createHorizontalBox(); // spSearchFilesFolderThreadCount
    private Box hb07_OptionPanel = Box.createHorizontalBox(); // spSearchInFileContentThreadCount
    private Box hb08_OptionPanel = Box.createHorizontalBox(); // buttons

    // -------------------------------
    // Menubar definieren
    /**
     * Standardkonstruktor.
     */
    public FiltItView() {
        super("FiltIt");
    }

    /**
     * Kopierkonstruktor.
     *
     * @param controller FiltItController
     */
    public FiltItView(FiltItController controller) {
        this.controller = controller;
        initForm();
    }

    public void manageProgressbar(int val, int max){
        if(max < val)
            return;

        // Progressbarwerte setzen:
        progressbar.setMinimum(0);
        progressbar.setValue(val);
        progressbar.setMaximum(max-1);
        progressbar.setToolTipText(max + " rows.");

        // Set progressbar text:
        progressbar.setStringPainted(true);
        progressbar.setString(((val * 100) / max) + "%");

        // If min and max are the same:
        if (val == (max - 1)) {
            progressbar.setString(100 + "%");
        }

    }
    /**
     * Update-Funktion sets all values from model.
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {

        FiltItModel m = (FiltItModel) o;
//        setSearchResult(m.getSearchState());
//
//        // Wenn "progressbarUpdate" als Argument übergeben wurde soll sich nur die progressbar updaten:
//        if (arg instanceof String) {
//            if (arg.equals("progressbarUpdate")) {
//
//                System.out.println("--> " + m.getProgressValue() + " von " + m.getDataModel().getDatalist().size());
//
//                // Progressbarwerte setzen:
//                progressbar.setMinimum(0);
//                progressbar.setValue(m.getProgressValue());
//                progressbar.setMaximum(m.getDataModel().getDatalist().size() - 1);
//                progressbar.setToolTipText(m.getDataModel().getDatalist().size() + " rows.");
//
//                // Set progressbar text:
//                progressbar.setStringPainted(true);
//                progressbar.setString(((m.getProgressValue() * 100) / m.getDataModel().getDatalist().size()) + "%");
//
//                // If min and max are the same:
//                if (m.getProgressValue() == (m.getDataModel().getDatalist().size() - 1)) {
//                    progressbar.setString(100 + "%");
//                }
//                return;
//            }
//        }
        // ------------------------------------------------------------------
        Viewmodi v = m.getVM().getChangedViewmodi();
        if (v == null) {
            v = m.getVM().getDefaultViewmodi();
            m.getVM().setChangedViewmodi(v);
        }

        lblResultCount.setText(m.getDataModel().getDatalist().size() + " results. ");

        showFilterPanel(v.getShowFilterPanel());
        getCbHideFiles().setSelected(v.getHideAllFiles());
        getCbHideFolder().setSelected(v.getHideAllFolder());
        getCbRememberWindowsPosAndSize().setSelected(v.getRememberWindowsPosAndSize());
        controller.saveWindowsPositionAndSize();

        // Look&Feel: -----------------------------------------------------
        if (!v.getLookAndFeel().equals(laf.getSelectedLookAndFeelClassName())) {
            // Explain: it must be set invisible cause the EDT-Thread will throw exceptions.
            this.loadLookAndFeel(v.getLookAndFeel());
        }

        // End Look&Feel --------------------------------------------------
        // Set Threadcounts in the view:    
        taSearchFilesFolderThreadCount.setText(String.valueOf(v.getSearchFilesFolderThreadCount()));
        taSearchInFileContentThreadCount.setText(String.valueOf(v.getSearchInFileContentThreadCount()));
        // SFD start: -----------------------------------------------------

        readAllData(v.getSFD_List(), v.getGlobalMinMax());
        refreshComponents();
        repaint();
        // SFD End  -------------------------------------------------------

        // generally stuff: ---------------------------------------------------
        // Set actual viewmodi in title:
        if (m.getVM().getActualViewmodi() != null) {
            setTitle("FiltIt     Viewmode: " + m.getVM().getActualViewmodi().getCaption());
        }

        // Menu -> Viewmodi anpassen:
        initViewmodiInMenubar(m.getVM());

        // Enable a menuitem "overwrite actual viewmodi" if possible:
        if (m.getVM().getActualViewmodi() != null) {
            getFiltItMenuBar().getOverwriteActualVM().setEnabled(true);
            getHeaderPopupMenu().getOverwriteActualVM().setEnabled(true);
        } else {
            getFiltItMenuBar().getOverwriteActualVM().setEnabled(false);
            getHeaderPopupMenu().getOverwriteActualVM().setEnabled(false);
        }
        // End generally stuff ------------------------------------------------

        // Update Tables: ---------------------------------------------------- 
        // Alte Daten in der Tabelle löschen wenn vorhanden:
        tm = new TTVTableModel();
        // Setze Tabellenstruktur:
        tm.setColumnIdentifiers(m.getDataModel().getColumnNameMapedByColumnCode(Utilities.castStringToArrayList(v.getColumnCode())));

        ttv.setModel(tm);
        ttv.setName("ttv");
        ttv.addMouseListener(controller);
        ttv.getTableHeader().setName("Header");
        ttv.getTableHeader().addMouseListener(controller);

        // set Column sizes:
        if (v.getAutoColumnResize()) {
            setColumnSize();
        } else {
            setColumnWidthAsString(v.getColumnWidthList());
        }
        setAutoColumnResize(v.getAutoColumnResize());

        // Set values in the table:
        for (int i = 0; i < m.getDataModel().getDatalist().size(); i++) {
            // if the data is a folder:
            if (m.getDataModel().getDatalist().get(i).getType().equals("folder")) {
                // Nach Optionen Filten:
                if (v.getHideAllFolder()) {
                    continue;
                }
                tm.addRow(m.getDataModel().getDataObjectFromIndex(i, v));
            }
            // if the data is a file:
            if (m.getDataModel().getDatalist().get(i).getType().equals("file")) {
                if (v.getHideAllFiles()) {
                    continue;
                }
                tm.addRow(m.getDataModel().getDataObjectFromIndex(i, v));
            }
        }

        // Filtern der Spalte(en) wenn gewählt:
        sorter = new TableRowSorter<TableModel>(tm);

        // Set Rowfilter for filterpanel:
        ttv.setRowSorter(sorter);

        // Using Rowfilter (showSearch) if filterPanel is visible:
        if (menuBar.getCbShowFilterPanel().isSelected()) {
            if (taColumnFilter.getText() != "" && !taColumnFilter.getText().equals(sFilterExpression)) {
                sorter.setRowFilter(RowFilter.regexFilter(taColumnFilter.getText()));
            }
        }
        // End Table manipulation ---------------------------------------------

    }// End Update --------------------------------------------

    public void loadLookAndFeel(String className) {
        setVisible(false);
        // muss sein, damit der Eventlistener nicht erneut triggert!
        laf.removeItemListener(controller);

        laf.setLookAndFeelSelectionToClassName(className);
        updateLookAndFeelGUI(laf.getSelectedLookAndFeelClassName());

        controller.loadWindowsPositionAndSize();
        laf.addItemListener(controller);
        setVisible(true);
    }

    public boolean isValide() {

        // 1.Validate der Logik der SFD-Werte (keine doppelten Werte erlaubt),
        boolean A = isSFD_valid();

        // 2. Validate all MinMax-TextAreas:
        boolean B = isMinMaxTA_Valid();

        return (A && B);
    }

    /**
     * Check the MinMax-TextAreas. Also globalMinMax.
     */
    private boolean isMinMaxTA_Valid() {

        String defaultTooltip = "Search Folder Option.";
        String value;
        String errorTooltip = "Please insert a valid input. Just positiv integer are valid.";
        boolean isError = false;
        // -------------------------- Validate global Min Max ------------------
        // Set defaultvalues:
        globalMinMax.getMinTA().setBorder(null);
        globalMinMax.getMinTA().setToolTipText(defaultTooltip);
        globalMinMax.getMaxTA().setBorder(null);
        globalMinMax.getMaxTA().setToolTipText(defaultTooltip);

        // check MinValue:
        value = globalMinMax.getMinTA().getText();

        // is globalMIN < 0?
        if (!value.equals("")) {
            try {
                if (Integer.valueOf(value) < 0) {

                    globalMinMax.getMinTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    globalMinMax.getMinTA().setToolTipText(errorTooltip);
                    isError = true;
                }
            } catch (Exception e) {
                globalMinMax.getMinTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                globalMinMax.getMinTA().setToolTipText(errorTooltip);
                isError = true;
            }
        }
        value = globalMinMax.getMaxTA().getText();
        // is globalMAX < 0?
        if (!value.equals("")) {

            try {
                if (Integer.valueOf(value) < 0) {

                    globalMinMax.getMaxTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    globalMinMax.getMaxTA().setToolTipText(errorTooltip);
                    isError = true;
                }
            } catch (Exception e) {
                globalMinMax.getMaxTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                globalMinMax.getMaxTA().setToolTipText(errorTooltip);
                isError = true;
            }
        }
        // -------------------------- End of Validate global Min Max ------------------
        // -------------------------- Start of Validate SFD-MinMax-TextArea -----------
        // Set all values to default:
        for (int i = 0; i < minMaxList.size(); i++) {
            minMaxList.get(i).getMinTA().setBorder(null);
            minMaxList.get(i).getMinTA().setToolTipText(defaultTooltip);
            minMaxList.get(i).getMaxTA().setBorder(null);
            minMaxList.get(i).getMaxTA().setToolTipText(defaultTooltip);
        }

        // Check MIN-values:
        for (int i = 0; i < minMaxList.size(); i++) {
            value = minMaxList.get(i).getMinTA().getText();

            if (value.equals("")) {
                continue;
            }

            try {
                if (Integer.valueOf(value) < 0) {

                    minMaxList.get(i).getMinTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMinTA().setToolTipText(errorTooltip);
                    isError = true;
                }

            } catch (Exception e) {
                minMaxList.get(i).getMinTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                minMaxList.get(i).getMinTA().setToolTipText(errorTooltip);
                isError = true;
            }
        }
        // Check MAX-values:
        for (int i = 0; i < minMaxList.size(); i++) {
            value = minMaxList.get(i).getMaxTA().getText();

            if (value.equals("")) {
                continue;
            }

            try {
                if (Integer.valueOf(value) < 0) {

                    minMaxList.get(i).getMaxTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMaxTA().setToolTipText(errorTooltip);
                    isError = true;
                }

            } catch (Exception e) {
                minMaxList.get(i).getMaxTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                minMaxList.get(i).getMaxTA().setToolTipText(errorTooltip);
                isError = true;
            }
        }
        // -------------------------- End of Validate SFD-MinMax-TextArea -----------
        // valid = no failure:
        return !isError;
    }

    /**
     * Prüfe ob die eingegebenen Optionen valid sind. Nicht valide ist z.B. ein
     * Eintrag der schon vergeben wird. Dieser kann im Algorithmus bisher nicht
     * berücksichtigt werden. Darum soll es nicht möglich sein doppelte
     * Einstellungen zu speichern. Hinweis: Die Optionen sollten sortiert nach
     * Namen sein. Dies erleichtert das Validieren und die Übersicht für den
     * Benutzer.
     */
    private boolean isSFD_valid() {
        // Fehlerabfangen, falls diese Funktion zu früh aufgerufen wird (Events):
        if (checkboxList == null || comboboxList == null || labelList == null || minMaxList == null) {
            return false;
        }
        // Fehlerabfangen, falls diese Funktion zu früh aufgerufen wird (Events):
        if (checkboxList.size() != comboboxList.size()) {
            return false;
        }

        ArrayList<String> sList = new ArrayList();
        ArrayList<Boolean> bList = new ArrayList();

        boolean isError = false;

        String defaultTooltip = "Search Folder Option.";
        String errorTooltip = "Please insert a valid input. Duplicates are not allowed.";

        // Für alle Einträge:
        for (int i = 0; i < checkboxList.size(); i++) {

            // Bilde Doppelte Werte als true in bListe ab:
            // Wenn ein Eintrag doppelt existiert:
            if (isInStringList(sList, checkboxList.get(i).getText() + comboboxList.get(i).getSelectedIndex())) {
                bList.add(true);
                continue;
            }
            sList.add(checkboxList.get(i).getText() + comboboxList.get(i).getSelectedIndex());
            bList.add(false);
        }

        // Gibt es doppelte zahlen?
        if (isInBooleanList(bList, Boolean.TRUE)) {
            // Für alle doppelten Zeilen:
            for (int i = 0; i < bList.size(); i++) {

                // Wenn der betrachte Wert ein doppelter Ist:
                if (bList.get(i)) {

                    checkboxList.get(i).setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    checkboxList.get(i).setToolTipText(errorTooltip);
                    checkboxList.get(i).setBorderPainted(true);

                    comboboxList.get(i).setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    comboboxList.get(i).setToolTipText(errorTooltip);

                    labelList.get(i).setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    labelList.get(i).setToolTipText(errorTooltip);

                    minMaxList.get(i).getMaxTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMaxTA().setToolTipText(errorTooltip);

                    minMaxList.get(i).getMinTA().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMinTA().setToolTipText(errorTooltip);

                    minMaxList.get(i).getMaxCB().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMaxCB().setToolTipText(errorTooltip);

                    minMaxList.get(i).getMinCB().setBorder(BorderFactory.createLineBorder(Color.RED, 2));
                    minMaxList.get(i).getMinCB().setToolTipText(errorTooltip);

                } else { // Wenn der betrachtete Wert nicht doppelt ist:
                    checkboxList.get(i).setBorder(UIManager.getBorder("Button.border"));
                    checkboxList.get(i).setToolTipText(defaultTooltip);
                    checkboxList.get(i).setBorderPainted(false);

                    comboboxList.get(i).setBorder(null);
                    comboboxList.get(i).setToolTipText(defaultTooltip);

                    labelList.get(i).setBorder(null);
                    labelList.get(i).setToolTipText(defaultTooltip);

                    minMaxList.get(i).getMaxTA().setBorder(null);
                    minMaxList.get(i).getMaxTA().setToolTipText(defaultTooltip);

                    minMaxList.get(i).getMinTA().setBorder(null);
                    minMaxList.get(i).getMinTA().setToolTipText(defaultTooltip);

                    minMaxList.get(i).getMaxCB().setBorder(null);
                    minMaxList.get(i).getMaxCB().setToolTipText(defaultTooltip);

                    minMaxList.get(i).getMinCB().setBorder(null);
                    minMaxList.get(i).getMinCB().setToolTipText(defaultTooltip);
                }// Wenn es keine doppelten Werte gibt:
            } // End for-loop

            isError = true;
        } else {

            // alle Werte wieder herstellen:
            for (int i = 0; i < checkboxList.size(); i++) {
                checkboxList.get(i).setBorder(UIManager.getBorder("Button.border"));
                checkboxList.get(i).setToolTipText(defaultTooltip);
                checkboxList.get(i).setBorderPainted(false);

                comboboxList.get(i).setBorder(null);
                comboboxList.get(i).setToolTipText(defaultTooltip);

                labelList.get(i).setBorder(null);
                labelList.get(i).setToolTipText(defaultTooltip);

                minMaxList.get(i).getMaxTA().setBorder(null);
                minMaxList.get(i).getMaxTA().setToolTipText(defaultTooltip);

                minMaxList.get(i).getMinTA().setBorder(null);
                minMaxList.get(i).getMinTA().setToolTipText(defaultTooltip);

                minMaxList.get(i).getMaxCB().setBorder(null);
                minMaxList.get(i).getMaxCB().setToolTipText(defaultTooltip);

                minMaxList.get(i).getMinCB().setBorder(null);
                minMaxList.get(i).getMinCB().setToolTipText(defaultTooltip);

            }
//            btnSaveNow.setEnabled(true);
//            btnSaveNowAndInViewmodi_p2.setEnabled(true);
//            btnSaveNowAndInViewmodi.setEnabled(true);

            isError = false;
        }
        return !isError;
    }

    /**
     * Initiate all elements.
     */
    private void initForm() {

        // Windows-Properties:
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(controller);
        this.addKeyListener(controller);
        this.setTitle(sWindowsTitle);
        this.setVisible(true);
        // FiltIt-Main-Menu:
        menuBar = new FiltItMenuBar(controller);
        this.setJMenuBar(menuBar.getMenuBar());

        // Popup-Menus:
        menuTablePopup = new TablePopupMenu(controller);
        this.add(menuTablePopup);
        menuHeaderPopup = new HeaderPopupMenu(controller);
        this.add(menuHeaderPopup);

        // -------------------------------------------------------------------
        // Folderpath element:
        lblFolderpath = new JLabel(sFolderpathLbl);
        taFolder = new JTextArea(srcTFfolderpath, 0, textAreaMaxHeigth);
        taFolder.setName("folderpath");
        taFolder.setToolTipText(srcTFfolderpath);
        // Um zu checken ob ein Pfad mit strg + v eingefügt wurde        
        taFolder.addKeyListener(controller);
        spSrcFolder = new JScrollPane(taFolder, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        spSrcFolder.setMinimumSize(new Dimension(100, textAreaMaxHeigth));
        spSrcFolder.setPreferredSize(new Dimension(200, textAreaMaxHeigth));
        spSrcFolder.setMaximumSize(new Dimension(Short.MAX_VALUE, textAreaMaxHeigth));

        btnChooseFolder = new JButton("\uf114");
        btnChooseFolder.setActionCommand("chooseFolderBtn");
        btnChooseFolder.addActionListener(controller);
        btnChooseFolder.setMinimumSize(new Dimension(150, 30));
        btnChooseFolder.setPreferredSize(new Dimension(150, 30));
        btnChooseFolder.setMaximumSize(new Dimension(150, 30));
        btnChooseFolder.setToolTipText(srcTFfolderpath);

        try {
            InputStream is = FiltItView.class.getResourceAsStream("/fontawesome-webfont.ttf");
            Font font = Font.createFont(Font.TRUETYPE_FONT, is);
            font = font.deriveFont(Font.PLAIN, 16f);
            btnStartSearch = new JButton("\uf002");
            btnStartSearch.setFont(font);
            btnChooseFolder.setFont(font);

            btnClearResults = new JButton("\uf12d");
            btnClearResults.setFont(font);
        } catch (Exception e) {
            System.err.println("Error to load Fontawesome");
        }

        // "\uf0c0"
        btnStartSearch.setActionCommand("startSearchButton");
        btnStartSearch.addActionListener(controller);
        btnStartSearch.setMinimumSize(new Dimension(150, 30));
        btnStartSearch.setPreferredSize(new Dimension(150, 30));
        btnStartSearch.setMaximumSize(new Dimension(150, 30));
        btnStartSearch.setToolTipText(sSearchStart);
//        btnStartSearch.addFocusListener(controller);
        btnStartSearch.addKeyListener(controller); // for Enter

        btnClearResults.setActionCommand("clearResultsButton");
        btnClearResults.addActionListener(controller);
        btnClearResults.setMinimumSize(new Dimension(150, 30));
        btnClearResults.setPreferredSize(new Dimension(150, 30));
        btnClearResults.setMaximumSize(new Dimension(150, 30));
        btnClearResults.setToolTipText(sSearchStart);
        btnClearResults.addKeyListener(controller); // for Enter

        // Keyword (find) element:
        findLbl = new JLabel(sFindLbl);
        taFind = new JTextArea(srcTFfind, 0, 30);
        taFind.setName("find");
        taFind.addKeyListener(controller);
        taFind.setToolTipText(srcTFfind);
        spSrcFind = new JScrollPane(taFind, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        spSrcFind.setMinimumSize(new Dimension(100, textAreaMaxHeigth));
        spSrcFind.setPreferredSize(new Dimension(200, textAreaMaxHeigth));
        spSrcFind.setMaximumSize(new Dimension(Short.MAX_VALUE, textAreaMaxHeigth));


        // Spaltenfilter element:
        taColumnFilter = new JTextArea(sFilterExpression, 0, 23);
        spColumnFilter = new JScrollPane(taColumnFilter, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        spColumnFilter.setMinimumSize(new Dimension(180, 23));
        spColumnFilter.setPreferredSize(new Dimension(180, 23));
        spColumnFilter.setMaximumSize(new Dimension(180, 23));
        // Listener for Changes in the Textarea:
        taColumnFilter.getDocument().addDocumentListener(controller);
//        taColumnFilter.getDocument().putProperty("actionCommand", "taColumnFilter");
        lblSearchresult = new JLabel();
        progressbar = new JProgressBar();
        progressbar.setMinimumSize(new Dimension(200, 14));
        progressbar.setPreferredSize(new Dimension(200, 14));
        progressbar.setMaximumSize(new Dimension(200, 14));
        // At first not visible:
        lblFilterPanel.setVisible(false);
        spColumnFilter.setVisible(false);

        // Tabel initiieren:
        // Tabelle: ----------------------------------------
        ttv = new TTV();
        ttv.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ttv.setModel(tm);
        ttv.setFillsViewportHeight(true);
        ttv.setName("ttv");
        ttv.addMouseListener(controller);
        ttvsp = new JScrollPane(ttv);
        ttv.addMouseListener(controller);
        ttv.getTableHeader().addMouseListener(controller);
        ttv.getTableHeader().setName("Header");
        ttv.getColumnModel().addColumnModelListener(controller);
        // Enable the sortfeature:
        ttv.setAutoCreateRowSorter(true);

        // -------------------------------------------------------------------
        // Default-Settings for all Panes:
        // Außenabstände:
        int outerMargin = 40;
        hbox02_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox03_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox04_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox05_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hbox05_5_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 200));
        hbox07_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));
        hboxEnd_MainPanel.setMaximumSize(new Dimension(Short.MAX_VALUE, 50));

        mainpanel = new JPanel(new BorderLayout());
        add(mainpanel);
        mainpanel.add(vbox00_MainPanel);

        vbox00_MainPanel.add(hbox02_MainPanel);
        hbox02_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox02_MainPanel.add(lblFolderpath);
        hbox02_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox02_MainPanel.add(Box.createHorizontalGlue());

        vbox00_MainPanel.add(hbox03_MainPanel);
        hbox03_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox03_MainPanel.add(spSrcFolder);
        hbox03_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox03_MainPanel.add(btnChooseFolder);
        hbox03_MainPanel.add(Box.createHorizontalStrut(outerMargin));

        vbox00_MainPanel.add(hbox04_MainPanel);
        hbox04_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox04_MainPanel.add(findLbl);
        hbox04_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox04_MainPanel.add(Box.createHorizontalGlue());

        vbox00_MainPanel.add(hbox05_MainPanel);
        hbox05_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox05_MainPanel.add(spSrcFind);
        hbox05_MainPanel.add(Box.createHorizontalStrut(outerMargin));

        // ----------------------------------------------------
        vbox00_MainPanel.add(Box.createVerticalStrut(16));
        vbox00_MainPanel.add(hbox05_5_MainPanel);
        hbox05_5_MainPanel.add(Box.createHorizontalGlue());
        hbox05_5_MainPanel.add(btnStartSearch);
        hbox05_5_MainPanel.add(btnClearResults);
        hbox05_5_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        // Initialisiere Tabs:        -------------------------
        tabbedpanel = new TabbedPanel(controller);

        vbox00_MainPanel.add(hbox06_MainPanel);
        hbox06_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox06_MainPanel.add(tabbedpanel.getTabbedPanel());
        hbox06_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        // -------------------------------------------------------------------
        // -------------------------------------------------------------------
        // Pane/Tab 01:       
        tabbedpanel.getPanelAL().get(0).add(ttvsp);

        // Spalten-Filter adden:
        vbox00_MainPanel.add(Box.createVerticalStrut(5));
        vbox00_MainPanel.add(hbox07_MainPanel);
        hbox07_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox07_MainPanel.add(lblFilterPanel);
        hbox07_MainPanel.add(Box.createHorizontalStrut(5));
        hbox07_MainPanel.add(spColumnFilter);
        hbox07_MainPanel.add(Box.createHorizontalStrut(outerMargin));
        hbox07_MainPanel.add(Box.createHorizontalGlue());

        // Progress stat and progressbar:
        vbox00_MainPanel.add(hboxEnd_MainPanel);
        hboxEnd_MainPanel.add(Box.createHorizontalGlue());
        hboxEnd_MainPanel.add(Box.createVerticalStrut(30));
        hboxEnd_MainPanel.add(lblSearchresult);
        //hboxEnd.add(btnAbortSearch);
        hboxEnd_MainPanel.add(lblResultCount);
        hboxEnd_MainPanel.add(progressbar);
        hboxEnd_MainPanel.add(Box.createHorizontalStrut(5));

        // -----------------------------------------------------------------------
        // Ende Hauptseite und Tab 01!
        // -----------------------------------------------------------------------
        // Tab Nr.2 with index 1 - SFD_Panel:
        hbox01_p2.setPreferredSize(new Dimension(600, globalMinMax.getComboundBoxHeight()));
        hbox01_p2.setMaximumSize(new Dimension(600, globalMinMax.getComboundBoxHeight()));
        hbox01_p2.setMinimumSize(new Dimension(600, globalMinMax.getComboundBoxHeight()));

        hbox02_p2.setPreferredSize(new Dimension(600, 20));
        hbox02_p2.setMaximumSize(new Dimension(600, 20));
        hbox02_p2.setMinimumSize(new Dimension(600, 20));

        hbox04_p2.setPreferredSize(new Dimension(600, 20));
        hbox04_p2.setMaximumSize(new Dimension(600, 20));
        hbox04_p2.setMinimumSize(new Dimension(600, 20));

        hbox06_p2.setPreferredSize(new Dimension(600, 50));
        hbox06_p2.setMaximumSize(new Dimension(600, 50));
        hbox06_p2.setMinimumSize(new Dimension(600, 50));

        hbox07_p2.setPreferredSize(new Dimension(600, 50));
        hbox07_p2.setMaximumSize(new Dimension(600, 50));
        hbox07_p2.setMinimumSize(new Dimension(600, 50));

        btnRestoreDefaultValues.setPreferredSize(new Dimension(180, 30));
        btnRestoreDefaultValues.setMaximumSize(new Dimension(180, 30));
        btnRestoreDefaultValues.setMinimumSize(new Dimension(180, 30));
        btnRestoreDefaultValues.setActionCommand("restoreDefaultValues");
        btnRestoreDefaultValues.addActionListener(controller);
        int bWidth = 150;
        int bHeight = 30;
        btnApply.setPreferredSize(new Dimension(bWidth, bHeight));
        btnApply.setMaximumSize(new Dimension(bWidth, bHeight));
        btnApply.setMinimumSize(new Dimension(bWidth, bHeight));
        btnApply.setActionCommand("apply");
        btnApply.addActionListener(controller);

        btnSaveNowAndInViewmodi_p2.setPreferredSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi_p2.setMaximumSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi_p2.setMinimumSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi_p2.setActionCommand("saveAndAdd");
        btnSaveNowAndInViewmodi_p2.addActionListener(controller);

        btnDiscard_OnPanel_02.setPreferredSize(new Dimension(bWidth, bHeight));
        btnDiscard_OnPanel_02.setMaximumSize(new Dimension(bWidth, bHeight));
        btnDiscard_OnPanel_02.setMinimumSize(new Dimension(bWidth, bHeight));
        btnDiscard_OnPanel_02.setActionCommand("discard");
        btnDiscard_OnPanel_02.addActionListener(controller);

        btnAddDocumenttype.setPreferredSize(new Dimension(bWidth, bHeight));
        btnAddDocumenttype.setMaximumSize(new Dimension(bWidth, bHeight));
        btnAddDocumenttype.setMinimumSize(new Dimension(bWidth, bHeight));
        btnAddDocumenttype.setActionCommand("addDocumenttype");
        btnAddDocumenttype.addActionListener(controller);

        btnDeleteAllOption.setPreferredSize(new Dimension(170, bHeight));
        btnDeleteAllOption.setMaximumSize(new Dimension(170, bHeight));
        btnDeleteAllOption.setMinimumSize(new Dimension(170, bHeight));
        btnDeleteAllOption.setActionCommand("deleteAllOption");
        btnDeleteAllOption.addActionListener(controller);

        mainPanel_p2 = new JPanel(new BorderLayout());
        mainPanel_p2.add(vbox000_p2);

        vbox000_p2.add(hbox00_p2);

        hbox00_p2.add(vbox00_p2);
        hbox00_p2.add(Box.createHorizontalGlue());

        vbox00_p2.add(hbox02_p2);
        hbox02_p2.add(Box.createHorizontalStrut(5));
        hbox02_p2.add(lblGlobalMinMax);
        hbox02_p2.add(Box.createHorizontalGlue());

        vbox00_p2.add(hbox03_p2);
        hbox03_p2.add(hbox01_p2);
        hbox01_p2.add(Box.createHorizontalStrut(5));
        hbox01_p2.add(globalMinMax.insertComponentsIntoAHorizontalBox(Box.createHorizontalBox()));
        hbox01_p2.add(Box.createHorizontalStrut(5));
        hbox01_p2.add(btnRestoreDefaultValues);

        hbox03_p2.add(Box.createHorizontalGlue());

        vbox00_p2.add(Box.createVerticalStrut(5));
        vbox00_p2.add(hbox04_p2);
        hbox04_p2.add(Box.createHorizontalStrut(5));
        hbox04_p2.add(lblDocType);
        hbox04_p2.add(Box.createHorizontalStrut(5));
        hbox04_p2.add(lblHandelMethode);
        hbox04_p2.add(Box.createHorizontalStrut(5));
        hbox04_p2.add(lblPrivateMinMax);

        vbox00_p2.add(hbox05_p2);
        hbox05_p2.add(Box.createHorizontalStrut(3));
        // Listen anfügen:
        hbox05_p2.add(vbox01_Label_p2);// Labels
        hbox05_p2.add(vbox01_p2);// Checkboxes
        hbox05_p2.add(vbox02_p2);// ComboBoxes
        hbox05_p2.add(Box.createHorizontalStrut(5));
        hbox05_p2.add(vbox03_p2); // MinMax-Components
        hbox05_p2.add(Box.createHorizontalStrut(5));
        hbox05_p2.add(vbox04_p2); // Delete buttons

        addIndexLabel(vbox01_Label_p2);
        // Content:
        // Add Checkboxes:
        addCheckBoxed(vbox01_p2);

        // Add ComboBoxes:
        addComboBoxes(vbox02_p2);

        // Add MinMax-Components:
        addMinMaxes(vbox03_p2);

        // Add delete button:
        addDeleteButtons(vbox04_p2);

        hbox05_p2.add(Box.createHorizontalGlue());

        vbox00_p2.add(hbox06_p2);
        hbox06_p2.add(btnAddDocumenttype);
        hbox06_p2.add(Box.createHorizontalStrut(10));
        hbox06_p2.add(Box.createHorizontalStrut(10));
        hbox06_p2.add(btnDeleteAllOption);

        vbox00_p2.add(hbox07_p2);
        hbox07_p2.add(Box.createHorizontalGlue());
        hbox07_p2.add(btnApply);
        hbox07_p2.add(Box.createHorizontalStrut(5));
        hbox07_p2.add(btnSaveNowAndInViewmodi_p2);
        hbox07_p2.add(Box.createHorizontalStrut(5));
        hbox07_p2.add(btnDiscard_OnPanel_02);
        hbox07_p2.add(Box.createHorizontalStrut(5));
        vbox00_p2.add(Box.createVerticalGlue());

        // Farben zum testen:
//        vbox00_p2.setBorder(new LineBorder(Color.ORANGE));
//        hbox00_p2.setBorder(new LineBorder(Color.red));
//        hbox02_p2.setBorder(new LineBorder(Color.GREEN));
//        hbox01_p2.setBorder(new LineBorder(Color.BLACK));
//        hbox06_p2.setBorder(new LineBorder(Color.WHITE));
//        hbox05_p2.setBorder(new LineBorder(Color.BLUE));
        tabbedpanel.getPanelAL().get(1).add(mainPanel_p2);

        // -----------------------------------------------------------------------
        // Tab Nr. 4 with index 3:
        tabbedpanel.getPanelAL().get(2).add(vb00_OptionPanel);
        vb00_OptionPanel.add(hb02_OptionPanel); // hide all files
        vb00_OptionPanel.add(Box.createVerticalStrut(5));
        vb00_OptionPanel.add(hb03_OptionPanel); // hide all folder
        vb00_OptionPanel.add(Box.createVerticalStrut(5));
        vb00_OptionPanel.add(hb04_OptionPanel); // look and feel chooser
        vb00_OptionPanel.add(Box.createVerticalStrut(5));
        vb00_OptionPanel.add(hb05_OptionPanel); // remember Windows Pos And Size
        vb00_OptionPanel.add(Box.createVerticalStrut(5));
        vb00_OptionPanel.add(hb06_OptionPanel); // remember Windows Pos And Size
        vb00_OptionPanel.add(Box.createVerticalStrut(5));
        vb00_OptionPanel.add(hb07_OptionPanel); // remember Windows Pos And Size
        vb00_OptionPanel.add(Box.createVerticalStrut(10));
        vb00_OptionPanel.add(hb08_OptionPanel); // buttons

        cbHideFiles = new JCheckBox("Hide all files.");
        cbHideFiles.setActionCommand("hideFiles");
        cbHideFiles.addActionListener(controller);

        cbHideFolder = new JCheckBox("Hide all folder.");
        cbHideFolder.setActionCommand("hideFolder");
        cbHideFolder.addActionListener(controller);

        cbRememberWindowsPosAndSize = new JCheckBox("Remember windowsposition and -size.");
        cbRememberWindowsPosAndSize.setActionCommand("rememberWindowsPosAndSize");
        cbRememberWindowsPosAndSize.addActionListener(controller);

        laf = new LookAndFeelChooser();
        laf.addItemListener(controller);
        laf.setActionCommand("laf-Trigger");

        // Hide Files:
        hb02_OptionPanel.add(new JLabel("1. "));
        hb02_OptionPanel.add(cbHideFiles);
        hb02_OptionPanel.add(Box.createHorizontalGlue());

        // Hide Folder:
        hb03_OptionPanel.add(new JLabel("2. "));
        hb03_OptionPanel.add(cbHideFolder);
        hb03_OptionPanel.add(Box.createHorizontalGlue());

        // Look and Feel:
        hb04_OptionPanel.add(new JLabel("3. "));
        hb04_OptionPanel.add(laf);
        hb04_OptionPanel.add(new JLabel(" Look&Feel Theme."));
        hb04_OptionPanel.add(Box.createHorizontalGlue());

        hb05_OptionPanel.add(new JLabel("4. "));
        hb05_OptionPanel.add(cbRememberWindowsPosAndSize);
        hb05_OptionPanel.add(Box.createHorizontalGlue());

        spSearchFilesFolderThreadCount.setMinimumSize(new Dimension(80, 23));
        spSearchFilesFolderThreadCount.setPreferredSize(new Dimension(80, 23));
        spSearchFilesFolderThreadCount.setMaximumSize(new Dimension(80, 23));

        hb06_OptionPanel.add(new JLabel("5. "));
        hb06_OptionPanel.add(spSearchFilesFolderThreadCount);
        hb06_OptionPanel.add(lblSearchFilesFolderThreadCount);
        hb06_OptionPanel.add(Box.createHorizontalGlue());

        spSearchInFileContentThreadCount.setMinimumSize(new Dimension(80, 23));
        spSearchInFileContentThreadCount.setPreferredSize(new Dimension(80, 23));
        spSearchInFileContentThreadCount.setMaximumSize(new Dimension(80, 23));

        hb07_OptionPanel.add(new JLabel("6. "));
        hb07_OptionPanel.add(spSearchInFileContentThreadCount);
        hb07_OptionPanel.add(lblsearchInFileContentThreadCount);
        hb07_OptionPanel.add(Box.createHorizontalGlue());

        btnSaveNowAndInViewmodi.setPreferredSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi.setMaximumSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi.setMinimumSize(new Dimension(bWidth, bHeight));
        btnSaveNowAndInViewmodi.setActionCommand("saveAndAdd");
        btnSaveNowAndInViewmodi.addActionListener(controller);

        btnDiscard.setPreferredSize(new Dimension(bWidth, bHeight));
        btnDiscard.setMaximumSize(new Dimension(bWidth, bHeight));
        btnDiscard.setMinimumSize(new Dimension(bWidth, bHeight));
        btnDiscard.setActionCommand("discard");
        btnDiscard.addActionListener(controller);

        hb08_OptionPanel.add(btnSaveNowAndInViewmodi);
        hb08_OptionPanel.add(btnDiscard);
        hb08_OptionPanel.add(Box.createHorizontalGlue());
        // ComponentListener setzen um Resize zu prüfen
        // Hinweis: Der Listener muss hier am Ende hinzugefügt werden, 
        // weil sonst Funktionen aufgerufen werden die noch nicht initialisiert wurden.
        this.addComponentListener(controller);

        pack();

        // Set DocumentListener:
        globalMinMax.getMinTA().getDocument().addDocumentListener(controller);
        globalMinMax.getMinTA().getDocument().putProperty("actionCommand", "minTA");

        globalMinMax.getMaxTA().getDocument().addDocumentListener(controller);
        globalMinMax.getMaxTA().getDocument().putProperty("actionCommand", "maxTA");

        taSearchFilesFolderThreadCount.getDocument().addDocumentListener(controller);
        taSearchFilesFolderThreadCount.getDocument().putProperty("actionCommand", "taSearchFilesFolderThreadCount");
//
        taSearchInFileContentThreadCount.getDocument().addDocumentListener(controller);
        taSearchInFileContentThreadCount.getDocument().putProperty("actionCommand", "taSearchInFileContentThreadCount");

    }

    /**
     * Load the look and feel-theme for all GUI-elements.
     *
     * @param lafName String
     */
    private void updateLookAndFeelGUI(String lafName) {
        try {
            UIManager.setLookAndFeel(lafName);
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(FiltItView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Set insertet ColumnFilter for use: Is performed from Document change
     * Event for taFilterPanel.
     */
    public void useColumnFilter() {
        if (taColumnFilter.getText() != "" && !taColumnFilter.getText().equals(sFilterExpression)) {
            if (sorter != null) {
                sorter.setRowFilter(RowFilter.regexFilter(taColumnFilter.getText()));
            }
        }
    }

    /**
     * Setzt Fensterposition und -größe des Fensters.
     *
     * @param values array
     */
    public void setFrameStartBounds(int[] values) {
        if (values == null) {
            int x = (Toolkit.getDefaultToolkit().getScreenSize().width - getSize().width) / 2;
            int y = (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2;
            //this.setBounds(x, y, 650, 650);
            this.setBounds(x, y, 850, 650);
        } else {
            this.setBounds(values[0], values[1], values[2], values[3]);
        }
    }

    /**
     * Show/Hide the filterPanel respectively the elements that represents the
     * filterPanel.
     *
     * @param bShow
     */
    public void showFilterPanel(boolean bShow) {
        spColumnFilter.setVisible(bShow);
        lblFilterPanel.setVisible(bShow);
    }

    /**
     * Set ResultLabel in the View. Value 1 means all okay/normal. Values -1
     * means Error.
     *
     * @param resultValue
     */
    private void setSearchResult(int resultValue) {
        if (resultValue == 0) {
            lblSearchresult.setText("running ...");
            lblSearchresult.setForeground(Color.black);
        }
        if (resultValue == 1) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String time = sdf.format(new Date());
            lblSearchresult.setText("Search completed. (" + time + ")");
            lblSearchresult.setForeground(Color.black);
        }
        if (resultValue == 2) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String time = sdf.format(new Date());
            lblSearchresult.setText("Search canceled by user. (" + time + ")");
            lblSearchresult.setForeground(Color.black);
        }
        if (resultValue == 3) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String time = sdf.format(new Date());
            lblSearchresult.setText("Unable to run a new search. Another search still in process.(" + time + ")");
            lblSearchresult.setForeground(Color.RED);
        }
        if (resultValue == 4) {
            lblSearchresult.setText("");
            lblSearchresult.setForeground(Color.black);
        }

        if (resultValue == -1) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String time = sdf.format(new Date());
            lblSearchresult.setText("Search-ERROR! (" + time + ")");
            lblSearchresult.setForeground(Color.RED);
        }
    }

    // Getter & Setter -------------------------------------------------
    public void initViewmodiInMenubar(ViewmodiManagement vm) {
        menuBar.initViewmodi(vm);
    }

    //Standard Getter & Setter:
    /**
     * Setter für den FolderPath.
     *
     * @param text String
     */
    public void setFolderPathTextArea(String text) {
        taFolder.setText(text);
    }

    /**
     * Getter für den Text aus dem FolderPath-Textfeld.
     *
     * @return String FolderPath.
     */
    public String getFolderPathTextArea() {
        return this.taFolder.getText();
    }

    /**
     * Getter für den Text aus dem Suchausdruck-Textfeld.
     *
     * @return String Searchexppression.
     */
    public String getFindTextArea() {
        return this.taFind.getText();
    }

    /**
     * Setter für den Find-Text.
     *
     * @param srcTFfind String
     */
    public void setFindTextArea(String srcTFfind) {
        this.srcTFfind = srcTFfind;
    }

    /**
     * Berechnet und setzt die spaltenbreite in gleicher Größe aus der
     * Tabellenbreite und Spaltenanzahl. Hinweis: Es besteht ein möglicher Bug
     * mit der breite der Tabs. Dies führt dazu, dass beim ersten Start manchmal
     * die Tabellenbreite größer ist als normal.
     */
    public void setColumnSize() {
        int columnwidth = 0;
        for (int i = 0; i < ttv.getColumnModel().getColumnCount(); i++) {
            // Tabellenbreite / Spaltenanzahl:
            //TableColumn col = ttv.getColumnModel().getColumn(i);
            columnwidth = Math.round(ttvsp.getWidth() / (ttv.getColumnModel().getColumnCount()));
            //System.out.println(columnwidth + " " + ttv.getColumnModel().getTotalColumnWidth() + " " + ttvsp.getWidth());
            ttv.getColumnModel().getColumn(i).setPreferredWidth(columnwidth);
        }
    }

    /**
     * Getter für CheckboxMenuItem AutoColumnResize.
     *
     * @return Boolean.
     */
    public boolean getAutoColumnResize() {
        return menuBar.getAutoColumnResize();
    }

    /**
     * Setter.
     *
     * @param bValue Boolean
     */
    private void setAutoColumnResize(Boolean bValue) {
        menuBar.setAutoColumnResize(bValue);
    }

    /**
     * Reults a List of ColumnWidth for actual column.
     *
     * @return String List like "4,5,10,6"
     */
    public String getColumnWidthAsString() {
        String result = "";
        for (int i = 0; i < ttv.getColumnModel().getColumnCount(); i++) {
            if (i == ttv.getColumnModel().getColumnCount() - 1) {
                return result + String.valueOf(ttv.getColumnModel().getColumn(i).getPreferredWidth());
            }
            result = result + String.valueOf(ttv.getColumnModel().getColumn(i).getPreferredWidth()) + ",";
        }
        return result;
    }

    /**
     * Setter for actual Columnwidth.
     *
     * @param list String List of Width
     */
    private void setColumnWidthAsString(String list) {

        if (list.equals("")) {
            this.setColumnSize();
            return;
        }
        int ipos = list.indexOf(",");
        int count = 0;
        while (ipos >= 0) {
            ttv.getColumnModel().getColumn(count).setPreferredWidth(Integer.valueOf(list.substring(0, ipos)));
            list = list.substring(ipos + 1);
            ipos = list.indexOf(",");
            count++;
        }
        list = list.substring(ipos + 1);
        ttv.getColumnModel().getColumn(count).setPreferredWidth(Integer.valueOf(list));
    }

    /**
     * Set all lists in the view.
     * @param list
     * @param glbMinMax
     */
    private void readAllData(ArrayList<SearchFilterData> list, SearchFilterData glbMinMax) {
        setGlobalMinMax_InView(glbMinMax);
        if (list != null) {
            // Clear all lists:
            labelList.clear();
            checkboxList.clear();
            comboboxList.clear();
            minMaxList.clear();
            // If there is a list:
            if (!list.isEmpty()) {

                // Sort the list:
                createCheckboxList(list);
                createComboboxList(list);
                createMinMaxList(list);

                setBoxSizes(list);
            }
        }
    }

    /**
     * Reads a SFD-List and creats a CheckboxList.
     *
     * @param list
     */
    private void createCheckboxList(ArrayList<SearchFilterData> list) {
        JCheckBox cb;

        for (int i = 0; i < list.size(); i++) {
            cb = new JCheckBox(list.get(i).getDocType());
            // Set size:
            cb.setPreferredSize(new Dimension(100, 23));
            cb.setMaximumSize(new Dimension(100, 23));
            cb.setMinimumSize(new Dimension(100, 23));

            cb.setSelected(list.get(i).getIsChecked());

            checkboxList.add(cb);
        }
    }

    /**
     * Reads a SFD-List and creats a ComboboxList.
     * @param list SFD list
     */
    private void createComboboxList(ArrayList<SearchFilterData> list) {
        JComboBox cb;
        FiltItModel m = new FiltItModel(null);
        for (int i = 0; i < list.size(); i++) {

            // Mögliche Werte für die Combobox setzen:
            cb = new JComboBox(m.getHandleMethodes());
            // Set size:
            cb.setPreferredSize(new Dimension(100, 23));
            cb.setMaximumSize(new Dimension(100, 23));
            cb.setMinimumSize(new Dimension(100, 23));

            cb.setSelectedIndex(0);
            // Komischer Fehler:
            if (String.valueOf(list.get(i).getHandleMethode()).equals("null")) {
                cb.setSelectedIndex(0);
            } else { // Kein komischer Fehler:
                cb.setSelectedIndex(Integer.valueOf(list.get(i).getHandleMethode()));
            }
            cb.addItemListener(controller);
            comboboxList.add(cb);
        }
    }

    /**
     * Reads a SFD-List and creats a MinMaxList.
     *
     * @param list
     */
    private void createMinMaxList(ArrayList<SearchFilterData> list) {

        MinMaxCollectorFileSizeComponent mmc;
        for (int i = 0; i < list.size(); i++) {
            mmc = new MinMaxCollectorFileSizeComponent();

            mmc.getMinTA().setText(list.get(i).getMinTA_Value());
            mmc.getMaxTA().setText(list.get(i).getMaxTA_Value());
            mmc.getMinCB().setSelectedIndex(list.get(i).getMinCB_Value());
            mmc.getMaxCB().setSelectedIndex(list.get(i).getMaxCB_Value());

            // Nötig für die validierung:
            mmc.getMinTA().getDocument().addDocumentListener(controller);
//            mmc.getMinTA().getDocument().putProperty("actionCommand", "minTA");
            mmc.getMaxTA().getDocument().addDocumentListener(controller);
            mmc.getMinCB().addItemListener(controller);
            mmc.getMaxCB().addItemListener(controller);
            minMaxList.add(mmc);
        }
    }

    /**
     * Set outer boxes on a size for all elements from the list.
     *
     * @param list
     */
    private void setBoxSizes(ArrayList<SearchFilterData> list) {
        // set mainbox-Size to prevent undesireable elementmoves:
        vbox00_p2.setPreferredSize(new Dimension(645, 200 + list.size() * 28));
        vbox00_p2.setMaximumSize(new Dimension(645, 200 + list.size() * 28));
        vbox00_p2.setMinimumSize(new Dimension(645, 200 + list.size() * 28));

        // set a box to prevent undesireable elementmoves:
        hbox05_p2.setPreferredSize(new Dimension(645, list.size() * 28));
        hbox05_p2.setMaximumSize(new Dimension(645, list.size() * 28));
        hbox05_p2.setMinimumSize(new Dimension(645, list.size() * 28));
    }

    /**
     * Reload all Elements in the view.
     */
    private void refreshComponents() {
        // Remove all elements from the view:
        vbox01_Label_p2.removeAll();
        vbox01_p2.removeAll();
        vbox02_p2.removeAll();
        vbox03_p2.removeAll();
        vbox04_p2.removeAll();

        // Add Index-Label:
        addIndexLabel(vbox01_Label_p2);

        // Add Checkboxes:
        addCheckBoxed(vbox01_p2);

        // Add ComboBoxes:
        addComboBoxes(vbox02_p2);

        // Add MinMax-Components:
        addMinMaxes(vbox03_p2);

        // Add Delete Buttons:
        addDeleteButtons(vbox04_p2);

    }

    /**
     * Add all Label in the view.
     *
     * @param box
     */
    private void addIndexLabel(Box box) {
        JLabel lbl;
        int len;
        for (int i = 0; i < checkboxList.size(); i++) {
            lbl = new JLabel(i + 1 + ".");
            len = lbl.getText().length() * 7;

            lbl.setPreferredSize(new Dimension(len, 23));
            lbl.setMaximumSize(new Dimension(len, 23));
            lbl.setMinimumSize(new Dimension(len, 23));

            labelList.add(lbl);

            box.add(Box.createVerticalStrut(2));
            box.add(lbl);
        }
    }

    /**
     * Add all CheckBoxes in the view.
     *
     * @param box
     */
    private void addCheckBoxed(Box box) {

        for (int i = 0; i < checkboxList.size(); i++) {
            box.add(checkboxList.get(i));
            box.add(Box.createVerticalStrut(2));
        }
    }

    /**
     * Add all ComboBoxes in the view.
     *
     * @param box Box
     */
    private void addComboBoxes(Box box) {
        for (int i = 0; i < comboboxList.size(); i++) {
            box.add(comboboxList.get(i));
            box.add(Box.createVerticalStrut(2));
        }
    }

    /**
     * Adds the Min-Max-Elements in the view.
     *
     * @param box Box
     */
    private void addMinMaxes(Box box) {
        for (int i = 0; i < minMaxList.size(); i++) {
            box.add(minMaxList.get(i).insertComponentsIntoAHorizontalBox(Box.createHorizontalBox()));
        }
    }

    private void addDeleteButtons(Box box) {
        JButton btn;

        for (int i = 0; i < checkboxList.size(); i++) {
            btn = new JButton("X");

            btn.setPreferredSize(new Dimension(45, 23));
            btn.setMaximumSize(new Dimension(45, 23));
            btn.setMinimumSize(new Dimension(45, 23));

            btn.setActionCommand("deleteType" + i);
            btn.addActionListener(controller);
            buttonList.add(btn);

            box.add(btn);
            box.add(Box.createVerticalStrut(2));
        }
    }

    /**
     * Creats a List with SearchFilterData-Strings out of the actual settings
     * (View). Example String: {".html", "txt", "50", "100"}
     *
     * @return ArrayList
     */
    public ArrayList<SearchFilterData> getSFD_FromView() {

        if (checkboxList.size() != comboboxList.size() || comboboxList.size() != checkboxList.size() || checkboxList.size() != minMaxList.size()) {
            return null;
        }

        ArrayList<SearchFilterData> resultList = new ArrayList();
        SearchFilterData data;

        // Für jeden angegebenen Datentyp:
        for (int i = 0; i < checkboxList.size(); i++) {
            data = new SearchFilterData();
            data.setDocType(checkboxList.get(i).getText());

            if (checkboxList.get(i).isSelected()) {
                data.setIsChecked(Boolean.TRUE);
            } else {
                data.setIsChecked(Boolean.FALSE);
            }

            data.setHandleMethode(String.valueOf(comboboxList.get(i).getSelectedIndex()));

            data.setMinDocSize(String.valueOf(minMaxList.get(i).getdMinValue()));
            data.setMaxDocSize(String.valueOf(minMaxList.get(i).getdMaxValue()));

            data.setMinCB_Value(minMaxList.get(i).getMinCB().getSelectedIndex());
            data.setMaxCB_Value(minMaxList.get(i).getMaxCB().getSelectedIndex());

            data.setMinTA_Value(minMaxList.get(i).getMinTA().getText());
            data.setMaxTA_Value(minMaxList.get(i).getMaxTA().getText());

            resultList.add(data);
        }

        return resultList;
    }

    /**
     * Reset alle content from view-elements in searchfolderOption.
     */
    public void resetGlobalMinMaxOptions() {
        globalMinMax.getMaxTA().setText("");
        globalMinMax.getMinTA().setText("");
        globalMinMax.getMaxCB().setSelectedIndex(0);
        globalMinMax.getMinCB().setSelectedIndex(0);
    }

    /**
     * Setze Werte für globalMinMax für die View.
     *
     * @param data
     */
    private void setGlobalMinMax_InView(SearchFilterData data) {

        if (data != null) {

            globalMinMax = new MinMaxCollectorFileSizeComponent();
            hbox01_p2.removeAll();

            hbox01_p2.add(Box.createHorizontalStrut(5));
            hbox01_p2.add(globalMinMax.insertComponentsIntoAHorizontalBox(Box.createHorizontalBox()));
            hbox01_p2.add(Box.createHorizontalStrut(5));
            hbox01_p2.add(btnRestoreDefaultValues);

            globalMinMax.getMinTA().setText(data.getGlobalMinTA());
            globalMinMax.getMaxTA().setText(data.getGlobalMaxTA());

            globalMinMax.getMinCB().setSelectedIndex(data.getGlobalMinCB());
            globalMinMax.getMaxCB().setSelectedIndex(data.getGlobalMaxCB());

            // Nötig für die validierung:
            globalMinMax.getMinTA().getDocument().addDocumentListener(controller);
            globalMinMax.getMinTA().getDocument().putProperty("actionCommand", "minTA");
            globalMinMax.getMaxTA().getDocument().addDocumentListener(controller);
            globalMinMax.getMaxTA().getDocument().putProperty("actionCommand", "maxTA");
        }
    }

    /**
     * Getter für die Globalen-MinMax-Werte aus der View.
     *
     * @return SearchFilterData
     */
    public SearchFilterData getGlobalMinMax_FromView() {
        SearchFilterData data = new SearchFilterData();
        data.setGlobalMinTA(globalMinMax.getMinTA().getText());
        data.setGlobalMaxTA(globalMinMax.getMaxTA().getText());
        data.setGlobalMinCB(globalMinMax.getMinCB().getSelectedIndex());
        data.setGlobalMaxCB(globalMinMax.getMaxCB().getSelectedIndex());
        data.setMinDocSize(String.valueOf(globalMinMax.getdMinValue()));
        data.setMaxDocSize(String.valueOf(globalMinMax.getdMaxValue()));
        return data;
    }

    /**
     * Prüft, ob ein Element (Boolean) in der Liste (Booleanliste) ist.
     *
     * @param al ArrayList
     * @param element Boolean
     * @return Boolean
     */
    private boolean isInBooleanList(ArrayList<Boolean> al, Boolean element) {
        // Für jedes Element in der Liste:
        for (int i = 0; i < al.size(); i++) {
            // Wenn das Element in der Liste ist:
            if (al.get(i) == element) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prüft, ob ein Element (String) in der Liste(Stringliste) ist.
     *
     * @param al ArrayList
     * @param element String
     * @return boolean
     */
    private boolean isInStringList(ArrayList<String> al, String element) {
        // Für jedes Element in der Liste:
        for (int i = 0; i < al.size(); i++) {
            // Wenn das Element in der Liste ist:
            if (al.get(i).equals(element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getter for TablePopupMenu.
     *
     * @return TablePopupMenu.
     */
    public TablePopupMenu getTablePopupmenu() {
        return this.menuTablePopup;
    }

    /**
     * Getter for HeaderPopupMenu.
     *
     * @return HeaderPopupMenu.
     */
    public HeaderPopupMenu getHPMPopup() {
        return this.menuHeaderPopup;
    }

    /**
     * Getter.
     *
     * @return FiltItMenuBar
     */
    public FiltItMenuBar getFiltItMenuBar() {
        return this.menuBar;
    }

    /**
     * Getter.
     *
     * @return HeaderPopupMenu
     */
    private HeaderPopupMenu getHeaderPopupMenu() {
        return menuHeaderPopup;
    }

    public JCheckBox getCbHideFiles() {
        return cbHideFiles;
    }

    public JCheckBox getCbHideFolder() {
        return cbHideFolder;
    }

    public JCheckBox getCbRememberWindowsPosAndSize() {
        return cbRememberWindowsPosAndSize;
    }

    public String getCbLookAndFeel() {
        return laf.getSelectedLookAndFeelClassName();
    }

    public LookAndFeelChooser getLookAndFeel() {
        return laf;
    }

    /**
     * Getter for FileFolder-Threadcount. Exceptionhandling is necessary,
     * beacause windows closing-event.
     *
     * @return int
     */
    public int getSearchFilesFolderThreadCount() {
        int result;
        try {
            result = Integer.valueOf(taSearchFilesFolderThreadCount.getText());
        } catch (Exception e) {
            return 4;
        }
        return result;
    }

    /**
     * Getter for FileFolderContent-Threadcount. Exceptionhandling is necessary,
     * beacause windows closing-event.
     *
     * @return int
     */
    public int getSearchInFileContentThreadCount() {
        int result;
        try {
            result = Integer.valueOf(taSearchInFileContentThreadCount.getText());
        } catch (Exception e) {
            return 2;
        }
        return result;
    }

    public void enableDiscardButtons(boolean enable) {
        btnDiscard.setEnabled(enable);
        btnDiscard_OnPanel_02.setEnabled(enable);
    }

    public void enableApplySaveButtons(boolean enable) {

        btnApply.setEnabled(enable);
        btnSaveNowAndInViewmodi_p2.setEnabled(enable);
        btnSaveNowAndInViewmodi.setEnabled(enable);

    }

    public JTextArea getTaFind() {
        return taFind;
    }
    public JTextArea getTaFolder(){
        return taFolder;
    }
    public JButton getSearchButton(){
        return btnStartSearch;
    }

}
