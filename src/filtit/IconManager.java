package filtit;

import javax.swing.ImageIcon;

/**
 * Manage Icons and Images.
 * @author Hans-Georg Schladitz
 */
public class IconManager {

    /**
     * Returns an ImageIcon, or null if the path was invalid.
     *
     * @param path String Path to the Icon
     * @return ImageIcon
     */
    public ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = IconManager.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Error: Couldn't find file: " + path);
            return null;
        }
    }
}
