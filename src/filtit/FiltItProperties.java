package filtit;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Rules Properties.
 *
 * @author Hans-Georg Schladitz
 */
public class FiltItProperties {

    /**
     * TODO: - Regelungen über die gespeicherte Tabllenform. - Speicherung der
     * Spaltenlängen. - Letzte Suche speichern. - Letzten Einstellungen wie die
     * Filteroptionen speichern. - Speichern der Filteroptionsvorlagen. Mögliche
     * Namen: Suchmodi, custom Suchoption -> Speichert alles in einer Datei. -
     * Es muss möglich sein den Speicherort der Propertydatei zu ändern.
     *
     */
    private Properties properties = new Properties();
    private File propFile = new File(System.getProperty("user.dir") + File.separator + "properties.properties");

    /**
     * Liest die Property-Datei ein und setzt die Werte in das Propertiesobjekt.
     * @return integer is -1 on error
     */
    public int readPropFile() {
        try {

            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(propFile));
            properties.load(bis);
            //System.out.println("Lese aus Properties: test -> " + properties.getProperty("Test1"));
            bis.close();
            return 1;
        } catch (Exception e) {
            System.err.println("Error: Unable to read the propertyfile." + propFile.getPath() + " " + e.getMessage());
                    String iconName = "icon32.png";
                    IconManager im = new IconManager();
                    Image img = im.createImageIcon(iconName).getImage();

                    // Dialog-MessageBox:
//                   JOptionPane.showConfirmDialog(null,  "Unable to read the propertyfile! \n"
//                            + "Starting with default values.", "Error:",JOptionPane.OK_OPTION,JOptionPane.OK_OPTION,new ImageIcon(img));
                   JOptionPane.showMessageDialog(null,"Unable to read the propertyfile! \n"
                            + "Starting with default values.","Error:",JOptionPane.ERROR_MESSAGE,new ImageIcon(img));
                   
                   // Create default properties.properties:
             return -1;                  
        }
    }

    /**
     * schreibt gesetzte werte des Propertiesobjekt in die Property-Datei.
     * Wichtig: Wenn nicht vor dem Schreiben gelesen wird, werden alle anderen
     * Werte überschrieben. Es kann passieren, dass die Datei dann leer ist,
     * wenn z.B. nur ein wert geschrieben wird.
     */
    public void writePropFile() {
        File propFile = new File(System.getProperty("user.dir") + File.separator + "properties.properties");
        try {
            if (!propFile.exists()) {
                try {
                    propFile.createNewFile();
                } catch (IOException ioe) {
                    System.err.println("Error: Unable to create a propertyfile: " + propFile.getPath() + " " + ioe.getMessage());
                }
            }
            properties.store(new FileOutputStream(propFile), System.getProperty("user.dir") + File.separator + "properties.properties");
        } catch (Exception e) {
            System.err.println("Error: Unable to write into the Property-file." + propFile.getPath() + " " + e.getMessage());
        }
    }
    // ----------------------------------------------------------------------
    // Getter & Setter:
    // ----------------------------------------------------------------------

    /**
     * Setter. Wrapper for LoD.
     *
     * @param key String
     * @param value String
     */
    public void setProperty(String key, String value) {
        this.properties.setProperty(key, value);
    }

    /**
     * Getter. Wrapper for LoD.
     *
     * @param key String
     * @return String Value
     */
    public String getProperty(String key) {
        return this.properties.getProperty(key);
    }

    /**
     * Removes the key in the propertyFile.
     *
     * @param key String
     */
    public void remove(String key) {
        properties.remove(key);
    }
}
