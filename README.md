# README #

### What is this repository for? ###

* Quick summary:

English:
This program allows you to search all files in a specified folder. This happens recursively. The special feature is that also within the files (TXT, PDF, XML etc.) is searched. It is also searched recursively in packed files (Zip, RAR, 7z, JAR).
Use case: You are looking for a PDF, but only know a certain text.
In order to improve the usability it is possible to save all set values as 'viewmodes' under own names. This eliminates the need to constantly adjust search settings for similar applications.

German:
Dieses Programm ermöglicht es dir alle Dateien in einem angegebenen Ordner zu durchsuchen. Dies geschieht rekursiv. Das Besondere hierbei ist, dass auch innerhalb der Dateien (TXT, PDF, XML etc.) gesucht wird. Es wird auch rekursiv in gepackten Dateien (Zip, RAR, 7z, JAR)   gesucht.
Anwendungsfall: Du suchst ein PDF, weißt aber nur noch einen bestimmten Wortlaut.
Um die Benutzbarkeit zu verbessern ist es möglich alle gesetzten Werte unter selbst gewählten Namen als 'viewmodes' zu speichern. Dadurch ist ein ständiges anpassen von Sucheinstellungen für ähnliche Verwendungszwecke nicht mehr nötig.

* Version: 1.0

### How it look ###

![stat.png](https://bitbucket.org/repo/a9BzB8/images/3842275985-stat.png)


### How do I get set up? ###

Steps for adding external jars in IntelliJ IDEA:

    Click File from File menu
    Project Structure (CTRL + SHIFT + ALT + S on Windows/Linux, ⌘ + ; on Mac OS X)
    Select Modules at the left panel
    Dependencies tab
    Add... → Project Library → Attach Jar
    -> add all JARs
    -> AND add all modules (junrar)

How to Build a fat Jar in IntelliJ IDEA?
    Build -> create Artifact -> Jar

For more HowTo`s look in sourcecode.

### Who do I talk to? ###

* Repo owner or admin: 
   Mail: hansgeorgwu@gmail.com